================
Decoding library
================

Compressed KDE (:mod:`compressed_kde`)
--------------------------------------

Classes for compressed kernel density estimation.

.. automodapi:: compressed_kde
   :no-heading: 
   :no-main-docstr:
   
Decode (:mod:`compressed_kde.decode`)
-------------------------------------

Classes for decoding.

   
.. automodapi:: compressed_kde.compressed_kde.decode
   :no-heading: 
   :no-main-docstr:

Neural decoding tools (:mod:`fklab.decode_tools.utilities`)
-----------------------------------------------------------

.. automodapi:: fklab.decode_tools.utilities
   :no-heading: 
   :no-main-docstr:
   
Spatial neural decoding tools (:mod:`fklab.decode_tools.maze`)
--------------------------------------------------------------
 
.. automodapi:: fklab.decode_tools.maze
   :no-heading:
   :no-main-docstr:

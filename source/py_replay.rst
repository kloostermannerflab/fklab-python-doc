==============
Replay library
==============

Collection of tools for replay analysis (:mod:`fklab.replay`)
-------------------------------------------------------------

.. currentmodule:: fklab.replay

Collection of tools to analyze neural replay events.

.. automodsumm:: fklab.replay
   :toctree: generated

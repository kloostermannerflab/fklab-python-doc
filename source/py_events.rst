========================================
Event time vectors (:mod:`fklab.events`)
========================================

Functions and classes to work with event time vectors.


Event algorithms (:mod:`fklab.events.basic_algorithms`)
-------------------------------------------------------

.. currentmodule:: fklab.events.basic_algorithms

Provides basic algorithms for event time vectors.

.. automodsumm:: fklab.events.basic_algorithms
   :toctree: generated


Event class (:mod:`fklab.events.event`)
---------------------------------------

.. automodapi:: fklab.events.event
   :no-heading:




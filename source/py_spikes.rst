======
Spikes
======

Core tools (:mod:`fklab.spikes.core`)
-------------------------------------

.. currentmodule:: fklab.spikes.core

Utilities for working with spike trains.

.. automodsumm:: fklab.spikes.core
   :toctree: generated


Spike train simulation (:mod:`fklab.spikes.simulation`)
-------------------------------------------------------

.. currentmodule:: fklab.spikes.simulation

Functions for generating spike trains as inhomogeneous Poisson
processes for which the rate varies as a function of time.
And functions for modeling cells with spatial, directional,
speed or ego-centric boundary tuning.

.. automodsumm:: fklab.spikes.simulation
   :toctree: generated


Spike train plotting (:mod:`fklab.spikes.plot`)
-----------------------------------------------

.. currentmodule:: fklab.spikes.plot

Visualization of spike data.

.. automodsumm:: fklab.spikes.plot
   :toctree: generated


Kilosort (:mod:`fklab.spikes.Kilosort`)
---------------------------------------

.. currentmodule:: fklab.spikes.Kilosort

Utilities for use the output of Kilosort to generate a spike collection

.. automodsumm:: fklab.spikes.Kilosort
   :toctree: generated



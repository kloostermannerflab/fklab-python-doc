.. raw:: html

    <style>
        .row {clear: both}
        .row h2  {border-bottom: 1px solid gray;}

        .column img {border: 1px solid gray;}

        @media only screen and (min-width: 1000px),
               only screen and (min-width: 500px) and (max-width: 768px){

            .column {
                padding-left: 5px;
                padding-right: 5px;
                float: left;
            }

            .column3  {
                width: 30%;
            }

            .column2  {
                width: 49%;
            }
        }

    </style>


############################################
Kloosterman Lab Data Analysis Tools (Python)
############################################

:Release: |release|
:Date: |today|

This documentation describes the common Python_ data analysis tools that are used
in the `Kloosterman Lab`_ at NERF_ (aka FKLab). 

Note that some of the tools are currently only available to lab members.
We are working towards releasing more and more of the code as open source
for public use. For questions and comments, please contact
`Fabian Kloosterman <https://www.nerf.be/people/fabian-kloosterman>`_.

If you are new in the domain, before beginning, you may want to take a look in the `How-to-start section <FAQ/how_to_start.rst>`_
to understand better how programming in python, how to use the versioning tool Git and how to document your code.

The first step to using FKlab data analysis tools will be to set up your environment in the `Set up FKlab tools part <set_up.rst>`_.
Different tutorials, depending of your level, will guide you. Once the environment is set up, you are ready to launch your first analysis!
The `Starting a new project  <starting_new_project.rst>`_ part will give you an theoretical overview of the data
and the analysis itself while the section `How-to guides <learn_by_examples.rst>`_ shows you some notebook examples.

.. warning::

    Fklab-python-internal repository / package are private. You cannot install them following this guide without having
    been accepted in the private KloostermanLab private channel. Contact the lab for more info.

    The fklab-compressed-decoder package is becoming public. You can find the public repository and the public documentation
    here: https://bitbucket.org/kloostermannerflab/py-compressed-kde and https://py-compressed-kde.readthedocs.io


You can also go through every available functions in the `reference <reference.rst>`_ documentation. 
As you gain experience with the FKLab Python tools and related utilities, read the
`guide for power users <set_up/guide_power_user.rst>`_ to learn about how you can contribute to improving the tools.


.. rst-class:: clearfix row 

********
Overview
********

.. rst-class:: clearfix row

.. rst-class:: column column2

`Set up the Fklab tool <set_up.rst>`_ - start here
==================================================

Jump in the analysis directly with `Jupyterhub <set_up/jupyterhub.rst>`_ or follow the installation guides for a `beginner user  <set_up/getting_started.rst>`_  and `developer user  <set_up/guide_power_user.rst>`_  of the FKlab analysis tools. 

.. rst-class:: column column2

`Launch your analysis <starting_new_project.rst>`_ - Key topics
===============================================================

Explanation of the different steps of the lab process from setting up the acquisition system during the experiment to analyze the data

.. rst-class:: clearfix row

.. rst-class:: column column2

`How-to guides <learn_by_examples.rst>`_ 
=========================================

Practical step-by-step notebook for conducting an analysis 

.. rst-class:: column column2

`Gallery <gallery.rst>`_ 
=========================================

Gallery of tools and visualizations 

.. rst-class:: column column2

`References <reference.rst>`_ 
==============================

Technical reference material for classes, methods, APIs, commands

.. rst-class:: clearfix row

****************
Additional notes
****************

.. rst-class:: clearfix row 

.. rst-class:: column column3

How to `start <FAQ/how_to_start.rst>`_ 
======================================

 - programing with python 
 - using Jupyter notebook
 - versioning your code with git
 - documenting your code with sphinx

.. rst-class:: column column3

How to `contribute <FAQ/how_to_contribute.rst>`_ 
=================================================

 - to the `Fklab tools <FAQ/how-to-contribute/git_workflow.rst>`_
 - to this `documentation <FAQ/how-to-contribute/documentation.rst>`_

.. rst-class:: column column3

How to `maintain <FAQ/how_to_maintain.rst>`_ 
============================================
 - `the fklab ecosystem <FAQ/how-to-maintain/ecosystem.rst>`_
 - `the fklab packages <FAQ/how-to-maintain/Build_conda_package.rst>`_
 - `the conda environments <FAQ/how-to-maintain/Build_conda_env.rst>`_
 - `the documentation <FAQ/how-to-maintain/Build_documentation.rst>`_

.. rst-class:: clearfix row 

******************
Indices and tables
******************

.. rst-class:: clearfix row

.. rst-class:: column column2

:ref:`genindex` - all functions, classes, terms
===============================================

.. rst-class:: column column2

:ref:`modindex`- Fklab modules index
====================================


.. rst-class:: clearfix row

.. rst-class:: column column2

:ref:`search`
=============

.. rst-class:: column column2

`Complete table of contents <content_table.rst>`_ - lists all sections and subsections
======================================================================================

.. rst-class:: clearfix row 

*****************
Meta informations
*****************

.. _core-changelog: https://bitbucket.org/kloostermannerflab/fklab-python-core/src/master/changelog.md
.. _internal-changelog: https://bitbucket.org/kloostermannerflab/fklab-python-internal/src/master/changelog.md

.. csv-table:: 
   :header: fklab-python-internal, fklab-python-core, fklab-compressed-decoder

   .. include:: ./internal_version.rst , .. include:: ./core_version.rst , .. include:: ./py_compressed_decoder_version.rst
   `internal-changelog`_ , `core-changelog`_,changelog

* `Reporting bugs or Enhancement proposals <FAQ/report_bugs.rst>`_

* `TODO list <FAQ/todo.rst>`_


.. _Python: http://www.python.org
.. _`Kloosterman Lab`: http://kloostermanlab.org
.. _NERF: http://www.nerf.be

.. toctree::
    :maxdepth: 1
    :hidden:

    content_table
    set_up
    starting_new_project
    learn_by_examples
    reference
    FAQ/how_to_start
    FAQ/how_to_contribute
    FAQ/how_to_maintain
    FAQ/report_bugs




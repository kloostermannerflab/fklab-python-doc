=======
File IO
=======

Common File IO Utilities (:mod:`fklab.io.common`)
-------------------------------------------------

.. currentmodule:: fklab.io.common

Common file import and export utilities.

.. automodsumm:: fklab.io.common
   :toctree: generated


FKLab Data Import (:mod:`fklab.io.data`)
----------------------------------------

.. currentmodule:: fklab.io.data

FKLab data import functions.

.. automodsumm:: fklab.io.data
   :toctree: generated

Neuralynx File IO (:mod:`fklab.io.neuralynx`)
---------------------------------------------

.. currentmodule:: fklab.io.neuralynx

Neuralynx file import functions.

Constants
.........

.. automodsumm:: fklab.io.neuralynx
   :toctree: generated
   :variables-only:

Functions
.........

.. automodsumm:: fklab.io.neuralynx
   :toctree: generated
   :functions-only:

Classes
.......

.. automodsumm:: fklab.io.neuralynx
   :toctree: generated
   :classes-only:


SpikeGLX (:mod:`fklab.io.spikeGLX`)
-----------------------------------

Utilities collection for SpikeGLX acquisition system.

Functions
.........

.. automodsumm:: fklab.io.spikeGLX
   :toctree: generated
   :functions-only:

Classes
.......

.. automodsumm:: fklab.io.spikeGLX
   :toctree: generated
   :classes-only:

Imro (Imec Readout)
...................

.. automodsumm:: fklab.io.spikeGLX.imec_imro
   :toctree: generated

OpenEphys File IO (:mod:`fklab.io.openephys`)
---------------------------------------------

.. currentmodule:: fklab.io.openephys

OpenEphys file import functions.

.. automodsumm:: fklab.io.openephys
   :toctree: generated

MWL File IO (:mod:`fklab.io.mwl`)
---------------------------------

.. currentmodule:: fklab.io.mwl

MWL file import functions.

.. automodsumm:: fklab.io.mwl
   :toctree: generated


Cloud data communication (:mod:`fklab.io.cloud`)
------------------------------------------------

.. currentmodule:: fklab.io.cloud

Collection of utilities to manage data in the diverse location.

.. automodsumm:: fklab.io.cloud
   :toctree: generated


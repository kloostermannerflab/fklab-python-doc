================================
Segments (:mod:`fklab.segments`)
================================

.. currentmodule:: fklab.segments

Functions and classes to work with time segments. All functions defined
in the basic_algorithms module and the Segment class are also available
directly from the top level segments package.


Segment class (:mod:`fklab.segments.segment`)
---------------------------------------------

.. currentmodule:: fklab.segments.segment

Provides a container class for a list of segments. Each segment is defined by
a start and end point (usually in units of time). Basic operations on lists
of segments are implemented as methods of the class.

.. automodsumm:: fklab.segments.segment
   :toctree: generated
   
   
Segment plotting (:mod:`fklab.segments.plot`)
---------------------------------------------

.. currentmodule:: fklab.segments.plot

Provides function for plotting segments.

.. automodsumm:: fklab.segments.plot
   :toctree: generated


Segment algorithms (:mod:`fklab.segments.basic_algorithms`)
-----------------------------------------------------------

.. currentmodule:: fklab.segments.basic_algorithms

Provides basic algorithms for lists of segments.

.. automodsumm:: fklab.segments.basic_algorithms
   :toctree: generated



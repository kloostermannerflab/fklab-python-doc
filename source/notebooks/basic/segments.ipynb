{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Segments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A segment is defined by two points: *start* and *end*. Most often segments are defined in time, for example to represent all time periods in which a signal is above threshold. The **Segment** class provides a convenient interface to a list of segments and implements many operations on these segments. The use of the **Segment** class is demonstrated in this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Module import"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.segments as seg\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creation of Segment objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create from lists\n",
    "s = seg.Segment([1, 2])  # 2-element list\n",
    "s = seg.Segment([[1, 2], [3, 4]])  # list with nested 2-element lists\n",
    "\n",
    "# create from numpy arrays\n",
    "s = seg.Segment(np.array([1, 2]))  # (2,) vector\n",
    "s = seg.Segment(np.array([[1, 2], [3, 4]]))  # (n,2) array\n",
    "\n",
    "# empty segment lists are OK\n",
    "s = seg.Segment()\n",
    "s = seg.Segment([])\n",
    "\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create from logical vector\n",
    "v = np.array([1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0], dtype=bool)\n",
    "s = seg.Segment.fromlogical(v)  # return segments expressed as indices into v\n",
    "print(s)\n",
    "\n",
    "x = np.arange(len(v)) / 10.0 + 5\n",
    "s = seg.Segment.fromlogical(v, x=x)  # return segments expressed as values in x\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create from vector of indices\n",
    "idx = np.array(\n",
    "    [3, 4, 5, 6, 10, 11, 15, 16, 17]\n",
    ")  # vector of indices into another vector\n",
    "s = seg.Segment.fromindices(idx)\n",
    "print(s)\n",
    "\n",
    "x = np.arange(20) / 10.0 + 5  # full vector of x-values that can be indexed with idx\n",
    "s = seg.Segment.fromindices(idx, x=x)\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create from lists of start and end points (aka events)\n",
    "on = np.array([1, 2, 4, 5, 9, 10, 14, 16, 19])\n",
    "off = np.array([3, 6, 11, 12, 17, 20])\n",
    "\n",
    "s = seg.Segment.fromevents(\n",
    "    on=on, off=off\n",
    ")  # construct smallest possible segments from start and end points\n",
    "print(s)\n",
    "\n",
    "# if there is a sequence of multiple start points before the next end point, then the default behavior is\n",
    "# to drop all start points except for the last one (i.e. the start point right before the end point)\n",
    "# to drop all but the first start point instead, use:\n",
    "s = seg.Segment.fromevents(on=on, off=off, greedyStart=True)\n",
    "print(s)\n",
    "\n",
    "# similarly, for a sequence of multiple end points, use:\n",
    "s = seg.Segment.fromevents(on=on, off=off, greedyStop=True)\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create from anchor points and durations\n",
    "anchor = np.array([1, 5, 9])\n",
    "duration = np.array([2, 4, 3])\n",
    "\n",
    "s = seg.Segment.fromduration(\n",
    "    anchor, duration\n",
    ")  # create segment with given duration and centered on the anchor\n",
    "print(s)\n",
    "\n",
    "# By default the segments are centered around the anchor points. A difference reference point can be set for the\n",
    "# anchors within the segments. The reference point is expressed in multiples/fractions of the segment duration.\n",
    "# For example, a reference of 0 means the anchor point is aligned to the start of the segment and a reference of 1\n",
    "# means that the anchor is aligned to the end of the segment\n",
    "s = seg.Segment.fromduration(anchor, duration, reference=0)\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create copy of other Segment\n",
    "s = seg.Segment([[1, 2], [4, 5]])\n",
    "s2 = seg.Segment(s)\n",
    "s is s2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tests on Segment objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# To test if input is a Segment or is segment-like\n",
    "a = seg.Segment.issegment([1, 2])  # returns True\n",
    "b = seg.Segment.issegment(seg.Segment([1, 2]))  # returns True\n",
    "c = seg.Segment.issegment(\"a\")  # returns False\n",
    "print(a, b, c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# To test of segments are sorted (i.e. start points are in order)\n",
    "s = seg.Segment([[1, 2], [4, 6], [5, 10]])\n",
    "print(s.issorted())\n",
    "\n",
    "s = seg.Segment([[4, 6], [1, 2], [5, 10]])\n",
    "print(s.issorted())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# To test if segments overlap (Note: this method will sort the segments as a side effect!)\n",
    "s = seg.Segment([[1, 2], [4, 6], [5, 10]])\n",
    "print(s.hasoverlap())\n",
    "\n",
    "s = seg.Segment([[1, 2], [4, 6], [8, 10]])\n",
    "print(s.hasoverlap())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing and modifying segment data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = seg.Segment([[1, 2], [5, 8], [10, 15], [20, 30]])\n",
    "\n",
    "# number of segments\n",
    "len(s)\n",
    "\n",
    "# slicing\n",
    "s[0:1]  # return new Segment object with first two segments\n",
    "\n",
    "# direct assignment\n",
    "s[0] = [2, 4]\n",
    "\n",
    "# remove segments\n",
    "del s[3]\n",
    "\n",
    "# get and set start points, end points\n",
    "s.start  # get start points\n",
    "s.start = [0, 4, 11]  # set start points\n",
    "\n",
    "s.stop  # get end points\n",
    "s.stop = [3, 9, 20]  # set end points\n",
    "\n",
    "# get and set segment durations and centers\n",
    "s.duration  # get duration\n",
    "s.duration = [2, 3, 3]  # set duration around segment center\n",
    "\n",
    "s.center  # get center\n",
    "s.center = [0, 10, 25]  # set segment center\n",
    "\n",
    "# get intervals in between segments (only really informative if segments are sorted and non-overlapping)\n",
    "s.intervals\n",
    "\n",
    "# iterate through segments\n",
    "# k is a tuple with segment start and end\n",
    "for k in s:\n",
    "    print(\"start=%(start)f, end=%(end)f\" % {\"start\": k[0], \"end\": k[1]})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convert segments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = seg.Segment([[1, 2], [5, 7], [10, 14]])\n",
    "\n",
    "# convert to string representation\n",
    "print(str(s))\n",
    "\n",
    "# convert to array (Note: returns a view on the data, not a copy!)\n",
    "a = np.asarray(s)\n",
    "\n",
    "# convert to boolean\n",
    "bool(s)  # returns True when 1 or more segment are defined and False otherwise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Operators"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = seg.Segment([[1, 3], [5, 7], [10, 15]])\n",
    "\n",
    "# not : return True if no segments are defined\n",
    "print(not s, not seg.Segment())\n",
    "\n",
    "# truth : return True of 1 or more segments are defined\n",
    "if s:\n",
    "    print(\"Segment object is not empty\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s1 = seg.Segment([[1, 2], [5, 7]])\n",
    "s2 = seg.Segment([[3, 4], [8, 10], [12, 15]])\n",
    "\n",
    "# equal\n",
    "print(s1 == s2)\n",
    "print(s1 == [[1, 2], [5, 7]])\n",
    "\n",
    "# not equal\n",
    "print(s1 != s2)\n",
    "print(s1 != np.array([2, 3]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# inversion of segments\n",
    "s = seg.Segment([0, 10])\n",
    "\n",
    "s.invert()  # return new inverted Segment\n",
    "~s  # return new inverted Segment\n",
    "\n",
    "s.iinvert()  # in place inversion\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# union of segments (will also remove overlap)\n",
    "s1 = seg.Segment([0, 10])\n",
    "s2 = seg.Segment([[5, 15], [20, 25]])\n",
    "s3 = seg.Segment(s1)  # copy\n",
    "\n",
    "s1.union(s2)  # will return new Segment object\n",
    "s1 | s2  # will return new Segment object\n",
    "\n",
    "s1.iunion(s2)  # in place union\n",
    "s3 |= s2  # in place union\n",
    "\n",
    "print(s1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# intersection (overlap) between segments\n",
    "s1 = seg.Segment([0, 10])\n",
    "s2 = seg.Segment([5, 15])\n",
    "s3 = seg.Segment(s1)  # copy\n",
    "\n",
    "s1.intersection(s2)  # will return new Segment object\n",
    "s1 & s2  # will return new Segment object\n",
    "\n",
    "s1.iintersection(s2)  # in place intersection\n",
    "s3 &= s2  # in place intersection\n",
    "\n",
    "print(s1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# difference (exclusive or) between segments\n",
    "s1 = seg.Segment([0, 10])\n",
    "s2 = seg.Segment([5, 15])\n",
    "s3 = seg.Segment(s1)  # copy\n",
    "\n",
    "s1.difference(s2)  # will return new Segment object\n",
    "s1 ^ s2  # will return new Segment object\n",
    "\n",
    "s1.idifference(s2)  # in place difference\n",
    "s3 ^= s2  # in place difference\n",
    "\n",
    "print(s1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# exclude other segments\n",
    "s1 = seg.Segment([[1, 10], [15, 20]])\n",
    "s2 = seg.Segment([3, 6])\n",
    "s3 = seg.Segment([18, 30])\n",
    "\n",
    "s1.exclusive(s2, s3)  # will return new Segment object\n",
    "\n",
    "s1.iexclusive(s2, s3)  # in place exclusion\n",
    "\n",
    "print(s1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add offsets\n",
    "s1 = seg.Segment([[2, 5], [6, 10]])\n",
    "\n",
    "s1.offset(3)  # will return new Segment object\n",
    "s1 + 3  # will return new Segment object\n",
    "s1 - np.array([1, 2])  # will return new Segment object\n",
    "\n",
    "s1.ioffset(3)  # in place offset\n",
    "s1 += 1  # in place offset\n",
    "s1 -= np.array([2, 3])  # in place offset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# scaling\n",
    "s1 = seg.Segment([[1, 5], [10, 12]])\n",
    "s2 = seg.Segment(s1)  # copy\n",
    "\n",
    "s1.scale(0.5)  # will return new Segment object\n",
    "s1 * 2  # will return new Segment object\n",
    "s1 / 2  # will return new Segment object\n",
    "\n",
    "s2.iscale(0.5)  # in place scaling\n",
    "s2 *= 2  # in place scaling\n",
    "s2 /= 3  # in place scaling\n",
    "\n",
    "print(s2)\n",
    "\n",
    "# the scale and iscale methods take an additional reference parameter that sets the anchor point for scaling\n",
    "s1.iscale(2, reference=0)\n",
    "\n",
    "print(s1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# concatenation of segments\n",
    "s1 = seg.Segment([[1, 3], [4, 7]])\n",
    "s2 = seg.Segment([[-1, 0], [10, 14]])\n",
    "s3 = seg.Segment([[5, 8], [15, 20]])\n",
    "\n",
    "s1.concat(s2, s3)  # will return new Segment object\n",
    "s1 + s2 + s3  # will return new Segment object\n",
    "\n",
    "s1.iconcat(s2)  # in place concatenation\n",
    "s1 += s3\n",
    "\n",
    "print(s1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Other functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Test if values are contained within segments\n",
    "s = seg.Segment([[1, 10], [30, 40]])\n",
    "\n",
    "if 5 in s:\n",
    "    print(\"Yup, segments contain 5\")\n",
    "\n",
    "# the underlying contains can be used to test for a vector of values. Note that both the segments and the vector\n",
    "# need to be sorted!\n",
    "a, b, c = s.contains([1, 4, 6, 15, 50])\n",
    "\n",
    "# The contains method returns three parameters:\n",
    "# a : vector, True for each input value that is contained within the segments\n",
    "# b : vector, for each segment the number of input values it contains\n",
    "# c : (n,2) array, for each segment the start and end indices of the input values that are contained within\n",
    "#    the segments ([-1 -1] for segments that do not contain any values)\n",
    "print(a, b, c)\n",
    "\n",
    "?s.contains"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sorting\n",
    "s = seg.Segment([[4, 6], [1, 3], [2, 5]])\n",
    "print(s)\n",
    "s.sort()  # in place sorting\n",
    "print(s)\n",
    "\n",
    "# remove overlap between segments\n",
    "s.removeoverlap()  # in place\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# count how many segments contain a value\n",
    "s = seg.Segment([[1, 4], [2, 6], [8, 10]])\n",
    "\n",
    "s.count(np.array([1.5, 3, 9, 15]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# determine overlap between segments\n",
    "s1 = seg.Segment([[1, 5], [3, 10], [15, 20]])\n",
    "s2 = seg.Segment([[8, 17]])\n",
    "\n",
    "a, b, c = s1.overlap()  # determine overlap between segments within Segment object\n",
    "# where:\n",
    "# a : absolute overlap between all combinations of segments\n",
    "# b : overlap relative to duration of first segment\n",
    "# c : overlap relative to duration of second segment\n",
    "\n",
    "a, b, c = s1.overlap(s2)  # determine overlap between segments across Segment objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# convert to indices into vector\n",
    "s = seg.Segment([[2, 4], [6, 10]])\n",
    "v = np.linspace(0, 12, 48)\n",
    "\n",
    "a = s.asindex(v)  # will return new Segment object\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# join segments with small inter-segment gap\n",
    "s = seg.Segment([[1, 10], [11, 15], [17, 20], [23, 30], [30.5, 40]])\n",
    "\n",
    "s.join(gap=1)  # will return new Segment object\n",
    "\n",
    "s.ijoin(gap=1)  # in place\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# split segments into smaller bins\n",
    "s = seg.Segment([[1, 10], [20, 25]])\n",
    "\n",
    "s.split(size=0.1)  # size (duration) of all new segments is 0.1\n",
    "\n",
    "# split segments into smaller segments with overlap - overlap is given in fraction (0-1)\n",
    "s.split(size=0.1, overlap=0.5)\n",
    "\n",
    "# return list of new Segment objects that contain the splits for each original segment\n",
    "s.split(size=1, join=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# apply function to each segment\n",
    "# applyfcn(self,x,*args,**kwargs)\n",
    "s = seg.Segment([[2, 5], [10, 14], [20, 25]])\n",
    "x = np.arange(start=0, stop=30, step=0.1)\n",
    "y = np.sin(x) ** 2\n",
    "z = np.cos(x) ** 2\n",
    "\n",
    "# by default the len function is applied to data contained in segments\n",
    "# thus returning the number of samples in x that are contained within segments\n",
    "s.applyfcn(x)\n",
    "\n",
    "# to apply function separately to each segment (returns a list of results):\n",
    "s.applyfcn(x, separate=True)\n",
    "\n",
    "# to apply a different function:\n",
    "s.applyfcn(x, function=np.mean)\n",
    "\n",
    "# to apply function to all values in y for which the corresponding x is contained within a segment\n",
    "# the following example returns the mean y-value for each segment\n",
    "s.applyfcn(x, y, function=np.mean, separate=True)\n",
    "\n",
    "# this examples computes the mean difference between y and z for each segment\n",
    "# equivalent to: np.mean( z[idx]- y[idx] ), where idx represent all samples such that x[idx] is contained within a segment\n",
    "s.applyfcn(x, y, z, function=lambda arg1, arg2: np.mean(arg2 - arg1), separate=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# partition segments into groups\n",
    "# the last group may have fewer segments than requested\n",
    "# note that the method returns a generator\n",
    "\n",
    "s = seg.Segment(\n",
    "    [\n",
    "        [1, 3],\n",
    "        [4, 5],\n",
    "        [7, 10],\n",
    "        [12, 15],\n",
    "        [16, 17],\n",
    "        [20, 30],\n",
    "        [35, 45],\n",
    "        [60, 63],\n",
    "        [70, 77],\n",
    "        [90, 99],\n",
    "    ]\n",
    ")\n",
    "\n",
    "# partition into blocks of 3 segments\n",
    "s.partition(partsize=3)\n",
    "s.partition(partsize=3, method=\"block\")\n",
    "\n",
    "# partition into sets of 3 segments, where each segment is randomly drawn (without replacement) from original segments\n",
    "s.partition(partsize=3, method=\"random\")\n",
    "\n",
    "# partition into sets of three segments, where each segment\n",
    "s.partition(partsize=3, method=\"sequence\")\n",
    "\n",
    "# to partition into a fixed number of groups:\n",
    "s.partition(nparts=4)\n",
    "\n",
    "# for more information see fklab.utilities.general.partitions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# uniform random sampling in segments\n",
    "# uniform_random(self,size=(1,))\n",
    "s = seg.Segment([[3, 7], [10, 12], [20, 25]])\n",
    "\n",
    "s.uniform_random(size=(10,))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

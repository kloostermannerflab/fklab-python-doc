{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Circular statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `fklab.statistics.circular` module contains a number of functions to deal with circular data (e.g. angles). For more information see [wikipedia](https://en.wikipedia.org/wiki/Directional_statistics) or look up textbooks \"Statistical Analysis of Circular Data\" by Fisher or \"Directional Statistics\" by Mardia and Jupp.\n",
    "\n",
    "Below, a brief demonstration of a few of the utilities in the `fklab.statistics.circular` module are provided. **Note that functions expect angles in radians (not degrees).** To convert between radians and degrees, use the `deg2rad` and `rad2deg` functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.statistics.circular as circ\n",
    "\n",
    "import seaborn as sns\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## utilities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Angles (in radians) are generally mapped to the intervals [0, 2$\\pi$) or [-$\\pi$, $\\pi$). Values outside these intervals are perfectly valid, but in many cases we would like to ensure that values are explicitly within the interval. For this, one can use the `wrap` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# map values to interval [0, 2*pi)\n",
    "print(circ.wrap([0.0, -0.5 * np.pi, -np.pi, -1.5 * np.pi]))\n",
    "\n",
    "# map values to interval [-pi, pi)\n",
    "print(circ.wrap([0.0, -0.5 * np.pi, -np.pi, -1.5 * np.pi], low=-np.pi, high=np.pi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## circular probability distributions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `fklab.statistic.circular` module defines two probability distributions on the circle: the uniform distribution and the Von Mises distribution. For each distribution one can draw random samples (`rvs` method) or compute the probability density function (PDF) and cumulative density function (CDF) at given angles (`pdf` and `cdf` methods).\n",
    "\n",
    "The [Von Mises distribution](https://en.wikipedia.org/wiki/Von_Mises_distribution) defines a unimodal distribution that is parameterized by a mean angle $\\mu$ and a concentration parameter $\\kappa$. The concentration parameter defines the width of the distribution - in the limit of $\\kappa\\to0$, the Von Mises distribution is equivalent to the circular uniform distribution, and in the limit of $\\kappa\\to\\infty$, the Von Mises distribution is equivalent to a Gaussian distribution with variance $\\sigma^2=\\frac{1}{\\kappa}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's sample from circular distributions\n",
    "\n",
    "# first, from a circular uniform distribution\n",
    "c1 = circ.uniform.rvs(size=200)\n",
    "\n",
    "# and second, from a von mises distribution\n",
    "# with mean=0.5*pi and kappa=1.\n",
    "c2 = circ.vonmises.rvs(size=200, kappa=1.0, loc=0.5 * np.pi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## summary statistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute mean angle and vector length\n",
    "mu1, rbar1, _ = circ.mean(c1)\n",
    "mu2, rbar2, _ = circ.mean(c2)\n",
    "\n",
    "median1, _, _ = circ.median(c1)\n",
    "median2, _, _ = circ.median(c2)\n",
    "\n",
    "print(\n",
    "    \"The first distribution has mean angle {0:0.1f} pi, median angle {1:0.1f} pi and vector length {2:0.1f}.\".format(\n",
    "        mu1 / np.pi, median1 / np.pi, rbar1\n",
    "    )\n",
    ")\n",
    "print(\n",
    "    \"The second distribution has mean angle {0:0.1f} pi, median angle {1:0.1f} pi and vector length {2:0.1f}.\".format(\n",
    "        mu2 / np.pi, median2 / np.pi, rbar2\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## plot histograms of data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# construct figure and polar axes\n",
    "fig = plt.figure(figsize=(9, 9))\n",
    "ax1 = fig.add_axes([0.1, 0.1, 0.35, 0.8], polar=True)\n",
    "ax2 = fig.add_axes([0.55, 0.1, 0.35, 0.8], polar=True)\n",
    "\n",
    "# compute histogram\n",
    "h1 = circ.hist(c1)\n",
    "h2 = circ.hist(c2)\n",
    "\n",
    "# plot bars\n",
    "ax1.bar(h1[1], h1[0], h1[2], align=\"center\", alpha=0.5)\n",
    "ax2.bar(h2[1], h2[0], h2[2], align=\"center\", alpha=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## fit Von Mises distribution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# fit a von mises distribution to the random data\n",
    "# the fscale=1. argument is required for the fit to work properly\n",
    "vm_kappa1, vm_mu1, _ = circ.vonmises.fit(c1, fscale=1.0)\n",
    "vm_kappa2, vm_mu2, _ = circ.vonmises.fit(c2, fscale=1.0)\n",
    "\n",
    "print(\n",
    "    \"The first data set is best fit with a Von Mises distribution with mean={0:0.1f} pi and kappa={1:0.1f}\".format(\n",
    "        vm_mu1 / np.pi, vm_kappa1\n",
    "    )\n",
    ")\n",
    "print(\n",
    "    \"The second data set is best fit with a Von Mises distribution with mean={0:0.1f} pi and kappa={1:0.1f}\".format(\n",
    "        vm_mu2 / np.pi, vm_kappa2\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## plot kernel density estimates of data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# construct figure and polar axes\n",
    "fig = plt.figure(figsize=(9, 5))\n",
    "ax1 = fig.add_axes([0.1, 0.1, 0.35, 0.8], polar=True)\n",
    "ax2 = fig.add_axes([0.55, 0.1, 0.35, 0.8], polar=True)\n",
    "\n",
    "# compute kernel density with default kernel bandwidth = 5.\n",
    "k1 = circ.kde(c1)\n",
    "k2 = circ.kde(c2)\n",
    "\n",
    "# plot density\n",
    "ax1.fill(k1[1], k1[0], closed=True, alpha=0.5)\n",
    "ax2.fill(k2[1], k2[0], closed=True, alpha=0.5)\n",
    "\n",
    "# plot mean vector\n",
    "ax1.annotate(\n",
    "    \"\",\n",
    "    xy=(mu1, rbar1),\n",
    "    xytext=(0.0, 0.0),\n",
    "    arrowprops=dict(arrowstyle=\"->\", color=\"red\", linewidth=1),\n",
    ")\n",
    "ax2.annotate(\n",
    "    \"\",\n",
    "    xy=(mu2, rbar2),\n",
    "    xytext=(0.0, 0.0),\n",
    "    arrowprops=dict(arrowstyle=\"->\", color=\"red\", linewidth=1),\n",
    ")\n",
    "\n",
    "# plot best fitting von mises distribution\n",
    "theta = np.arange(48) * 2.0 * np.pi / 48\n",
    "ax1.fill(\n",
    "    theta,\n",
    "    circ.vonmises.pdf(theta, kappa=vm_kappa1, loc=vm_mu1),\n",
    "    closed=True,\n",
    "    linewidth=1,\n",
    "    linestyle=\"--\",\n",
    "    edgecolor=\"black\",\n",
    "    facecolor=\"none\",\n",
    ")\n",
    "ax2.fill(\n",
    "    theta,\n",
    "    circ.vonmises.pdf(theta, kappa=vm_kappa2, loc=vm_mu2),\n",
    "    closed=True,\n",
    "    linewidth=1,\n",
    "    linestyle=\"--\",\n",
    "    edgecolor=\"black\",\n",
    "    facecolor=\"none\",\n",
    ")\n",
    "\n",
    "# set radial axes limits\n",
    "ax1.set_rlim(0, 0.5)\n",
    "ax2.set_rlim(0, 0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## statistical tests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# test if sample differs significantly from a uniform distribution\n",
    "\n",
    "p1, _ = circ.kuiper(c1)\n",
    "p2, _ = circ.kuiper(c2)\n",
    "\n",
    "print(\n",
    "    \"Kuiper test: dataset 1 is {0}significantly different from a uniform distribution.\".format(\n",
    "        \"not \" if p1 > 0.05 else \"\"\n",
    "    )\n",
    ")\n",
    "print(\n",
    "    \"Kuiper test: dataset 2 is {0}significantly different from a uniform distribution.\".format(\n",
    "        \"not \" if p2 > 0.05 else \"\"\n",
    "    )\n",
    ")\n",
    "\n",
    "# an alternative test is the rayleigh test, which tests against a unimodal alternative\n",
    "p1, _ = circ.rayleigh(c1)\n",
    "p2, _ = circ.rayleigh(c2)\n",
    "\n",
    "print(\n",
    "    \"Rayleigh test: dataset 1 is {0}significantly different from a uniform distribution.\".format(\n",
    "        \"not \" if p1 > 0.05 else \"\"\n",
    "    )\n",
    ")\n",
    "print(\n",
    "    \"Rayleigh test: dataset 2 is {0}significantly different from a uniform distribution.\".format(\n",
    "        \"not \" if p2 > 0.05 else \"\"\n",
    "    )\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

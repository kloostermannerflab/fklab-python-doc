{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "83e39055",
   "metadata": {},
   "source": [
    "# Binned statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c911f19",
   "metadata": {},
   "source": [
    "Binned statistics are a generalization of histograms in which a custom function is applied to the data in single bins, rather than counting the number of samples in a bin.\n",
    "\n",
    "As an example, suppose we have measured the (x,y) location and run speed of an animal that explores an open arena. If we are interested in studying the relation between position and run speed, we may want to compute the expected (mean) speed as a function of position. One approach we could use is to partition the (x,y) location into small bins and compute the average speed for all samples within each bin. This is the basic idea of binned statistics.\n",
    "\n",
    "The concept can be extended to non-scalar variables, for example power spectra of recorded brain signals, and to more complex functions than the mean used above.\n",
    "\n",
    "The scipy library ships with several functions to compute binned statistics for [1d](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binned_statistic.html), [2d](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binned_statistic_2d.html) and [nd](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binned_statistic_dd.html) variables. These functions work well for most applications, but more advanced use cases are not supported. For example, it is not possible for custom functions to return arrays (instead of scalars) or to take multiple arguments. For these cases, there is an alternative available in the fklab package: `map_array`.\n",
    "\n",
    "A few examples of how to use the functions in scipy and fklab to computed binned statistics are shown below."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21e5aecf",
   "metadata": {},
   "source": [
    "## imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0723e4f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.stats\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "import fklab.statistics.core"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c3b4437",
   "metadata": {},
   "source": [
    "## Example 1: mean running speed as a function of 1d position"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de09519a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate data\n",
    "\n",
    "# random 1d position data on a linear track\n",
    "position = np.random.uniform(0, 100, size=(1000,))\n",
    "\n",
    "# speed is high in the center and low towards the edges of the track\n",
    "speed = np.random.normal(50 - np.abs(position - 50), 5)\n",
    "\n",
    "# define position bins\n",
    "bins = np.linspace(0, 100, 21)\n",
    "\n",
    "bin_centers = (bins[:-1] + bins[1:]) / 2.0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78f1c842",
   "metadata": {},
   "source": [
    "### Scipy's binned_statistic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0b4167e",
   "metadata": {},
   "outputs": [],
   "source": [
    "mu, bins, _ = scipy.stats.binned_statistic(position, speed, bins=bins)\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "ax.plot(bin_centers, mu)\n",
    "ax.set(xlabel=\"position [cm]\", ylabel=\"speed [cm/s]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d9b785b",
   "metadata": {},
   "source": [
    "### FKLab's map_array\n",
    "\n",
    "The `map_array` function supports this simple use case, but it requires a few more lines of code (in particular two support functions). For this reason, you may want to use scipy's `binned_statistic` instead.\n",
    "\n",
    "In a first step, the bin indices for data samples need to be computed using `bin_array`. Note that it is possible that not all data samples fall in a bin. The `bin_array` function returns a `valid` argument that can be used to select the valid data samples.\n",
    "\n",
    "Next, the (valid) indices and variable(s) of interest for which one wants to compute the statistic are passed to `map_array`. A custom function is then applied to the variable data in each bin.\n",
    "\n",
    "The `map_array` function only returns the statistic for bins that contain data samples. If the bins form a regular grid (as usually is the case), then a step is needed to construct a full array with all bins. Therefore, in a final step, the computing statistic is mapped back onto a full binned array using `construct_array`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a24cd8e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "index, valid, bin_edges, shape = fklab.statistics.core.bin_array(position, bins=bins)\n",
    "\n",
    "mu, groups = fklab.statistics.core.generate_full_binned_array(\n",
    "    index[valid], speed[valid], fcn=np.mean\n",
    ")\n",
    "\n",
    "result = fklab.statistics.core.transform_binned_array(\n",
    "    mu, groups, shape=shape, fill_value=np.nan\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "ax.plot(bin_centers, result)\n",
    "ax.set(xlabel=\"position [cm]\", ylabel=\"speed [cm/s]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a8c66af",
   "metadata": {},
   "source": [
    "## Example 2: mean running speed as a function of 2D position"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "941868e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate data\n",
    "\n",
    "# random 2d position data on a linear track\n",
    "position = np.random.uniform(-50, 50, size=(1000, 2))\n",
    "\n",
    "# speed is high in the center and low towards the edges of the arena\n",
    "speed = np.random.normal(70 - np.sqrt(np.sum(position**2, axis=1)), 5)\n",
    "\n",
    "# define position bins\n",
    "bins = np.linspace(-50, 50, 21)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79872c7d",
   "metadata": {},
   "source": [
    "### Scipy's binned_statistic2d"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34b50757",
   "metadata": {},
   "outputs": [],
   "source": [
    "mu, x_bins, y_bins, _ = scipy.stats.binned_statistic_2d(\n",
    "    position[:, 0], position[:, 1], speed, bins=(bins, bins)\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "\n",
    "ax.imshow(mu, origin=\"lower\")\n",
    "ax.set(xlabel=\"x bin index\", ylabel=\"y bin index\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2b07f47",
   "metadata": {},
   "source": [
    "### FKLab's map_array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f725720",
   "metadata": {},
   "outputs": [],
   "source": [
    "index, valid, bin_edges, shape = fklab.statistics.core.bin_array(position, bins=bins)\n",
    "\n",
    "mu, groups = fklab.statistics.core.generate_full_binned_array(\n",
    "    index[valid], speed[valid], fcn=np.mean\n",
    ")\n",
    "\n",
    "result = fklab.statistics.core.transform_binned_array(\n",
    "    mu, groups, shape=shape, fill_value=np.nan\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "\n",
    "ax.imshow(result, origin=\"lower\")\n",
    "ax.set(xlabel=\"x bin index\", ylabel=\"y bin index\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3193cf9",
   "metadata": {},
   "source": [
    "## Example 3: mean spectrum as a function of 1d position\n",
    "\n",
    "In this example, we would like to compute the average power spectrum for each position bin. Because the output of our reduction function is not a scalar, we **cannot** use scipy's `binned_statistic`. However, this can be easily done using `map_array`. Note that the custom function will compute the average spectrum across all samples in a bin, but it should keep the first dimension (of size 1 after computing the average) intact. For this reason, we add the `keepdims=True` argument to `np.mean` in the custom function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82a10983",
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate data\n",
    "\n",
    "# random 1d position data on a linear track\n",
    "position = np.random.uniform(0, 100, size=(1000,))\n",
    "\n",
    "# speed is high in the center and low towards the edges of the track\n",
    "speed = np.random.normal(50 - np.abs(position - 50), 5)\n",
    "\n",
    "# define position bins\n",
    "bins = np.linspace(0, 100, 21)\n",
    "\n",
    "# let's simulate power spectra with a peak frequency that varies with run speed\n",
    "spectra = scipy.stats.norm.pdf(\n",
    "    np.linspace(0, 100, 101)[None, :], loc=speed[:, None], scale=5\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0dd3da5d",
   "metadata": {},
   "outputs": [],
   "source": [
    "index, valid, bin_edges, shape = fklab.statistics.core.bin_array(position, bins=bins)\n",
    "\n",
    "mu, groups = fklab.statistics.core.generate_full_binned_array(\n",
    "    index[valid], spectra[valid], fcn=lambda x: np.mean(x, axis=0, keepdims=True)\n",
    ")\n",
    "\n",
    "result = fklab.statistics.core.transform_binned_array(\n",
    "    mu, groups, shape=shape, fill_value=np.nan\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "\n",
    "ax.imshow(\n",
    "    result.T,\n",
    "    aspect=\"auto\",\n",
    "    origin=\"lower\",\n",
    "    extent=[bin_edges[0][0], bin_edges[0][-1], 0, 100],\n",
    ")\n",
    "ax.set(xlabel=\"position [cm]\", ylabel=\"frequency [Hz]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "614f5299",
   "metadata": {},
   "source": [
    "## Example 4: z-scoring within bins (mapping operation)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f127f50a",
   "metadata": {},
   "source": [
    "The previous example showed how `map_array` can be used for \"pure\" reduction of all samples inside a bin to a single (e.g. mean) sample. In addition, `map_array` function also supports partial reduction and mapping operations. In partial reduction, the custom function may return more than a single sample. In a mapping operation, the data samples are transformed per bin, but the number of samples does not change.\n",
    "\n",
    "Below, we show how one can z-score data per bin, as an example of a mapping operation. Note that for a mapping operation (or partial reduction) you do not use the `construct_array` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f568938b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate data\n",
    "\n",
    "# random 1d position data on a linear track\n",
    "position = np.random.uniform(0, 100, size=(1000,))\n",
    "\n",
    "# speed is high in the center and low towards the edges of the track\n",
    "speed = np.random.normal(50 - np.abs(position - 50), 5)\n",
    "\n",
    "# define position bins\n",
    "bins = np.linspace(0, 100, 21)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f87e7990",
   "metadata": {},
   "outputs": [],
   "source": [
    "index, valid, bin_edges, shape = fklab.statistics.core.bin_array(position, bins=bins)\n",
    "\n",
    "speed_zscore, groups = fklab.statistics.core.generate_full_binned_array(\n",
    "    index[valid], speed[valid], fcn=lambda x: (x - np.mean(x)) / np.std(x)  # z-score\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa768887",
   "metadata": {},
   "source": [
    "To visualize the result, we plot the speed as a function of position in a scatter plot, both before and after the mapping operation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3aa76a8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 2, figsize=(8, 4))\n",
    "\n",
    "ax[0].plot(position[valid], speed[valid], \"k.\")\n",
    "ax[0].set(xlabel=\"position [cm]\", ylabel=\"speed  [cm/s]\", title=\"before\")\n",
    "\n",
    "ax[1].plot(position[valid], speed_zscore, \"k.\")\n",
    "ax[1].set(xlabel=\"position [cm]\", ylabel=\"z-score\", title=\"after\");"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

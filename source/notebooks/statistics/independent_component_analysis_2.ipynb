{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Independent Component Analysis (ICA) - part 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will decompose laminar extracellular potential recordings from the hippocampus using independent component analysis. The main assumption is that the observed extracellular potential is a mixture of multiple underlying sources that have a characteristic spatial profile (e.g. corresponding to activation of a certain hippocampal pathway) and non-Gaussian distribution. For more background information, see the following papers by Oscar Herreras and colleagues: [paper 1](https://www.ncbi.nlm.nih.gov/pubmed/20094907), [paper 2](https://www.ncbi.nlm.nih.gov/pubmed/26415769)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## module import"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.signal\n",
    "\n",
    "import h5py\n",
    "import fklab.signals.multirate\n",
    "import fklab.signals.filter\n",
    "\n",
    "import fklab.signals.ica\n",
    "\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "\n",
    "import fklab.plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## load data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the dataset example- if you don't have yet access token, ask for one to the fklab maintainer first!\n",
    "from fklab.io.cloud import dropboxAPI\n",
    "import intake\n",
    "\n",
    "dbx = dropboxAPI()\n",
    "\n",
    "source = intake.open_numpy(\n",
    "    \"dropbox:///Data/ica/ica_data.npy\",\n",
    "    storage_options={\"token\": dbx.load_token()},\n",
    ")\n",
    "data = source.read()\n",
    "\n",
    "# The dataset contain :\n",
    "# - a numpy dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 30  # number of seconds to load\n",
    "fs_orig = 30000  # sampling frequency\n",
    "\n",
    "# decimate 15x\n",
    "fs = 2000.0\n",
    "data = fklab.signals.multirate.decimate(\n",
    "    data, int(fs_orig / fs), axis=0, zero_phase=True\n",
    ")\n",
    "nsamples, nchannels = data.shape\n",
    "time_vector = np.arange(nsamples) / fs\n",
    "\n",
    "# optionally: low-pass filter data\n",
    "data = fklab.signals.filter.apply_filter(\n",
    "    data, band=300, axis=0, fs=2000.0, transition_width=40, attenuation=60\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spacing = -6 * (np.median(np.var(data, axis=0) ** 0.5))\n",
    "plt.figure(figsize=(15, 8))\n",
    "fklab.plot.plot_signals(time_vector, data, spacing=spacing, colormap=\"Dark2\")\n",
    "plt.xlim(0, nsamples / fs)\n",
    "plt.ylim(nchannels * spacing, -spacing)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## select number of components"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's select the number of components that explain 99.9% of the variance\n",
    "ncomp = fklab.signals.ica.ncomp(data, p=99.9, plot=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## perform ica decomposition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# construct ICA object\n",
    "ica = fklab.signals.ica.FastICA(ncomp, random_state=0)\n",
    "\n",
    "# run ICA\n",
    "components = ica.fit_transform(data)\n",
    "\n",
    "# total normalized explained variance of the components to all signals\n",
    "# explained_variance = np.sum( ica.mixing_**2, axis=0) * np.var( components, axis=0 )\n",
    "# explained_variance = explained_variance / np.sum(explained_variance)\n",
    "\n",
    "explained_variance = fklab.signals.ica.explained_variance(\n",
    "    components, ica.mixing_, normalize=True\n",
    ")\n",
    "\n",
    "# sorting vector by explained variance\n",
    "index = np.argsort(explained_variance)[::-1]\n",
    "\n",
    "plt.plot(range(1, ncomp + 1), 100 * explained_variance[index], \"o-\")\n",
    "plt.ylabel(\"Explained variance (%)\")\n",
    "plt.xlabel(\"Sorted component\")\n",
    "plt.title(\"Explained variance for independent components\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## inspect components"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 2, figsize=(16, 8), gridspec_kw={\"width_ratios\": [1, 5]})\n",
    "\n",
    "spacing = -np.max(np.abs(ica.mixing_))\n",
    "fklab.plot.plot_signals(\n",
    "    ica.mixing_[:, index], spacing=spacing, colormap=\"Dark2\", axes=ax[0]\n",
    ")\n",
    "ax[0].set_title(\"spatial loadings\")\n",
    "ax[0].set_xlim(-1, nchannels)\n",
    "ax[0].set_ylim(ncomp * spacing, -spacing)\n",
    "ax[0].set_xlabel(\"channel\")\n",
    "ax[0].set_ylabel(\"components\")\n",
    "ax[0].set_yticks(spacing * np.arange(ncomp))\n",
    "ax[0].set_yticklabels(np.arange(ncomp))\n",
    "\n",
    "spacing = -6 * (np.var(components[:, 0]) ** 0.5)\n",
    "fklab.plot.plot_signals(\n",
    "    time_vector, components[:, index], spacing=spacing, colormap=\"Dark2\", axes=ax[1]\n",
    ")\n",
    "ax[1].set_xlim(0, nsamples / fs)\n",
    "ax[1].set_ylim(ncomp * spacing, -spacing)\n",
    "ax[1].set_title(\"time courses\")\n",
    "ax[1].set_xlabel(\"time [s]\")\n",
    "ax[1].set_yticks([])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## detailed look at individual components"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### component 8 : ripples?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fklab.signals.ica.inspect_component(\n",
    "    time_vector,\n",
    "    components,\n",
    "    ica,\n",
    "    component=8,\n",
    "    time_window=[17, 18],\n",
    "    upper_freq=300,\n",
    "    order=index,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### component 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fklab.signals.ica.inspect_component(\n",
    "    time_vector,\n",
    "    components,\n",
    "    ica,\n",
    "    component=1,\n",
    "    time_window=[10, 20],\n",
    "    upper_freq=100,\n",
    "    order=index,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### component 6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fklab.signals.ica.inspect_component(\n",
    "    time_vector,\n",
    "    components,\n",
    "    ica,\n",
    "    component=6,\n",
    "    time_window=[10, 20],\n",
    "    upper_freq=50,\n",
    "    order=index,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### component 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fklab.signals.ica.inspect_component(\n",
    "    time_vector,\n",
    "    components,\n",
    "    ica,\n",
    "    component=0,\n",
    "    time_window=[15, 20],\n",
    "    upper_freq=50,\n",
    "    order=index,\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

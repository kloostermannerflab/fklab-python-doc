{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false
   },
   "source": [
    "# Rate maps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Generally speaking, rate maps (or tuning curves) express the event or spike rate as a function of the stimulus condition. For example, the stimulus could be the (x,y) location of a subject in a 2D environment. In this example, the stimulus is really a behavioral variable that is measured along with the spiking data. In other instances, stimuli are controlled by the experimenter (e.g. a visual stimulus).\n",
    "\n",
    "A rate map is computed according to:\n",
    "$$spikerate(stimulus)=\\frac{spikecount(stimulus)}{duration(stimulus)}$$\n",
    "\n",
    "Here, *spikecount* represents the number of spikes emitted for a particular stimulus condition, and *duration* (or occupancy) is the total time spent in that stimulus condition.\n",
    "\n",
    "For experiments with discrete trials, one may write:\n",
    "\n",
    "$$spikerate(stimulus)=\\frac{\\sum_{trials}{spikecount_{trial}(stimulus)}}{\\sum_{trials}{duration_{trial}(stimulus)}}$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below, two methods for computing rate maps are described: histogram and density based rate maps. In both methods, *spikecount* and *duration* are computed separately and used to construct the final spike rate map.\n",
    "\n",
    "In this notebook we mainly consider a stimulus that continuously varies over time (e.g. location of an animal on a track). The stimulus can be multi-dimensional (e.g. joint stimulus of speed and running direction)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Construct test data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test the rate map functions, we need example data. In this section, we define functions for:\n",
    "\n",
    "1. the generation of a subject's movement trajectory inside an arena according to some model(s) (e.g. a correlated random walk model). \n",
    "1. the definition of a tuning curve as a function of some behavioral stimulus variable (e.g. place tuning curve)\n",
    "1. the generation of inhomogeneous Poisson spike trains in which the rate varies with the behavioral stimulus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulate movement trajectory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.statistics.circular as circ\n",
    "from fklab.behavior.simulation import (\n",
    "    random_walk,\n",
    "    generate_trajectory,\n",
    "    circle_boundary,\n",
    "    custom_turn_function,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "thigmotaxis = 0.5\n",
    "\n",
    "distance_grid = np.linspace(0, 1, 11)\n",
    "angle_grid = np.linspace(-np.pi, np.pi, 21)\n",
    "d, a = np.meshgrid(distance_grid, angle_grid)\n",
    "\n",
    "fig, ax = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)\n",
    "\n",
    "mu = (\n",
    "    thigmotaxis\n",
    "    * circ.diff(0.5 * np.pi, np.abs(a), directed=True)\n",
    "    * (1 - d)\n",
    "    * np.sign(a)\n",
    ")\n",
    "\n",
    "kappa = 1 - (1 - d) * circ.diff(0.5 * np.pi, np.abs(a)) / (0.5 * np.pi)\n",
    "\n",
    "# compute the extent\n",
    "# the distance and angle grids specify the center of the image pixels\n",
    "# but for imshow, we need to specify the lower and upper edges of the image\n",
    "extent = [\n",
    "    *(distance_grid[[0, -1]] + np.diff(distance_grid)[[0, -1]] * [-0.5, 0.5]),\n",
    "    *(angle_grid[[0, -1]] + np.diff(angle_grid)[[0, -1]] * [-0.5, 0.5]),\n",
    "]\n",
    "\n",
    "z1 = ax[0].imshow(mu, extent=extent, cmap=\"PiYG\", aspect=\"auto\")\n",
    "z2 = ax[1].imshow(kappa, extent=extent, aspect=\"auto\")\n",
    "\n",
    "ax[0].set(\n",
    "    xlabel=\"distance (norm)\",\n",
    "    ylabel=\"angle [rad]\",\n",
    "    yticks=np.linspace(-np.pi, np.pi, 5),\n",
    "    yticklabels=[r\"$-\\pi$\", r\"$-\\frac{\\pi}{2}$\", \"0\", r\"$\\frac{\\pi}{2}$\", r\"$\\pi$\"],\n",
    ")\n",
    "\n",
    "ax[1].set(xlabel=\"distance (norm)\", yticks=np.linspace(-np.pi, np.pi, 5))\n",
    "\n",
    "plt.colorbar(z1, ax=ax[0], label=\"$\\mu$\")\n",
    "plt.colorbar(z2, ax=ax[1], label=\"$\\kappa$ scale factor\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's simulate a movement trajectory with two models: one that represent running and another model that represent \"rest\". The two differences between the models are the speed parameter of the random walk (10 cm/s vs 1 cm/s) and the turn parameter of the custom turn function (1 turns/s vs 3 turns/s; i.e. less turning while running). The subject moves around in a 1-m diameter circular environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_model = random_walk(\n",
    "    dt=1 / 20,\n",
    "    boundary=circle_boundary(50),\n",
    "    speed=10,\n",
    "    turn=custom_turn_function(10, turn=1, dt=1 / 20, thigmotaxis=0.3),\n",
    "    maxiter=1000,\n",
    ")\n",
    "\n",
    "stop_model = random_walk(\n",
    "    dt=1 / 20,\n",
    "    boundary=circle_boundary(50),\n",
    "    speed=1.0,\n",
    "    turn=custom_turn_function(10, turn=3, dt=1 / 20, thigmotaxis=0.0),\n",
    "    maxiter=1000,\n",
    ")\n",
    "\n",
    "# not let's generate the movement trajectory\n",
    "# we simulate 40k steps at 50 ms per step, i.e. a little over 30 minutes\n",
    "# model switching can only occur every 20 steps\n",
    "t, x, y, a, m = generate_trajectory(\n",
    "    [run_model, stop_model],\n",
    "    nsteps=40000,\n",
    "    transitions=[[0.99, 0.01], [0.01, 0.99]],\n",
    "    batch=20,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the simulated trajectory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib.gridspec import GridSpec\n",
    "\n",
    "fig = plt.figure(constrained_layout=True, figsize=(12, 5))\n",
    "\n",
    "gs = GridSpec(2, 2, figure=fig, width_ratios=[2, 1])\n",
    "ax1 = fig.add_subplot(gs[0, 0])\n",
    "ax2 = fig.add_subplot(gs[1, 0], sharex=ax1)\n",
    "ax3 = fig.add_subplot(gs[:, 1])\n",
    "\n",
    "ax1.step(m[:, 0], m[:, 1], where=\"post\")\n",
    "ax1.set(ylabel=\"model\", yticks=[0, 1], yticklabels=[\"run\", \"rest\"])\n",
    "\n",
    "ax2.plot(t, x, label=\"x\")\n",
    "ax2.plot(t, y, label=\"y\")\n",
    "ax2.set(ylabel=\"location [cm]\", xlabel=\"time [s]\")\n",
    "ax2.legend(loc=\"lower right\")\n",
    "\n",
    "ax3.plot(x, y, \"k\", alpha=0.5)\n",
    "ax3.axis(\"equal\")\n",
    "ax3.set(xlabel=\"x [cm]\", ylabel=\"y [cm]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "### Simulate spike trains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Spike trains are modeled as inhomogeneous Poisson processes for which the rate varies as a function of time, as governed by the underlying tuning of the cell. Let's say that a cell is tuned to behavioral variable *X*, with corresponding tuning curve *R(x)*. For a time-varying X(t), the rate changes as a function of time according to *R(X(t))*. With this rate function we can now simulate an inhomogeneous Poisson spike train."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Generate example place cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.interpolate\n",
    "\n",
    "from fklab.spikes.simulation import (\n",
    "    inhomogeneous_poisson_rescale,\n",
    "    gaussian_2d_place_field,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Place cell with gaussian tuning curve\n",
    "placefield = gaussian_2d_place_field([10, 10], bandwidth=5.0, rate=10.0)\n",
    "rate = placefield(np.column_stack([x, y]))\n",
    "\n",
    "spike_t_place = inhomogeneous_poisson_rescale(rate, x=t)\n",
    "# spike_t_place = inhomogeneous_poisson_thinning(rate, x=t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 1)\n",
    "\n",
    "ax.plot(x, y, \"k\", alpha=0.25)\n",
    "\n",
    "ax.plot(\n",
    "    scipy.interpolate.interp1d(t, x)(spike_t_place),\n",
    "    scipy.interpolate.interp1d(t, y)(spike_t_place),\n",
    "    \"k.\",\n",
    ")\n",
    "\n",
    "ax.axis(\"equal\")\n",
    "ax.set(xlabel=\"x [cm]\", ylabel=\"y [cm]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Generate example heading direction cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from fklab.spikes.simulation import inhomogeneous_poisson_rescale, directional_tuning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hd_tuning = directional_tuning(mu=0.5 * np.pi, kappa=5.0, rate=10.0)\n",
    "\n",
    "# compute heading direction from position data\n",
    "hd = np.arctan2(np.gradient(y, 1 / 20), np.gradient(x, 1 / 20))\n",
    "rate = hd_tuning(hd)\n",
    "\n",
    "spike_t_hd = inhomogeneous_poisson_rescale(rate, x=t)\n",
    "# spike_t_hd = inhomogeneous_poisson_thinning(rate, x=t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 1)\n",
    "\n",
    "ax.quiver(\n",
    "    scipy.interpolate.interp1d(t, x)(spike_t_hd),\n",
    "    scipy.interpolate.interp1d(t, y)(spike_t_hd),\n",
    "    scipy.interpolate.interp1d(t, np.gradient(x, 1 / 20))(spike_t_hd),\n",
    "    scipy.interpolate.interp1d(t, np.gradient(y, 1 / 20))(spike_t_hd),\n",
    "    color=\"k\",\n",
    "    alpha=0.5,\n",
    ")\n",
    "\n",
    "ax.axis(\"equal\")\n",
    "ax.set(xlabel=\"x [cm]\", ylabel=\"y [cm]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Preprocessing of data\n",
    "\n",
    "The raw data usually consists of a continuous record of stimulus samples (e.g. (x,y) positions) and a record of a cell's spike times. Prior to the calculation of a rate map, a number of steps may need to be performed:\n",
    "\n",
    "1. **Missing data and noise** The stimulus data may have missing values or needs to be filtered for noise. Generally, there are two approaches: either all missing/noisy stimulus samples are set to NaN or they are removed from the array. In the latter case, the resulting stimulus array is not sampled at regular intervals anymore (i.e. it contains gaps in time). For either appraoch, one needs to make sure that subsequent processing steps deal properly with gaps and/or NaNs.\n",
    "1. **Select time epochs of interest** Often, one is interested in the data collected during select time windows (e.g. when a subject is running, rather than sitting still). Only part of the stimulus record and spike time record that fall within the time windows are then maintained.\n",
    "1. **Determine stimulus values at spike times** To compute rate maps, one has to determine the stimulus condition at the time of a spike. For a continuous stimulus (e.g. location of subject during maze exploration) that is sampled at time points *t*, the stimulus value at spike times *spike_t* can be easily calculated using interpolation. For a stimulus that is densely sampled in time, nearest neighbor interpolation is sufficient and can be applied more generally to different types of data without additional work (e.g.circular data)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `compute_variable_at_times` function in `fklab.ratemap.utilities` performs step 3 and will properly deal with gaps in the data, NaNs and time window basedselection of the data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Histogram based rate maps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's assume we have a (nstim,ndim) stimulus array (e.g. (x,y) coordinates in an arena) and we have computed the stimulus at the time spikes occurred (using the `compute_variable_at_times` function in `fklab.ratemap.utilities`). A histogram-based ratemap can then be computed as follows:\n",
    "\n",
    "1. Define edges of histogram bins that span the relevant data space for each of the data dimensions.\n",
    "1. Optionally, for non-rectangular data spaces, define which bins are valid. For example, for a circular arena, only (x,y) coordinates inside the circle should be considered. Thus, bins that fall outside the circle are not valid.\n",
    "1. Count the stimulus occupancy for each bin as the number of entries in the stimulus array in each bin multiplied by the duration of each entry. For example, for a record of (x,y) positions that are sampled at 50 Hz, the stimulus duration equals 1/50 seconds. We use the `np.histogramdd` function to do the actual counting.\n",
    "1. Count the number of times a spike occurred in each bin.\n",
    "1. Set any invalid bins to NaN.\n",
    "1. Optionally, smooth spike count and occupancy separately.\n",
    "1. Compute the rate map as $\\frac{spike\\ count}{occupancy}$.\n",
    "\n",
    "The general `ratemap` function in `fklab.ratemap.hist` implements these steps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### General rate map function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is a general function for construction N-dimensional rate maps from event and stimulus data. It has support for smoothing of the rate map.\n",
    "\n",
    "**Important note** Computing a histogram-based ratemap with *fklab.ratemap.hist.ratemap* assumes that the data space is linear. If one or more of the data dimensions are not linear, then please note the following:\n",
    "\n",
    "**circular data space**\n",
    "\n",
    "For a circular data space (e.g. head direction), the data should be wrapped to a defined circular range (e.g. $[0, 2\\pi)$ or $[-\\pi,+\\pi)$) that matches the bin edges. In other words, the **bin edges should not extend beyond the chosen circular range**. The `fklab.statistics.circular.wrap`  function can be used to make sure that data are mapped to a circular range. **Smoothing will not wrap around the circular range and should not be used**. If smoothing is desired, you should use the kernel-density-based ratemap functions.\n",
    "\n",
    "**discontinuities in data space**\n",
    "\n",
    "In some specific cases, the data space may have discontinuities. An example of such data is the linearized position of a subject on a multi-arm track (i.e. \"graph\" type track), where discontinuities exist at the boundaries between arms. The *fklab.ratemap.hist.ratemap*  function can still be used, as long as **the bin edges respect (and not span across) the discontinuities**. The *bin* method of the graph shape object can be used to construct proper bin edges. **Smoothing should not be used if there are discontinuities in the data space**. If smoothing is desired, you should use the kernel-density-based ratemap functions.\n",
    "\n",
    "**categorical or discrete data space**\n",
    "\n",
    "Discrete or categorical data (e.g. running direction on track) is often encoded as integers. **Bin edges should be specified such that they cover individual categories**. If there are N discrete values encoded as `np.arange(N)`, then bin edges should be: `np.arange(N+1)-0.5`. For example, for discrete values encoded as [0,1,2], the bin edges should be [-0.5,0.5,1.5,2.5]. **Smoothing should never be applied to categorical or discrete data**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from fklab.ratemap.utilities import in_circle\n",
    "import fklab.ratemap as rm\n",
    "from fklab.plot import plot_2d_maps\n",
    "\n",
    "import warnings\n",
    "\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Spatial rate maps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute position at spike times\n",
    "stackxy = np.column_stack([x, y])\n",
    "eventxy = [\n",
    "    rm.compute_variable_at_times(z, variable=stackxy, var_t=t)\n",
    "    for z in [spike_t_place, spike_t_hd]\n",
    "]\n",
    "\n",
    "rmap_hist, _, occ_hist, binedges = rm.hist.ratemap(\n",
    "    *eventxy,\n",
    "    stimdata=stackxy,\n",
    "    stimduration=1 / 20,\n",
    "    smooth=5,\n",
    "    valid=in_circle(50),\n",
    "    bins=[np.linspace(-50, 50, 21), np.linspace(-50, 50, 21)]\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plot_2d_maps(\n",
    "    [*rmap_hist, occ_hist],\n",
    "    coordinates=binedges,\n",
    "    cmap=\"inferno\",\n",
    "    xlabel=\"x [cm]\",\n",
    "    ylabel=\"y [cm]\",\n",
    "    colorlabel=\"firing rate [Hz] / occupancy [s]\",\n",
    "    cbar=True,\n",
    "    grid=(None, 3),\n",
    "    figsize=(14, 3.5),\n",
    "    cmin=0,\n",
    "    cmax=\"auto\",\n",
    "    roundto=0.5,\n",
    ")\n",
    "\n",
    "axes[0, 0].set(title=\"place cell\")\n",
    "axes[0, 1].set(title=\"heading cell\")\n",
    "axes[0, 2].set(title=\"occupancy\")\n",
    "\n",
    "for ax in axes.ravel():\n",
    "    ax.set_frame_on(False)\n",
    "    ax.axis(\"equal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Kernel density based rate maps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Computation of kernel density-based ratemaps makes use of the compressed kernel density estimation in the py-compressed-decoder package.\n",
    "\n",
    "Let's assume we have a (nstim,ndim) stimulus array (e.g. (x,y) coordinates in an arena) and we have computed the stimulus at the time spikes occurred (using the `compute_variable_at_times` function). A kernel density-based ratemap can then be computed as follows:\n",
    "\n",
    "1. Define a N-dimensional kernel Space object that matches the data space and sets the level of smoothing for each dimension. This can be any combination of euclidean (linear), circular, categorical and encoded kernel spaces.\n",
    "1. Create a Grid object that defines points spanning the relevant data space at which the densities are evaluated. For non-rectangular data spaces, the grid definition includes the option to mark grid points as invalid.\n",
    "1. Compute the stimulus occupancy density at each grid point, which is scaled such that the density integrates to the total stimulus time (i.e. `nstim*stimduration`).\n",
    "1. Compute the event count density at each grid point. The density is scaled such that it integrates to the number of events.\n",
    "1. Set density value at invalid grid points to NaN.\n",
    "1. Compute the rate map as $\\frac{event\\ count\\ density}{occupancy\\ density}$.\n",
    "\n",
    "The general `ratemap` function in `fklab.ratemap.kde` implements these steps. However, the kernel Space and Grid objects need to be defined by the user outside this function. For common use cases (e.g. 2d spatial rate map, 1d directional rate map), specialized functions below provide a more convenient interface to the user and will constrict the Space and Grid objects internally."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `fklab.ratemap.kde.ratemap` function computes the occupancy density and the spike count density, which express the occupancy and spike count as seconds per unit space and spikes per unit space, respectively. To obtain an actual occupancy (in seconds) and spike count in a specific region of the data space, the densities need to be integrated. The `local_integral` function in `fklab.ratemap.kde` performs a local integration for each point in the density.\n",
    "\n",
    "Note that once you have computed the local integral, the occupancy and spike count at each point should be interpreted as \"the occupancy and spike count within a local region of space around the point\". This local space generally extends beyond the neigboring points. Or,in other words, the occupancy and spike counts after local integration do *not* sum up to the stimulus time and number of spikes in the original data.\n",
    "\n",
    "I originally though that a conversion from densities to occupancy and spike count would be needed to compute spatial information (`fklab.statistics.information.spatial_information`), but now I am not so sure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Spatial tuning curves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We commonly want to compute a 2D spatial rate map. The function `spatial_2d_tuning` provides a convenient wrapper around `fklab.ratemap.kde.ratemap` for this use case."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test on our example data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rmap, _, occ, samplegrid = rm.kde.linear_2d_tuning(\n",
    "    spike_t_place,\n",
    "    spike_t_hd,\n",
    "    xy=np.column_stack([x, y]),\n",
    "    t=t,\n",
    "    bandwidth=5.0,\n",
    "    grid=[np.linspace(-50, 50, 101), np.linspace(-50, 50, 101)],\n",
    "    valid=in_circle(radius=50),\n",
    "    compression=1.0,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plot_2d_maps(\n",
    "    [*rmap, occ],\n",
    "    coordinates=samplegrid,\n",
    "    cmap=\"inferno\",\n",
    "    xlabel=\"x [cm]\",\n",
    "    ylabel=\"y [cm]\",\n",
    "    colorlabel=\"firing rate [Hz] / occupancy [$s/cm^2$]\",\n",
    "    cbar=True,\n",
    "    grid=(1, 3),\n",
    "    figsize=(14, 3.5),\n",
    "    cmin=0,\n",
    "    cmax=\"auto\",\n",
    "    roundto=0.5,\n",
    ")\n",
    "\n",
    "axes[0, 0].set(title=\"place cell\")\n",
    "axes[0, 1].set(title=\"heading cell\")\n",
    "axes[0, 2].set(title=\"occupancy\")\n",
    "\n",
    "for ax in axes[0, :]:\n",
    "    ax.set_frame_on(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Directional tuning curves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We commonly want to compute a directional rate map. In `fklab.ratemap.kde`, the function `directional_tuning`  provides a convenient wrapper around `ratemap` for this use case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from fklab.plot import plot_1d_maps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hd_rmap, _, hd_occ, samplegrid = rm.kde.directional_tuning(\n",
    "    spike_t_place, spike_t_hd, direction=hd, t=t, npoints=25\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plot_1d_maps(\n",
    "    [*hd_rmap, hd_occ],\n",
    "    x=samplegrid,\n",
    "    grid=(1, 3),\n",
    "    fill_alpha=0.2,\n",
    "    figsize=(12, 4),\n",
    "    vmin=0,\n",
    "    vmax=\"auto\",\n",
    "    roundto=1,\n",
    "    emax=0.15,\n",
    "    xlabel=\"run direction [rad]\",\n",
    ")\n",
    "\n",
    "ax[0, 0].set(title=\"place cell\")\n",
    "ax[0, 1].set(title=\"heading cell\")\n",
    "ax[0, 2].set(title=\"occupancy\")\n",
    "\n",
    "ax[0, 0].set(\n",
    "    ylabel=\"firing rate [Hz] / occupancy [$s/rad$]\",\n",
    "    xticks=np.linspace(0, 2 * np.pi, 5),\n",
    "    xticklabels=[\"0\", r\"$\\frac{\\pi}{2}$\", r\"$\\pi$\", r\"$\\frac{3\\pi}{2}$\", r\"$2\\pi$\"],\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rate map uncertainty"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute the uncertainty of rate maps, once could perform bootstrapping of the confidence interval, i.e. compute multiple rate maps with random selection of the data (i.e. randomly and repeatedly select trials with replacement). However, this may be rather computationally intensive. Instead, it is often better to determine the structure or statistic of interest and device a dedicated statistical test. See next section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Quantification and statistical evaluation of tuning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "### Information theoretic measure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can quantify the information about a stimulus carried by a cell's spikes. According to Skaggs et al. (1993), the information content (in bits per spike) is computed as:\n",
    "\n",
    "$$\\sum_{i=0}^{N_{bins}}{P_i(\\frac{R_i}{R})\\log_2(\\frac{R_i}{R})}$$\n",
    "\n",
    "where $i$ is the bin number, $P_i$ is the probability for occupancy of bin $i$, $R_i$ is the mean firing rate for bin $i$, and $R$ is the overall mean firing rate.\n",
    "\n",
    "The function `fklab.statistics.information.spatial_information` will compute the information content given a rate map and the corresponding occupancy. In addition to expressing information content in bits/spike, you can also express it as an information rate in bits/second. Despite that the function is called `spatial_information`, it can also be used to compute information content for other stimulus variables (e.g. direction, etc.)\n",
    "\n",
    "*Skaggs WE, McNaughton BL, Gothard KM, Markus EJ (1993) An information-theoretic approach to deciphering the hippocampal code. In: Advances in neural information processing 5 (Hanson SJ, Cowan JD, Giles CL, eds), pp 1030-1037. San Mateo, CA: Morgan Kaufmann Pub.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from fklab.statistics.information import spatial_information"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total_size = 64\n",
    "field_size = [8, 16, 32, 64]\n",
    "\n",
    "fig, axes = plt.subplots(len(field_size), 1, sharex=True, sharey=True, figsize=(6, 8))\n",
    "\n",
    "occupancy = np.ones(total_size)\n",
    "\n",
    "for sz, ax in zip(field_size, axes):\n",
    "    rate = np.concatenate([np.ones(sz), np.zeros(total_size - sz)])\n",
    "    si = spatial_information(rate, occupancy, \"bits/spike\")\n",
    "\n",
    "    ax.fill_between(np.arange(total_size), rate, color=\"k\", alpha=0.3)\n",
    "    ax.set(ylabel=\"firing rate [Hz]\")\n",
    "    ax.text(total_size, 1.4, \"{:.1f} bits/spike\".format(*si), ha=\"right\", va=\"top\")\n",
    "\n",
    "axes[-1].set(xlabel=\"stimulus\", ylim=(None, 1.5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Information content of example cells"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from fklab.statistics.information import spatial_information\n",
    "\n",
    "si_space = spatial_information(rmap, occ, \"bits/spike\")\n",
    "si_hd = spatial_information(hd_rmap, hd_occ, \"bits/spike\")\n",
    "\n",
    "print(\n",
    "    \"spatial information content: place cell {:.1f} \"\n",
    "    \"bits/spike, heading cell {:.1f} bits/spike\".format(*si_space)\n",
    ")\n",
    "print(\n",
    "    \"heading information content: place cell {:.1f} \"\n",
    "    \"bits/spike, heading cell {:.1f} bits/spike\".format(*si_hd)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "### Directional tuning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Uniformizing directional data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To quantify directional tuning, we can turn to circular statistics (in module `fklab.statistics.circular`). There are adaptations of these statistics that can be applied to directional tuning curves, but we can also work directly on the underlying spiking data. However, we do have to take into account the sample distribution of the stimulus and not just analyze the distribution of the stimulus at spike times. To do so, we can uniformize the spike stimulus distribution according to the stimulus distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.statistics.circular"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hd_spike = [\n",
    "    rm.compute_variable_at_times(k, fklab.statistics.circular.wrap(hd), var_t=t)\n",
    "    for k in [spike_t_place, spike_t_hd]\n",
    "]\n",
    "\n",
    "uniform_hd_spike = [fklab.statistics.circular.uniformize(hd, k) for k in hd_spike]\n",
    "\n",
    "fig, axes = plt.subplots(1, 3, sharex=True, figsize=(12, 3))\n",
    "\n",
    "bins = np.linspace(0, 2 * np.pi, 16)\n",
    "for ax, pre, post in zip(axes[:2], hd_spike, uniform_hd_spike):\n",
    "    ax.hist(pre, bins=bins, alpha=0.5, label=\"raw\")\n",
    "    ax.hist(post, bins=bins, alpha=0.5, label=\"uniformized\")\n",
    "    ax.legend()\n",
    "\n",
    "axes[-1].hist(fklab.statistics.circular.wrap(hd), bins=bins, alpha=0.5, color=\"k\")\n",
    "\n",
    "axes[0].set(\n",
    "    xticks=np.linspace(0, 2 * np.pi, 5),\n",
    "    xticklabels=[\"0\", r\"$\\frac{\\pi}{2}$\", r\"$\\pi$\", r\"$\\frac{3\\pi}{2}$\", r\"$2\\pi$\"],\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Circular statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test if the uniformized heading distributions for the two example cells are significantly different from the uniform distribution. For this, we use the Kuiper test."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pval = [fklab.statistics.circular.kuiper(k)[0] for k in uniform_hd_spike]\n",
    "print(\n",
    "    \"Kuiper test for uniformity: place cell \"\n",
    "    \"p={:.3f}, heading cell p={:.3f}\".format(*pval)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the Rayleigh test of uniformity could also be used, but then against the alternative hypothesis of a unimodal distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pval = [fklab.statistics.circular.rayleigh(k)[0] for k in uniform_hd_spike]\n",
    "print(\n",
    "    \"Rayleigh test for uniformity: place cell \"\n",
    "    \"p={:.3f}, heading cell p={:.3f}\".format(*pval)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These tests clearly indicate a strongly significant directional tuning of the heading cell. But (and this may depend on the simulated data) also significant or close to significant directional tuning of the place cell. Event thoughwe known that the place cell model does not contain a directional component.\n",
    "\n",
    "It is possible that there is an interaction between position and heading in the simulated data, such the overall heading inside the place field may not be as uniform as the overall heading shown in the right histogram above. Let's select only the heading directions in the placefield and quickly check the distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = in_circle(10, [10, 10])(x, y)\n",
    "hd_in_field = fklab.statistics.circular.wrap(hd[b])\n",
    "in_field_uniform_hd_spike = fklab.statistics.circular.uniformize(\n",
    "    hd_in_field, hd_spike[0]\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots(1, 2, figsize=(8, 3))\n",
    "\n",
    "ax[0].hist(hd_spike[0], bins=bins, alpha=0.5, label=\"raw\")\n",
    "ax[0].hist(in_field_uniform_hd_spike, bins=bins, alpha=0.5, label=\"uniformized\")\n",
    "ax[0].legend()\n",
    "\n",
    "ax[1].hist(hd_in_field, bins=bins, alpha=0.5, color=\"k\")\n",
    "\n",
    "ax[0].set(\n",
    "    xticks=np.linspace(0, 2 * np.pi, 5),\n",
    "    xticklabels=[\"0\", r\"$\\frac{\\pi}{2}$\", r\"$\\pi$\", r\"$\\frac{3\\pi}{2}$\", r\"$2\\pi$\"],\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indeed, there is a different distribution of headings inside the place field (your results may vary depending on the test data simulation). What happens to the statistics if we uniformize according to the in-field heading distribution?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pval = fklab.statistics.circular.kuiper(in_field_uniform_hd_spike)[0]\n",
    "print(\"Kuiper test for uniformity: place cell \" \"p={:.3f}\".format(pval))\n",
    "\n",
    "pval = fklab.statistics.circular.rayleigh(in_field_uniform_hd_spike)[0]\n",
    "print(\"Rayleigh test for uniformity: place cell \" \"p={:.3f}\".format(pval))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Fit Von Mises distribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assuming a unimodal directional tuning, we could further quantify the preferred direction and its spread. For example by fitting a Von Mises distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vonmises_fit = [\n",
    "    fklab.statistics.circular.vonmises.fit(k, fscale=1)[:2] for k in uniform_hd_spike\n",
    "]\n",
    "\n",
    "print(\n",
    "    \"Von Mises fit: place cell \\u03BC={:.1f}\\u03C0 \\u03BA={:.1f}, \"\n",
    "    \"heading cell \\u03BC={:.1f}\\u03C0 \\u03BA={:.1f}\".format(\n",
    "        vonmises_fit[0][1] / np.pi,\n",
    "        vonmises_fit[0][0],\n",
    "        vonmises_fit[1][1] / np.pi,\n",
    "        vonmises_fit[1][0],\n",
    "    )\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  },
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Egocentric boundary coding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cells in some brain regions appear to be active only when the subject is near a wall or other type of boundary in the environment. The cells are called \"border\" cells or \"boundary vector cells\". More recently, cells have described that fire when boundaries appear at a certain distance and angle relative to the subject position and heading. This is called egocentric boundary coding. In this notebook, we will simulate an egocentric boundary neuron and show how we can analyze such data. We will further compare the egocentric boundary neuron to other spatially tuned cells: a place cell, head direction cell and a linear speed cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.colors\n",
    "import matplotlib.cm\n",
    "\n",
    "import fklab.ratemap as rm\n",
    "import fklab.ratemap.utilities\n",
    "import fklab.behavior.simulation\n",
    "import fklab.spikes.simulation\n",
    "import fklab.plot\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# variables\n",
    "boundary_type = \"circle\"  # 'circle' or 'rectangle'\n",
    "boundary_half_size = 50"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Construct behavioral trajectory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# construct boundary function and set of points on boundary\n",
    "# that will be used to compute egocentric distance and angle\n",
    "if boundary_type == \"rectangle\":\n",
    "    width = height = 2 * boundary_half_size\n",
    "    boundary = fklab.behavior.simulation.rectangle_boundary(width=width, height=height)\n",
    "    boundary_points = fklab.behavior.simulation.points_on_rectangle_boundary(\n",
    "        n=25, width=width, height=height\n",
    "    )\n",
    "else:\n",
    "    radius = boundary_half_size\n",
    "    boundary = fklab.behavior.simulation.circle_boundary(radius=radius)\n",
    "    boundary_points = fklab.behavior.simulation.points_on_circle_boundary(\n",
    "        n=100, radius=radius\n",
    "    )\n",
    "\n",
    "dt = 1 / 20  # behavior is sampled at 20 Hz\n",
    "run_speed = 10  # mean run speed\n",
    "turn_distance = 15  # boundary distance at which custom turn function kicks in\n",
    "\n",
    "# generate the random walk model\n",
    "model = fklab.behavior.simulation.random_walk(\n",
    "    dt=dt,\n",
    "    speed=run_speed,\n",
    "    boundary=boundary,\n",
    "    turn=fklab.behavior.simulation.custom_turn_function(\n",
    "        turn_distance, dt=dt, thigmotaxis=0.1\n",
    "    ),\n",
    "    maxiter=1000,\n",
    ")\n",
    "\n",
    "# construct trajectory from model\n",
    "t, x, y, _, _ = fklab.behavior.simulation.generate_trajectory(model, nsteps=20000)\n",
    "\n",
    "# plot trajectory\n",
    "plt.plot(x, y, \"k\")\n",
    "plt.axis(\"equal\")\n",
    "plt.xlabel(\"x [cm]\")\n",
    "plt.ylabel(\"y [cm]\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Generate spike trains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute behavioral variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xy = np.column_stack([x, y])\n",
    "\n",
    "velocity = np.gradient(x, dt) + np.gradient(y, dt) * 1.0j\n",
    "speed = np.abs(velocity)\n",
    "\n",
    "# compute heading direction\n",
    "phi = np.angle(velocity)\n",
    "\n",
    "# compute egocentric coordinates relative to points on boundary\n",
    "bnd_dist, bnd_angle = fklab.ratemap.utilities.egocentric_coordinates(\n",
    "    xy, phi, boundary_points, maxdistance=boundary_half_size\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Egocentric boundary cell model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# egocentric boundary cell\n",
    "\n",
    "preferred_distance = 20\n",
    "preferred_heading = 0\n",
    "distance_scale = 2\n",
    "heading_kappa = 8\n",
    "\n",
    "ego_model = fklab.spikes.simulation.egocentric_boundary_tuning(\n",
    "    distance=preferred_distance,\n",
    "    distance_scale=distance_scale,\n",
    "    mu=preferred_heading,\n",
    "    kappa=heading_kappa,\n",
    ")\n",
    "\n",
    "# compute the mean firing rate at each time point as the\n",
    "# maximum across all boundary points from the model\n",
    "ego_rate = np.nanmax(ego_model(bnd_dist, bnd_angle), axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Place cell model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "place_model = fklab.spikes.simulation.gaussian_2d_place_field(\n",
    "    center=[10, 25], bandwidth=10, rate=10\n",
    ")\n",
    "\n",
    "place_rate = place_model(xy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Heading cell model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "heading_model = fklab.spikes.simulation.directional_tuning(mu=0.25 * np.pi)\n",
    "\n",
    "heading_rate = heading_model(phi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Linear speed cell model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speed_model = fklab.spikes.simulation.linear_speed_tuning(slope=0.5)\n",
    "\n",
    "speed_rate = speed_model(speed)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulate spike trains"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "celltypes = [\"EB cell\", \"place cell\", \"heading cell\", \"speed cell\"]\n",
    "\n",
    "# use the time-varying rate to generate inhomogeneous Poisson spike train\n",
    "spike_t = [\n",
    "    fklab.spikes.simulation.inhomogeneous_poisson_rescale(rate, t)\n",
    "    for rate in [ego_rate, place_rate, heading_rate, speed_rate]\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize cell activity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute location and heading at time of spikes\n",
    "simxy = [fklab.ratemap.compute_variable_at_times(k, xy, var_t=t) for k in spike_t]\n",
    "\n",
    "simphi = [fklab.ratemap.compute_variable_at_times(k, phi, var_t=t) for k in spike_t]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot spike location on top of behavior\n",
    "# color spikes by heading\n",
    "\n",
    "fig, axes = plt.subplots(\n",
    "    1,\n",
    "    len(celltypes),\n",
    "    sharex=False,\n",
    "    sharey=True,\n",
    "    figsize=(18, 3.5),\n",
    "    gridspec_kw={\"right\": 0.8},\n",
    ")\n",
    "\n",
    "colscale = matplotlib.colors.Normalize(vmin=-np.pi, vmax=np.pi)\n",
    "\n",
    "for celltype, _xy, _phi, ax in zip(celltypes, simxy, simphi, axes.ravel()):\n",
    "\n",
    "    ax.plot(x, y, color=\"lightgray\", zorder=0)\n",
    "\n",
    "    h = ax.scatter(_xy[:, 0], _xy[:, 1], c=_phi, s=3, cmap=\"hsv\", norm=colscale)\n",
    "\n",
    "    ax.axis(\"equal\")  # does not work when both sharex and sharey are True :(\n",
    "    ax.set(xlabel=\"x [cm]\", title=celltype)\n",
    "\n",
    "axes[0].set(ylabel=\"y [cm]\")\n",
    "\n",
    "cax = fig.add_axes([0.825, 0, 0.075, 1], polar=True)\n",
    "fklab.plot.polar_colorbar(\n",
    "    cax, rotation=-np.pi, annulus=0.75, show_theta_axis=True, style=\"math\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Spatial coding properties (histogram)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Egocentric boundary coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = [np.linspace(0, boundary_half_size, 25), np.linspace(-np.pi, np.pi, 25)]\n",
    "\n",
    "ratemap, spikecount, occ, grid = rm.hist.general_tuning(\n",
    "    *spike_t, stimdata=np.stack([bnd_dist, bnd_angle], axis=-1), t=t, dt=dt, bins=bins\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_2d_maps(\n",
    "    ratemap,\n",
    "    coordinates=bins,\n",
    "    xlabel=\"distance [cm]\",\n",
    "    ylabel=\"angle [rad]\",\n",
    "    colorlabel=\"firing rate [Hz]\",\n",
    "    cmap=\"inferno\",\n",
    "    cbar=True,\n",
    "    cbar_kw={\"shrink\": 0.9},\n",
    "    cmin=0,\n",
    "    cmax=\"auto\",\n",
    "    roundto=1,\n",
    "    grid=(None, 4),\n",
    "    figsize=(18, 3.5),\n",
    ")\n",
    "\n",
    "axes[0, 0].set_yticks([-np.pi, 0, np.pi])\n",
    "axes[0, 0].set_yticklabels([r\"$-\\pi$\", \"0\", r\"$+\\pi$\"])\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)\n",
    "    ax.set_frame_on(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Place coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = [\n",
    "    np.linspace(-boundary_half_size, boundary_half_size, 25),\n",
    "] * 2\n",
    "\n",
    "ratemap, spikecount, occ, grid = rm.hist.general_tuning(\n",
    "    *spike_t,\n",
    "    stimdata=xy,\n",
    "    t=t,\n",
    "    dt=dt,\n",
    "    bins=bins,\n",
    "    smooth=[2.5, 2.5],\n",
    "    valid=fklab.ratemap.utilities.in_circle(radius)\n",
    "    if boundary_type == \"circle\"\n",
    "    else None\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_2d_maps(\n",
    "    ratemap,\n",
    "    coordinates=grid,\n",
    "    xlabel=\"x [cm]\",\n",
    "    ylabel=\"y [cm]\",\n",
    "    colorlabel=\"firing rate [Hz]\",\n",
    "    cmap=\"inferno\",\n",
    "    cbar=True,\n",
    "    cbar_kw={\"shrink\": 0.9},\n",
    "    cmin=0,\n",
    "    cmax=\"auto\",\n",
    "    roundto=1,\n",
    "    grid=(None, 4),\n",
    "    figsize=(18, 3.5),\n",
    ")\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)\n",
    "    ax.set_frame_on(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Heading coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = [np.linspace(-np.pi, np.pi, 25)]\n",
    "\n",
    "ratemap, spikecount, occ, grid = rm.hist.general_tuning(\n",
    "    *spike_t, stimdata=phi, t=t, dt=dt, bins=bins\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_1d_maps(\n",
    "    ratemap,\n",
    "    x=grid[0],\n",
    "    grid=(None, 4),\n",
    "    figsize=(15, 3),\n",
    "    roundto=1,\n",
    "    xlabel=\"heading [rad]\",\n",
    "    ylabel=\"firing rate [Hz]\",\n",
    ")\n",
    "\n",
    "axes[0, 0].set_xticks([-np.pi, 0, np.pi])\n",
    "axes[0, 0].set_xticklabels([r\"$-\\pi$\", \"0\", r\"$+\\pi$\"])\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Speed coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = [np.linspace(0, 25, 25)]\n",
    "\n",
    "ratemap, spikecount, occ, grid = rm.hist.general_tuning(\n",
    "    *spike_t, stimdata=speed, t=t, dt=dt, bins=bins, smooth=[2]\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_1d_maps(\n",
    "    ratemap,\n",
    "    x=grid[0],\n",
    "    grid=(None, 4),\n",
    "    figsize=(15, 3),\n",
    "    roundto=1,\n",
    "    xlabel=\"speed [cm/s]\",\n",
    "    ylabel=\"firing rate [Hz]\",\n",
    ")\n",
    "\n",
    "axes[0, 0].set_xticks(np.linspace(0, 25, 6))\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spatial coding properties (kde)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Egocentric boundary coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ratemap, spikecount, occ, grid = rm.kde.vector_tuning(\n",
    "    *spike_t,\n",
    "    t=t,\n",
    "    dt=dt,\n",
    "    vectors=np.stack([bnd_dist, bnd_angle], axis=-1),\n",
    "    kappa=8,\n",
    "    bandwidth=2,\n",
    "    grid=np.linspace(0, boundary_half_size, 51),\n",
    "    offset=-np.pi,\n",
    "    compression=1.0\n",
    ")\n",
    "\n",
    "polar_props = {\n",
    "    \"style\": \"egocentric\",\n",
    "    \"style_kw\": {\"labels\": {90: \"L\", 270: \"R\"}},\n",
    "    \"radial_min\": 0,\n",
    "    \"radial_max\": boundary_half_size,\n",
    "    \"radial_ticks\": [0, 50],\n",
    "    \"annulus\": 0.2,\n",
    "    \"radial_label\": \"distance [cm]\",\n",
    "}\n",
    "\n",
    "fig, axes = fklab.plot.setup_polar_axes_grid(\n",
    "    len(celltypes),\n",
    "    grid=(1, None),\n",
    "    figsize=(20, 3),\n",
    "    show_reference=True,\n",
    "    reference_kw={\"facecolor\": [0.9, 0.9, 0.9]},\n",
    "    axes_props=polar_props,\n",
    ")\n",
    "\n",
    "roundto = 1\n",
    "\n",
    "for celltype, data, ax in zip(celltypes, ratemap, axes.ravel()):\n",
    "    vmax = np.ceil(np.nanmax(data) / roundto) * roundto\n",
    "\n",
    "    fklab.plot.plot_polar_map(data, theta=grid[1], rho=grid[0], ax=ax, vmax=vmax)\n",
    "\n",
    "    fklab.plot.axes_annotation(\n",
    "        ax, \"{:.0f} Hz\".format(vmax), hloc=\"center\", vloc=\"center\"\n",
    "    )\n",
    "\n",
    "    fklab.plot.axes_annotation(ax, celltype, hloc=\"center\", vloc=\"top out\")\n",
    "\n",
    "\n",
    "fklab.plot.fixed_colorbar(\n",
    "    vmax=\"max\",\n",
    "    ax=axes,\n",
    "    label=\"firing rate [Hz]\",\n",
    "    shrink=0.9,\n",
    "    orientation=\"vertical\",\n",
    "    ndivisions=5,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Place coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grid = [\n",
    "    np.linspace(-boundary_half_size, boundary_half_size, 51),\n",
    "] * 2\n",
    "\n",
    "ratemap, spikecount, occ, grid = rm.kde.linear_2d_tuning(\n",
    "    *spike_t,\n",
    "    xy=xy,\n",
    "    t=t,\n",
    "    dt=dt,\n",
    "    grid=grid,\n",
    "    bandwidth=5,\n",
    "    compression=1.0,\n",
    "    valid=fklab.ratemap.utilities.in_circle(radius)\n",
    "    if boundary_type == \"circle\"\n",
    "    else None\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_2d_maps(\n",
    "    ratemap,\n",
    "    coordinates=grid,\n",
    "    xlabel=\"x [cm]\",\n",
    "    ylabel=\"y [cm]\",\n",
    "    colorlabel=\"firing rate [Hz]\",\n",
    "    cmap=\"inferno\",\n",
    "    cbar=True,\n",
    "    cbar_kw={\"shrink\": 0.9},\n",
    "    cmin=0,\n",
    "    cmax=\"auto\",\n",
    "    roundto=1,\n",
    "    grid=(None, 4),\n",
    "    figsize=(18, 3.5),\n",
    ")\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)\n",
    "    ax.set_frame_on(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Heading coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ratemap, spikecount, occ, grid = rm.kde.directional_tuning(\n",
    "    *spike_t,\n",
    "    direction=phi,\n",
    "    t=t,\n",
    "    dt=dt,\n",
    "    npoints=51,\n",
    "    kappa=8,\n",
    "    offset=-np.pi,\n",
    "    compression=1.0\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_1d_maps(\n",
    "    ratemap,\n",
    "    x=grid,\n",
    "    grid=(None, 4),\n",
    "    figsize=(15, 3),\n",
    "    roundto=1,\n",
    "    xlabel=\"heading [rad]\",\n",
    "    ylabel=\"firing rate [Hz]\",\n",
    ")\n",
    "\n",
    "axes[0, 0].set_xticks([-np.pi, 0, np.pi])\n",
    "axes[0, 0].set_xticklabels([r\"$-\\pi$\", \"0\", r\"$+\\pi$\"])\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Speed coding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grid = [np.linspace(0, 25, 51)]\n",
    "\n",
    "ratemap, spikecount, occ, grid = rm.kde.linear_1d_tuning(\n",
    "    *spike_t, x=speed, t=t, dt=dt, grid=grid, bandwidth=2, compression=1.0\n",
    ")\n",
    "\n",
    "fig, axes = fklab.plot.plot_1d_maps(\n",
    "    ratemap,\n",
    "    x=grid,\n",
    "    grid=(None, 4),\n",
    "    figsize=(15, 3),\n",
    "    roundto=1,\n",
    "    xlabel=\"speed [cm/s]\",\n",
    "    ylabel=\"firing rate [Hz]\",\n",
    ")\n",
    "\n",
    "axes[0, 0].set_xticks(np.linspace(0, 25, 6))\n",
    "\n",
    "for label, ax in zip(celltypes, axes.ravel()):\n",
    "    ax.set(title=label)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

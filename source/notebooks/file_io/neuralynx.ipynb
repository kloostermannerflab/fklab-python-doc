{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Neuralynx data files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Neuralynx data files consist of two parts: a text header and binary data. The text header has a fixed size of 16384 bytes and contains metadata in the form of key/value pairs. The binary part contains fixed size data records with multiple fields, depending on the type of file. There are four basic file types: **Spike**, **Event**, **Video** and **CSC**. The module **fklab.io.neuralynx** provides support for importing data from neuralynx files, as explained below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Module import"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.io.neuralynx as nlx\n",
    "import os\n",
    "\n",
    "import warnings\n",
    "\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pathlib\n",
    "\n",
    "root_path = pathlib.Path(\"~/notebook_data_cache\").expanduser()\n",
    "path = root_path.joinpath(\"neuralynx\")\n",
    "\n",
    "data_path = path.joinpath(\"CSC_TT1_1.ncs\")\n",
    "\n",
    "if not path.exists():\n",
    "    path.mkdir(parents=True)\n",
    "\n",
    "if not data_path.exists():\n",
    "    from fklab.io.cloud import dropboxAPI\n",
    "\n",
    "    dbx = dropboxAPI.from_token(\n",
    "        os.environ.get(\"BITBUCKET_TOKEN\")\n",
    "    )  # Replace by your own token\n",
    "    dbx.download(\"/Data/fileIO/CSC_TT1_1.ncs\", data_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### File objects"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To open a neuralynx file, call the **NlxOpen** function with the name of the file. Internally, this function will determine the type of data file and return an object that represents that file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = nlx.NlxOpen(data_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Through the file object you have access to basic file information:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f.fullpath  # returns full path of the file\n",
    "f.filesize  # returns file size in bytes\n",
    "f.recordsize  # returns the size of data records in bytes\n",
    "f.nrecords  # returns the number of data records in the file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The contents of the file header are defined in a dictionary that is accessible through the *header* property of the file object. Under the hood, the value of most header keys is automatically converted from text representation to an appropriate python data type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f.header.keys()  # list all header keys\n",
    "f.header[\"FileType\"]  # returns value of FileType key in header"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data access"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data can be read from neuralynx data files using the *data* property. Each field in a data record can be accessed separately and a subset of records can be imported using standard slicing syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = f.data.timestamp[0:10]\n",
    "print(ts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For some fields a postprocessing step is performed. For example, spike waveforms are converted to microVolts. This conversion can be configured - for more information see *Spike files* below.\n",
    "\n",
    "Additional *virtual fields* are defined, in particular the *time* field provides access to the record timestamp after conversion to seconds.\n",
    "\n",
    "Finally, direct access to multiple fields is provided by *group fields*. The data is returned in a dictionary. For example, the *position* field for a Video file returns a dictionary with data for the time, x and y fields. For all data files a *default* group field that contains the most often used combination of fields is defined. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To import data one needs to provide the record indices. However, often one would like to select records based on a time range. This is supported by the **timeslice** method, which converts a time range into a record index range:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = f.data.signal[f.timeslice(start=1000, stop=1100)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An alternative way to import the default (grouped) fields is to use the **readdata** method, which takes start and stop arguments that define a time range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = f.readdata(start=0, stop=100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Spike files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List of all fields and grouped fields:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "record fields\n",
    "\n",
    "- **timestamp**: data record timestamp in microseconds\n",
    "- **scnumber** \n",
    "- **cellnumber**: cell number of spike determined by auto-sorting\n",
    "- **params**\n",
    "- **waveform**: spike waveform (nchan=1,2 or 4)\n",
    "\n",
    "virtual fields\n",
    "\n",
    "- **time**: data record time in seconds\n",
    "\n",
    "grouped fields\n",
    "\n",
    "- **spikes**: [time, waveform]\n",
    "- **default**: [time, waveform]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *waveform* field is automatically converted to microVolts. The behavior of this conversion can be configured by three file object properties:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### CSC files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List of all fields and grouped fields:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "record fields\n",
    "\n",
    "- **timestamp**: data record timestamp in microseconds\n",
    "- **channelnumber** \n",
    "- **samplefreq**\n",
    "- **numvalidsamples**\n",
    "- **signal**: buffer of 512 samples\n",
    "\n",
    "virtual fields\n",
    "\n",
    "- **time**: sample time in seconds\n",
    "\n",
    "grouped fields\n",
    "\n",
    "- **default**: [time, signal]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *signal* field is automatically converted to microVolts. The behavior of this conversion can be configured by three file object properties:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, the *time* virtual field returns the time for each sample in the signal by extrapolating the record timestamp (using the sampling frequency). (Note that the *timestamp* field currently does not support extrapolation). This behavior can be configured:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When reading data for a certain time range, it is possible that the requested times fall in the middle of a signal buffer in one record. The **readdata** method on CSC File objects will therefore read additional records and return only those samples that really lie in the requested time interval. Of course, this will only work if the expand_time property is set to True."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Video files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List of all fields and grouped fields:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "record fields\n",
    "\n",
    "- **swstx**\n",
    "- **swid**\n",
    "- **swdata_size**\n",
    "- **timestamp**: data record timestamp in microseconds\n",
    "- **points**: position and colour information on pixels that changed state \n",
    "- **sncrc**\n",
    "- **x**: computed x pixel position of LEDs\n",
    "- **y**: computed y pixel position of LEDs\n",
    "- **angle**: computed direction of dual LEDs\n",
    "- **targets**: position and colour information on groups of pixels that are above threshold\n",
    "\n",
    "virtual fields\n",
    "\n",
    "- **time**: data record time in seconds\n",
    "\n",
    "grouped fields\n",
    "\n",
    "- **position**: [time, x, y]\n",
    "- **default**: [time, x, y]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *points* and *targets* fields are automatically unpacked into their components, i.e. position (*x*, *y*), colour (*red*, *green*, *blue*) and *intensity*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Event files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List of all fields and grouped fields:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "record fields\n",
    "\n",
    "- **nstx**\n",
    "- **npkt_id**\n",
    "- **timestamp**: data record timestamp in microseconds\n",
    "- **nevent_id**\n",
    "- **nttl**\n",
    "- **ncrc**\n",
    "- **ndummy1**\n",
    "- **ndummy2**\n",
    "- **extra**\n",
    "- **eventstring**: event description\n",
    "\n",
    "virtual fields\n",
    "\n",
    "- **time**: sample time in seconds\n",
    "\n",
    "grouped fields\n",
    "\n",
    "- **default**: [time, eventstring]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ripple detection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## simulating ripples for testing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we first simulate a signal with ripples of a fixed frequency and with variable amplitude and duration. Noise is added to the signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.stats\n",
    "import scipy.signal\n",
    "\n",
    "fs = 1000.0  # Hz\n",
    "T = 25.0  # seconds\n",
    "time = np.arange(fs * T) / fs  # seconds\n",
    "\n",
    "# add variation in ripple frequency by construction a chirp signal\n",
    "signal = scipy.signal.chirp(time, 140, time[-1], 200)\n",
    "\n",
    "ripple_occurrence = 2  # Hz\n",
    "nripples = int(T * ripple_occurrence)\n",
    "\n",
    "np.random.seed(77)\n",
    "\n",
    "ripple_times = np.cumsum(\n",
    "    np.random.exponential(1.0 / ripple_occurrence, (nripples,))\n",
    ")  # seconds\n",
    "ripple_amplitudes = np.random.uniform(50.0, 250.0, (nripples,))  # micro volts\n",
    "ripple_durations = np.random.uniform(40.0, 150.0, (nripples,))  # milliseconds\n",
    "\n",
    "modulation = 0.0\n",
    "\n",
    "# construct ripple envelope\n",
    "for k in range(nripples):\n",
    "    std = 0.00025 * ripple_durations[k]  # seconds\n",
    "    norm_factor = 1.0 / (std * np.sqrt(2 * np.pi))\n",
    "    modulation = (\n",
    "        modulation\n",
    "        + ripple_amplitudes[k]\n",
    "        * scipy.stats.distributions.norm(ripple_times[k], std).pdf(time)\n",
    "        / norm_factor\n",
    "    )\n",
    "\n",
    "# construct ripple signal with added noise\n",
    "noise_std = 50.0  # micro volts\n",
    "signal = modulation * signal + np.random.normal(0.0, noise_std, len(signal))\n",
    "\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn\n",
    "\n",
    "plt.plot(time, signal)\n",
    "plt.xlabel(\"time [s]\")\n",
    "plt.ylabel(\"signal [uV]\")\n",
    "plt.title(\"Simulated ripple signal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## filtering signals in the ripple band"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step in detecting ripples is to filter the signal in the ripple band (~ 140-225 Hz). The `fklab.signals.filter` module has convenience functions to construct and inspect filters, as well as to apply a filter to your signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.signals.filter\n",
    "\n",
    "b = fklab.signals.filter.construct_filter(\n",
    "    \"ripple\", fs=fs, transition_width=\"10%\", attenuation=60\n",
    ")\n",
    "\n",
    "fklab.signals.filter.inspect_filter(\n",
    "    b, fs=fs, filtfilt=True, grid=True, detail=[100, 270]\n",
    ")\n",
    "plt.title(\"Frequency response of filter\")\n",
    "\n",
    "filtered_signal = fklab.signals.filter.apply_filter(\n",
    "    signal, \"ripple\", fs=fs, transition_width=\"10%\", attenuation=60\n",
    ")\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(time, filtered_signal)\n",
    "plt.xlabel(\"time [s]\")\n",
    "plt.ylabel(\"signal [uV]\")\n",
    "plt.title(\"Filtered signal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## computing ripple envelope"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to compute an envelope of the band-pass filtered signal. There is a generic function in `fklab.signals.filter` that can be used to compute the envelope in any frequency band. `fklab.signals.ripple` also contains a convenience function `ripple_envelope` specifically for ripples that already has sensible defaults for filtering and smoothing options set.\n",
    "\n",
    "Alternatively, one could compute the (smoothed) power or root-mean-square in the ripple band (see `fklab.signals.filter.compute_smoothed_rms`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute the envelope\n",
    "# since we already filtered the signal, we set isfiltered=True\n",
    "# we also enable smoothing with a gaussian kernel with 7.5 ms bandwidth\n",
    "envelope = fklab.signals.filter.compute_envelope(\n",
    "    filtered_signal, fs=fs, isfiltered=True, smooth_options=dict(bandwidth=0.0075)\n",
    ")\n",
    "\n",
    "# if you want to compute the envelope of an unfiltered signal, you can do:\n",
    "# envelope = fklab.signals.filter.compute_envelope( signal, freq_band='ripple', fs=fs, isfiltered=False, smooth_options=dict(bandwidth=0.0075) )\n",
    "\n",
    "plt.plot(time, envelope)\n",
    "plt.xlabel(\"time [s]\")\n",
    "plt.ylabel(\"envelope [uV]\")\n",
    "plt.title(\"Ripple envelope\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## setting an appropriate ripple detection threshold"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An import step is setting the threshold for ripple detection. Ideally, one would use a threshold that best separates the background noise from the ripples. Unfortunately, determining that best threshold is not straightforward.\n",
    "\n",
    "Most often we use the envelope statistics are used to compute the threshold values. For example, one could use the mean and standard deviation, the median and inter-quartile range or specific percentiles. These three methods are illustrated below. Again, in the ideal case, one could compute the signal statistics of only the background noise and then set a threshold such that you will only detect a low number of false positives. However, this would require that you know upfront what is background and what is ripple, which is generally not the case.\n",
    "\n",
    "In practice, you use the statistics of the whole signal. You do need to realize that these statistics are affected by the very ripples you would like to detect. A robust measure of of the statistics (such as median and inter-quartile range) are thus preferred over non-robust measures (such as mean and standard deviation)\n",
    "\n",
    "For detecting start and stop times of ripple segments, generally two thresholds are used. The upper threshold determines if a peak in the envelope is considered a ripple or not and the lower thresholds determines the start and stop times.\n",
    "\n",
    "_Note that for the simulated test signal here, the relatively large number of ripples significantly influence the mean and standard deviation, resulting in rather high threshold._"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.signals.ripple\n",
    "\n",
    "fcn = fklab.signals.core.compute_threshold_zscore([0, 5])\n",
    "thresholds_zscore = fcn(envelope)\n",
    "\n",
    "fcn = fklab.signals.core.compute_threshold_median([0, 5])\n",
    "thresholds_median = fcn(envelope)\n",
    "\n",
    "fcn = fklab.signals.core.compute_threshold_percentile([50, 90])\n",
    "thresholds_percentile = fcn(envelope)\n",
    "\n",
    "seaborn.distplot(envelope)\n",
    "\n",
    "plt.axvline(thresholds_zscore[1], color=\"blue\")\n",
    "plt.axvline(thresholds_median[1], color=\"green\")\n",
    "plt.axvline(thresholds_percentile[1], color=\"orange\")\n",
    "\n",
    "plt.xlabel(\"envelope amplitude [uV]\")\n",
    "plt.ylabel(\"probability\")\n",
    "\n",
    "plt.title(\"Distribution of envelope amplitudes and thresholds\")\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(time, envelope)\n",
    "\n",
    "plt.axhline(thresholds_zscore[1], color=\"blue\")\n",
    "plt.axhline(thresholds_median[1], color=\"green\")\n",
    "plt.axhline(thresholds_percentile[1], color=\"orange\")\n",
    "\n",
    "plt.xlabel(\"time [s]\")\n",
    "plt.ylabel(\"envelope [uV]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## detecting ripple peaks and ripple segments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you detect ripple peaks and/or ripple segments. For this you use the `local_maxima` and `detect_mountains` functions in the `fklab.signals` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.signals.core\n",
    "\n",
    "fcn = fklab.signals.core.compute_threshold_median([0, 5])\n",
    "thresholds = fcn(envelope)\n",
    "\n",
    "ripple_peaks, ripple_amp = fklab.signals.core.localmaxima(\n",
    "    envelope, x=time, method=\"gradient\", yrange=[thresholds[1], np.inf]\n",
    ")\n",
    "\n",
    "ripple_segments = fklab.signals.core.detect_mountains(\n",
    "    envelope, x=time, low=thresholds[0], high=thresholds[1]\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "\n",
    "ax.plot(time, envelope)\n",
    "\n",
    "import fklab.segments\n",
    "\n",
    "fklab.segments.plot_segments_as_patches(\n",
    "    ripple_segments, ycoords=\"axes\", alpha=0.2, color=\"orange\", axes=ax\n",
    ")\n",
    "ax.vlines(\n",
    "    ripple_peaks,\n",
    "    color=[0.5, 0, 0],\n",
    "    linewidth=0.5,\n",
    "    ymin=0,\n",
    "    ymax=1,\n",
    "    transform=ax.get_xaxis_transform(),\n",
    ")\n",
    "ax.plot(ripple_peaks, ripple_amp, \"r.\")\n",
    "\n",
    "ax.set(title=\"Detected ripple events and segments\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## one function to do it all"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can perform ripple detection is one step using the `detect_ripples` function in `fklab.signals.ripple`. You can either provide a raw signal, a pre-filtered signal or a pre-computed envelope, and the function will return the ripple peaks and ripple segments. Just make sure that you set the `isfiltered` and `isenvelope` options accordingly.\n",
    "\n",
    "The additional `allowable_gap` and `minimum_duration` options determine if two neighbouring segments with a small gap should be merged, and if short-duration ripples should be removed.\n",
    "\n",
    "The threshold option should be either a list of manually set threshold values, or a function that will compute thresholds from a signal (as was done above)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "threshold_fcn = fklab.signals.core.compute_threshold_median([0, 5])\n",
    "\n",
    "(\n",
    "    (ripple_peaks, ripple_amp),\n",
    "    ripple_segments,\n",
    "    thresholds,\n",
    ") = fklab.signals.ripple.detect_ripples(\n",
    "    time, signal, threshold=threshold_fcn, allowable_gap=0.02, minimum_duration=0.04\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ripple analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more in-depth analysis of ripple properties, you can use the `analyze_ripples` function. This function will take the wide-band signal, perform ripple detection, extract ripple signal windows, compute power spectra and quantify ripple properties. The output is a dictionary with the following entries:\n",
    "\n",
    "- **n** number of detected ripples\n",
    "- **peaks** peaks in ripple envelope\n",
    "- **peak_times** times of peaks in ripple envelope\n",
    "- **ripple_windows** ripple start and end times\n",
    "- **threshold** detection threshold in data units\n",
    "- **npeaks** the number of peaks per ripple\n",
    "- **max_peak** largest peak per ripple\n",
    "- **max_peak_time** time of largest peak per ripple\n",
    "- **max_peak_rel_time** time of largest peak per ripple relative to center time\n",
    "- **ripple_signals** unfiltered signal snippet around ripples\n",
    "- **ripple_filtered** filtered signal snippet around ripples\n",
    "- **ripple_envelopes** envelope snippet around ripples\n",
    "- **ripple_t** time for snippets\n",
    "- **spectra** spectral density per ripple\n",
    "- **frequency** frequency vector for spectra\n",
    "- **peak_frequency** peak frequency in spectra\n",
    "- **peak_power** peak power in spectra\n",
    "- **options** a dictionary with options used for ripple analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ripple_data = fklab.signals.ripple.analyze_ripples(\n",
    "    time,\n",
    "    signal,\n",
    "    fs=fs,\n",
    "    detection_kw={\"allowable_gap\": 0.02, \"threshold\": [1, 5]},\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\n",
    "    \"There were {} ripples detected, with {}-{} peaks in the envelope per ripple \"\n",
    "    \"and {:.0f}-{:.0f} Hz peak frequency.\".format(\n",
    "        ripple_data[\"n\"],\n",
    "        np.min(ripple_data[\"npeaks\"]),\n",
    "        np.max(ripple_data[\"npeaks\"]),\n",
    "        np.min(ripple_data[\"peak_frequency\"]),\n",
    "        np.max(ripple_data[\"peak_frequency\"]),\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## visualize ripple features\n",
    "\n",
    "Scatter plot and distributions of the peak amplitude and duration of detected ripples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "jointgrid = fklab.signals.ripple.visualize_ripple_features(ripple_data, marker_size=16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## visualize ripple envelopes\n",
    "\n",
    "Plots of all ripple envelopes, the distribution of ripple window sizes and the distribution of ripple peak times relative to the center of the ripple window."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = fklab.signals.ripple.visualize_ripple_envelopes(ripple_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## visualize ripple spectra\n",
    "\n",
    "Plot spectra of all detected ripples and a distribution of peak frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = fklab.signals.ripple.visualize_ripple_spectra(ripple_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ripple modulation of spike trains\n",
    "\n",
    "Compute how the spike rate is modulated by ripples. The basic procedure is to count the number of spikes inside ripple windows and compare to the number of spikes in a reference window. The reference window is defined either before, after or both before and after the ripple windows with an optional time offset from the ripple window. The reference windows have the same duration as the ripple window.\n",
    "\n",
    "The following ripple modulation metrics are available. Here, $s$ is the array of spike counts in ripples, $\\Delta$ is the array of ripple durations, $ref$ is the array of reference spike counts, $n$ is the number of ripples.\n",
    "\n",
    "- **rate**: $\\frac{\\sum{s}}{\\sum{\\Delta}}$\n",
    "- **relative_rate**: $\\frac{\\sum{s}-\\sum{ref}}{\\sum{\\Delta}}$\n",
    "- **fraction**: $\\frac{\\sum{(s>0)}}{n}$\n",
    "- **relative_fraction**: $\\frac{\\sum{(s>0)}-\\sum{(ref>0)}}{n}$\n",
    "- **depth**: $\\frac{\\sum{s}-\\sum{ref}}{n}$\n",
    "- **relative_depth**: $\\frac{\\sum{s}-\\sum{ref}}{\\sum{ref}}$\n",
    "- **normalized**: $\\frac{\\sum{s}-\\sum{ref}}{\\sum{s}+\\sum{ref}}$\n",
    "\n",
    "The confidence interval for a modulation metric is computed using bootstrapping."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.spikes.simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# first, simulate a ripple modulated spike train\n",
    "background_rate = 2.0\n",
    "rate_modulation = 20.0\n",
    "\n",
    "spike_rate = background_rate + rate_modulation * modulation / np.max(modulation)\n",
    "\n",
    "spike_times = fklab.spikes.simulation.inhomogeneous_poisson_thinning(spike_rate, time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(\n",
    "    2, 1, sharex=True, gridspec_kw={\"height_ratios\": [1, 4]}, figsize=(10, 4)\n",
    ")\n",
    "\n",
    "# plot spike train\n",
    "ax[0].eventplot(spike_times, color=\"k\")\n",
    "ax[0].set(yticks=[])\n",
    "\n",
    "# plot filtered signal\n",
    "ax[1].plot(time, filtered_signal)\n",
    "ax[1].set(xlabel=\"time [s]\", ylabel=\"signal [uV]\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute the various modulation metrics\n",
    "m = {\n",
    "    metric: fklab.signals.ripple.compute_ripple_modulation(\n",
    "        ripple_data[\"ripple_windows\"], spike_times, metric=metric\n",
    "    )\n",
    "    for metric in [\n",
    "        \"rate\",\n",
    "        \"relative_rate\",\n",
    "        \"fraction\",\n",
    "        \"relative_fraction\",\n",
    "        \"depth\",\n",
    "        \"relative_depth\",\n",
    "        \"normalized\",\n",
    "    ]\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for metric, value in m.items():\n",
    "    print(\n",
    "        \"{}: {:.2f} [{:.2f}-{:.2f}]\".format(\n",
    "            metric, value[0][0], value[1][0, 0], value[1][0, 1]\n",
    "        )\n",
    "    )"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "156px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

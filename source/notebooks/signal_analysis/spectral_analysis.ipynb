{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Spectral analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multi-taper spectral analysis (background)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **power spectral density** (PSD) describes how the power of a signal is distributed across different frequencies (from 0 to the [Nyquist frequency](https://en.wikipedia.org/wiki/Nyquist_frequency)). The PSD is a density, which means that the **integral** between any two frequencies *f1* and *f2* corresponds to the average power in the signal over that frequency band.\n",
    "\n",
    "A sampled (discrete) time-domain signal (such as local field potentials) can be transformed into the frequency domain using the [discrete Fourier transform](https://en.wikipedia.org/wiki/Discrete_Fourier_transform). The periodogram is the simplest estimate of the PSD, which is computed as the modulus squared of the discrete Fourier transform. The major drawbacks of the periodogram are a high deviation from the true PSD - represented by both a narrow-band bias (i.e. frequency resolution) and a broad-band bias - and a high variance. \n",
    "\n",
    "**Welch's method** for estimating the PSD (also called modified periodogram) uses averaging across multiple (possibly overlapping) data segments and tapering (or windowing) of the data segments to improve the trade-off between resolution, broad-band bias and variance. Tapering reduces the broad-band bias at the expense of resolution. Splitting up the data in smaller segments and averaging the modified periodograms of the individual segments, reduces the variance of the estimate, but also reduces the resolution.\n",
    "\n",
    "The **multi-taper method**, however, provides a more favorable tradeoff between narrow-band bias (resolution), broad-band bias, and variance than Welch’s method. In the multi-taper method, a set of orthogonal tapers are applied to the data and the PSD is computed as the average of the set of tapered estimates. The functions in the fklab.signals.multitaper python module can be used to compute multi-taper PSD spectral estimates. The functions are based on the [Chronux](www.chronux.org/) matlab toolbox.\n",
    "\n",
    "For more background information on multi-taper spectral analysis see [Wikipedia](https://en.wikipedia.org/wiki/Multitaper) and this [paper](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6767046) by Babadi and Brown (2014)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multi-taper spectral analysis (practice)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. **Pick time window.** Most signals we work with are nonstationary (at least over long time scales) and spectral estimation techniques cannot be readily applied. The practical solution is to choose a time window over which the signal can reasonably be assumed stationary. \n",
    "2. **Pick spectral resolution.** Select a desired spectral resolution (in Hz) that is appropriate for the frequency bands of interest. The spectral resolution can not be smaller than 1.0 divided by the time window and should be below the Nyquist frequency.\n",
    "3. **Apply multi-taper spectral analysis.** Use the functions in fklab.signals.multitaper to compute the spectrum or spectrogram."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Module import and test signal generation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.signals.multitaper as mt\n",
    "import scipy.signal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create test signal\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "duration = 10.0  # seconds\n",
    "fs = 100.0  # Hz\n",
    "nsamples = int(duration * fs)\n",
    "\n",
    "# amplitude of the uniform noise\n",
    "noise_amplitude = 0.5\n",
    "\n",
    "# sine waves: (amplitude, frequency, normalized phase (0-1))\n",
    "sine_waves = [(0.5, 3.0, 0.5), (1.0, 17.0, 0.25), (0.4, 24.0, 0.0)]\n",
    "\n",
    "time = np.arange(nsamples) / fs\n",
    "\n",
    "noise1 = noise_amplitude * np.random.uniform(-1, 1, (nsamples,))\n",
    "noise2 = noise_amplitude * np.random.uniform(-1, 1, (nsamples,))\n",
    "\n",
    "signal = 0.0\n",
    "for wave in sine_waves:\n",
    "    signal = signal + wave[0] * np.sin(time * 2 * np.pi * wave[1] + wave[2] * 2 * np.pi)\n",
    "\n",
    "signal1 = signal + noise1\n",
    "signal2 = signal + noise2\n",
    "\n",
    "plt.plot(time, signal1)\n",
    "plt.plot(time, signal2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Power spectral density"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The (windowed) power spectral density is estimated with the **mtspectrum** function. The example below demonstrate the use of this function and illustrate the most important parameters.\n",
    "\n",
    "The function returns the power spectral density (*psd*), a vector of corresponding frequencies (*f*), lower and upper error bounds (*err*) if requested and a dictionary with the options used for PSD estimation (*options*)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# windowed multi-tapered spectrum of signal, averaged across 2 second windows\n",
    "psd, f, _, options = mt.mtspectrum(signal1, fs=fs, window_size=2)\n",
    "\n",
    "# multi-tapered spectrum of signal separately for 2 second windows\n",
    "psd, f, _, options = mt.mtspectrum(signal1, fs=fs, window_size=2, average=False)\n",
    "\n",
    "# windowed multi-tapered spectrum for specified epochs only\n",
    "# use start_time to set the time of the first sample in the signal\n",
    "# each epoch is split into window_size segments and the multi-taper\n",
    "# PSD estimates are computed for each segment\n",
    "psd, f, _, options = mt.mtspectrum(\n",
    "    signal1, fs=fs, window_size=2, start_time=0.0, epochs=[[1, 5], [7, 9]]\n",
    ")\n",
    "\n",
    "# windowed multi-tapered spectrum with non-default spectral bandwidth\n",
    "# and only for frequencies in range [0, fs/4.]\n",
    "psd, f, _, options = mt.mtspectrum(\n",
    "    signal1, fs=fs, window_size=2, bandwidth=1.0, fpass=fs / 4.0\n",
    ")\n",
    "\n",
    "# compute error estimates with desired p-value\n",
    "psd, f, err, options = mt.mtspectrum(\n",
    "    signal1, fs=fs, window_size=2, error=\"theory\", pvalue=0.01\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are interested in the averaged power spectral density around a set of time points (or *triggers*), then you can pass in an array of trigger times to **mtspectrum**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute averaged multi-tapered spectrum in windows around trigger times\n",
    "psd, f, _, options = mt.mtspectrum(\n",
    "    signal1, fs=fs, start_time=0, window_size=0.5, triggers=[1.0, 3, 7]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function **plot_spectrum** is a convenience function to plot the spectrum of a signal. This function takes the same parameters as the **mtspectrum** function and in addition you can specify several plotting options. For example, you can indicate whether or not the PSD should be plotted on a logarithmic scale (*db*), the units of the signal (*units*) and the color of the plot (*color*).\n",
    "\n",
    "If errors are estimated, then these are plotted as well. The plotting function returns the *axes* in which the spectrum is drawn, the plot elements (*artists*) and the output of the **mtspectrum** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "axes, artists, _ = mt.plot_spectrum(\n",
    "    signal1,\n",
    "    fs=fs,\n",
    "    window_size=2,\n",
    "    bandwidth=1.0,\n",
    "    error=\"theory\",\n",
    "    pvalue=0.01,\n",
    "    db=True,\n",
    "    units=\"mV\",\n",
    "    color=\"black\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Spectrogram"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A spectrogram is used to estimate the power spectral density for small (possibly overlapping) time windows in a longer nonstationary signal. The **mtspectrogram** function will estimate the multi-tapered spectrogram of a signal. The function returns the spectrogram, a (n,2) time array with start and end times of each window, a frequency vector, error estimates (if requested) and a dictionary with multi-taper options.\n",
    "\n",
    "The use of the **mtspectrogram** function is demonstrated below. Note that in order for the function to properly compute the returned window times, the correct *start_time* has to be specified. An additional *window_overlap* parameter sets the fraction of overlap between adjacent windows. Most other parameters are identical to **mtspectrum**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute spectrogram for 2 second windows with 50% overlap between windows\n",
    "psd, t, f, _, options = mt.mtspectrogram(\n",
    "    signal1, fs=fs, start_time=0, window_size=2, window_overlap=0.5, bandwidth=1\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute the spectrogram in a time window around events, you c can pass in a *triggers* parameter. In this case, the *trigger_window* parameter set the size of the window around the triggers and the *window_size* parameter sets the size of the (smaller) windows inside the trigger window for which the spectra are computed. The *trigger_window* parameter can be either a scalar that specifies a symmetrical window around the triggers (i.e. *trigger_window*=1 means a [-1,1] window) or a (left, right) sequence that specifies an arbitrary window."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psd, t, f, _, options = mt.mtspectrogram(\n",
    "    signal1,\n",
    "    fs=fs,\n",
    "    start_time=0,\n",
    "    triggers=[1.0, 3, 7],\n",
    "    trigger_window=0.5,\n",
    "    window_size=0.2,\n",
    "    window_overlap=0.5,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The convenience function plot_spectrogram is used to plot the spectrogram of a signal. \n",
    "\n",
    "The function **plot_spectrogram** is a convenience function to plot the spectrogram of a signal. This function takes the same parameters as the **mtspectrogram** function and in addition you can specify several plotting options. For example, you can indicate whether or not the PSD should be plotted on a logarithmic scale (*db*) and the units of the signal (*units*).\n",
    "\n",
    "The plotting function returns the *axes* in which the spectrum is drawn, the plot elements (*artists*) and the output of the **mtspectrogram** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "axes, artists, _ = mt.plot_spectrogram(\n",
    "    signal1, fs=fs, window_size=2, bandwidth=1, window_overlap=0.5, db=True, units=\"mV\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Coherence"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Coherence is the normalized cross-spectral density between two signals. The **mtcoherence** function computes the windowed coherence. The parameters are the same as for the **mtspectrum** function.\n",
    "\n",
    "The function returns the coherence, the phase angle, a frequency vector, error estimates (if requested) and a dictionary of multi-taper options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute coherence for two signals, averaged across 2 second windows\n",
    "coh, phi, f, _, options = mt.mtcoherence(\n",
    "    signal1, signal2, fs=fs, window_size=2, bandwidth=1\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The convenience plotting function **plot_coherence** plots the coherence of two signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "axes, artists, _ = mt.plot_coherence(\n",
    "    signal1, signal2, fs=fs, window_size=2, bandwidth=1, error=\"jackknife\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Coherogram"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Analogous to the spectrogram, the coherogram estimates the coherence for small (possibly overlapping) time windows in a longer nonstationary signal. **mtcoherogram** takes the same parameters as **mtspectrogram** and returns the coherence, phase angle, (n,2) time array of the windows, error estimates and a dictionary of multi-taper options. Note that error estimates are currently not calculated for coherograms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coh, phi, t, f, _, options = mt.mtcoherogram(signal1, signal2, fs=fs, window_size=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The convience plotting  function **plot_coherogram** plots the coherogram and behaves similar to the **plot_spectrogram** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "axes, artists, _ = mt.plot_coherogram(\n",
    "    signal1, signal2, fs=fs, window_size=2, window_overlap=0.5, bandwidth=2\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

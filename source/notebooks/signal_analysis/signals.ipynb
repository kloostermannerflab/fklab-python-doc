{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic signal algorithms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Module import"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fklab.signals.core as sig"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a test signal\n",
    "import numpy as np\n",
    "\n",
    "y = np.array([-1.0, 0.0, 2, 3, 5, 2, 2, 7, 8, 8, 8, 8, 2, 0, 0, -2, -1, 3, 4])\n",
    "x = np.arange(len(y)) * 10.0 + 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Detecting local extremes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To detect the local extremes (maxima and minima) in a signal, use the *localextrema*, *localmaxima* and *localminima* functions. Thess function will return the (interpolated) locations in the signal of the maxima and minima. If multiple samples in a sequence form an extreme, then the center of these samples is taken as the location of the extreme. \n",
    "\n",
    "There are two methods for detecting extremes in a signal, one is *direct* and the other uses the *gradient* of the signal. In the direct case, the difference is computed between neighbouring samples and all points where the preceding difference has opposite sign to the following difference is labeled an extreme. In the *gradient* method, first the derivative (gradient) is estimated and all zero crossings in the derivative are labeled extrema.\n",
    "\n",
    "See image below for examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "\n",
    "SVG(filename=\"images/localextremes.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here are code examples demonstrating the use of the localextrema, localmaxima and localminima functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# detect local extremes (both minima and maxima)\n",
    "# the function returns the (possibly fractional) index and value of the extrema\n",
    "idx, ym = sig.localextrema(y)\n",
    "\n",
    "# to return the location of extrema in an user specified index vector:\n",
    "xm, ym = sig.localextrema(y, x=x)\n",
    "\n",
    "# to only detect minima or maxima:\n",
    "xm, ym = sig.localextrema(y, x=x, kind=\"max\")\n",
    "xm, ym = sig.localextrema(y, x=x, kind=\"min\")\n",
    "\n",
    "# or use the convenience functions:\n",
    "xm, ym = sig.localmaxima(y, x=x)\n",
    "xm, ym = sig.localminima(y, x=x)\n",
    "\n",
    "# to use the gradient method for detecting extrema:\n",
    "xm, ym = sig.localextrema(y, x=x, method=\"gradient\")\n",
    "\n",
    "# by default the value of the extrema is computed using linear interpolation\n",
    "# to select a different interpolation method:\n",
    "xm, ym = sig.localextrema(y, x=x, method=\"gradient\", interp=\"cubic\")\n",
    "\n",
    "# to filter extrema by (absolute) amplitude\n",
    "xm, ym = sig.localextrema(\n",
    "    y, x=x, yrange=[2, None]\n",
    ")  # returns all extrema with absolute amplitude >= 2\n",
    "xm, ym = sig.localextrema(\n",
    "    y, x=x, yrange=[2, 4]\n",
    ")  # returns all extrema with absolute amplitude >= 2 and <= 4\n",
    "xm, ym = sig.localextrema(\n",
    "    y, x=x, yrange=[None, 4]\n",
    ")  # returns all extrema with absolute amplitude <= 4\n",
    "xm, ym = sig.localextrema(\n",
    "    y, x=x, yrange=[4, 2]\n",
    ")  # returns all extrema with absolute amplitude <= 2 or >= 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Detecting zero crossings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To find zero crossings in a signal, use the *zerocrossing* function. This function will return the (interpolated) locations in the signal where it crosses zero, either from positive to negative (down-slope) or negative to positive (up-slope). If multiple samples in a sequence are equal to zero, then the center of these samples is taken as the zero crossing. See image below for examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "\n",
    "SVG(filename=\"images/zerocrossings.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here are some code examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find (interpolated) indices of positive-to-negative and negative-to-positive zero crossings\n",
    "p2n, n2p = sig.zerocrossing(y)\n",
    "\n",
    "# to return the location of zero crossings in an user specified index vector:\n",
    "p2n, n2p = sig.zerocrossing(y, x=x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Detecting periods above threshold"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To detect segments in a signal where the value is above a threshold, use the *detect_mountains* function. It will return  a list of segments (start and end locations) for each detected 'mountain'. Either a single threshold or a double threshold can be used. In the latter case, the maximum value has to be above the upper threshold and the segment start and end locations are determined using the lower threshold. See image below for examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import SVG\n",
    "\n",
    "SVG(filename=\"images/detectmountains.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Examples of how to use the *detect_mountains* function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# to detect all start and end indices that mark a segment with values above threshold.\n",
    "# A single threshold is computed from the data as the 90th percentile.\n",
    "s = sig.detect_mountains(y)\n",
    "\n",
    "# to return the segment start and end in an user specified index vector:\n",
    "s = sig.detect_mountains(y, x=x)\n",
    "\n",
    "# to specify a single threshold\n",
    "s = sig.detect_mountains(y, x=x, low=1)\n",
    "\n",
    "# to specify two thresholds\n",
    "s = sig.detect_mountains(y, x=x, low=1, high=2)\n",
    "\n",
    "# to only detect mountains in a sub-segments of the signal:\n",
    "import fklab.segments as seg\n",
    "\n",
    "a = seg.Segment([10, 50])\n",
    "s = sig.detect_mountains(y, x=x, low=1, high=2, segments=a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

=====================
Statistical functions
=====================

Statistical functions (:mod:`fklab.statistics.core`)
----------------------------------------------------

.. currentmodule:: fklab.statistics.core

Collection of statistical functions.

.. automodsumm:: fklab.statistics.core
   :toctree: generated

Circular statistics (:mod:`fklab.statistics.circular`)
------------------------------------------------------

.. automodapi:: fklab.statistics.circular.circular
   :no-heading: 

.. automodapi:: fklab.statistics.circular.linear_circular_regression
   :no-heading: 

.. automodapi:: fklab.statistics.circular.simulation
   :no-heading: 

Correlation (:mod:`fklab.statistics.correlation`)
-------------------------------------------------

.. currentmodule:: fklab.statistics.correlation

Functions for correlation analysis.

.. automodsumm:: fklab.statistics.correlation
   :toctree: generated

Information theoretic functions (:mod:`fklab.statistics.information`)
---------------------------------------------------------------------

.. currentmodule:: fklab.statistics.information

Information theoretic functions.

.. automodsumm:: fklab.statistics.information
   :toctree: generated

Utilities for MCMC sampling (:mod:`fklab.statistics.mcmc`)
----------------------------------------------------------

.. currentmodule:: fklab.statistics.mcmc

Collection of utilities for MCMC sampling with pymc3.

.. automodsumm:: fklab.statistics.mcmc
   :toctree: generated

   
Particle Filter (:mod:`fklab.statistics.particlefilter`)
--------------------------------------------------------

.. currentmodule:: fklab.statistics.particlefilter

Implementation of particle filter plus related utilities.

.. automodsumm:: fklab.statistics.particlefilter
   :toctree: generated

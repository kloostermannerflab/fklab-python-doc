=====================
Geometrical functions
=====================

Geometrical functions (:mod:`fklab.geometry`)
---------------------------------------------

.. automodapi:: fklab.geometry
   :no-heading:

Geometrical shapes (:mod:`fklab.geometry.shapes`)
-------------------------------------------------

.. automodapi:: fklab.geometry.shapes
   :no-heading:
   :inherited-members:

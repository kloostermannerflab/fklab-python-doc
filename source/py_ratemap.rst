===============================
Rate map (:mod:`fklab.ratemap`)
===============================

.. currentmodule:: fklab.ratemap

Compute rate map

Histogram based rate map (:mod:`fklab.ratemap.histogram_based_rate_map`)
------------------------------------------------------------------------

.. currentmodule:: fklab.ratemap.histogram_based_rate_map

Compute event rate map

.. automodsumm:: fklab.ratemap.histogram_based_rate_map
   :toctree: generated

Kernel density based rate map (:mod:`fklab.ratemap.kernel_density_based_rate_map`)
----------------------------------------------------------------------------------

.. currentmodule:: fklab.ratemap.kernel_density_based_rate_map

.. automodsumm:: fklab.ratemap.kernel_density_based_rate_map
   :toctree: generated

Utilities (:mod:`fklab.ratemap.utilities`)
------------------------------------------

.. currentmodule:: fklab.ratemap.utilities

Utilities function for the rate maps

.. automodsumm:: fklab.ratemap.utilities
   :toctree: generated

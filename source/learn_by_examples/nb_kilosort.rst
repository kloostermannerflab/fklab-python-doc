*************
Spike Sorting
*************

.. toctree::
    :maxdepth: 2
    :glob:

    ../../notebooks/spike_sorting/Create a spike collection from kilosort output.ipynb
    ../../notebooks/spike_sorting/Play with the kilosort output.ipynb
    ../../notebooks/spike_sorting/load_lfp.ipynb
    ../../notebooks/spike_sorting/validation_spike_sorting_Kilosort2.ipynb

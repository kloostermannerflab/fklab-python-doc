**********
Statistics
**********

.. toctree::
    :maxdepth: 2
    :glob:

    ../../notebooks/statistics/*

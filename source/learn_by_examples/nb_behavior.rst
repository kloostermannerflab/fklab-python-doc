*****************
Behavior analysis
*****************

.. toctree::
    :maxdepth: 4
    :glob:

    ../../notebooks/behavior_analysis/run_calibration*
    ../../notebooks/behavior_analysis/run_transformation*
    ../../notebooks/behavior_analysis/Localize_workflow*
    ../../notebooks/behavior_analysis/learning_curve_estimation*
    

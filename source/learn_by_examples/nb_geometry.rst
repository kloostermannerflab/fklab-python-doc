********
Geometry
********

.. toctree::
    :maxdepth: 2
    :glob:

    ../../notebooks/geometry/*

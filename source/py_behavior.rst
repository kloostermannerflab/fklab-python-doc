===========================================
Behavior processing (:mod:`fklab.behavior`)
===========================================

.. currentmodule:: fklab.behavior

Collection of utilities to process behavioral data

Learning curve (:mod:`fklab.behavior.learning`)
-----------------------------------------------

.. automodapi:: fklab.behavior.learning
   :no-heading:

Behavioral task analysis (:mod:`fklab.behavior.task_analysis`)
--------------------------------------------------------------

.. automodapi:: fklab.behavior.task_analysis
   :no-heading:


Pre-processing (:mod:`fklab.behavior.preprocessing`)
----------------------------------------------------

.. automodapi:: fklab.behavior.preprocessing
   :no-heading:


Target tracker (:mod:`fklab.behavior.target_tracker`)
-----------------------------------------------------

.. automodapi:: fklab.behavior.target_tracker
   :no-heading:
   :inherited-members:


Behavioral data generation (:mod:`fklab.behavior.simulation`)
-------------------------------------------------------------

.. automodapi:: fklab.behavior.simulation
   :no-inheritance-diagram:
   :no-heading:




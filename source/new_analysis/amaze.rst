.. raw:: html

    <style> .red {color:red} </style>
.. role:: red

.. raw:: html

    <style> .green {color:green} </style>

.. role:: green


.. raw:: html

    <style> .blue {color:blue} </style>

.. role:: blue


Amaze
-----

To launch it, use the command **amaze** in the console. 

Amaze is used to specify the behavior environment. It takes in input the animal positions from the hdf5 file generated
by localize or/and an image of the maze. It returns an environment yaml file describing the different regions of the maze
and the tracks used to project position on it.


The Workflow :
''''''''''''''  

.. image:: ../../images/amaze.png
	   :width: 80 %
	   :align: center


#. Load the position of the animal (**button 2**) and/or an image of the environment (**button 3**) in order to visualize the environment of the experiments in the main blank space (**button 1**)


#. (Optional) with import, you can load, on top of the previous data, an environment file already existing (**button 4**)


#. Create an environment (**button 6**) 


#. Add regions to this environment by drawing on the maze, fields or objects (**button 8**). Example: reward field, sleep box ...etc. 


#. Add tracks to the environment (**button 8**): Tracks are used to projects position data on them and then, remove one dimension to the data. It makes it easy to correlate with the electro-physiological signal from the brain. 
 

#. Once a session is finished, you can save or export your environment file (**button 5**)

Troubleshoot : 
''''''''''''''

Do not forget to click on cancel in the first box if you want to stay in pixels and not cm.
If you want to stay in cm, use your pixel2cm coefficient present in your calibration file.

The environment file generated can be used in analysis. The file needs to load with

.. code-block::

    yaml.unsafe_load("environment.yaml")

It cannot use the safe mode as it will directly construct the fklab.geometry object described in the file. Check the fklab.geometry
package for more information on how to use it in your analysis.

Tracks, fields and other shapes in the environment :
''''''''''''''''''''''''''''''''''''''''''''''''''''

.. image:: ../../images/graph.png
	   :width: 25 %
	   :align: right


**Shapes**:

    - all shapes have a name, center and bounding box
    - all shapes can be transformed (scaled, rotated and translated) - *not implemented for multishape*

**Solids**:

    - all :blue:`solids` have an area
    - all :blue:`solids` have a contains method to test if points lie inside
    - all :blue:`solids` have a random_in method to generate random points inside the shape

**Paths**:

    - all :green:`paths` have a **length**
    - all :green:`paths` have a point2path method to project (x,y) points onto the path; the path2point method does the inverse. 
    - all :green:`paths` have the ability to compute the tangent and normal vectors at points on the path
    - all :green:`paths` have the ability to compute the distance between two points and the gradient for a sequence of points on the path
    - all :green:`paths` have random_on and random_along methods to generate random points on or along the path
    - all :green:`paths` can be binned into small segments

**Graphs**:

     - :green:`Graphs` consist of nodes connected with polyline edges 
     - The path length L of a graph is the sum of the length of all edges
     - The point2path method projects (x,y) points to the nearest edge and represents their location z in linearized space :red:`- linearization` 
     - Alternatively, the location of projected points is represented as (edge, zedge) :red:`- can be used to compute deviation of head direction`
     - The distance and gradient methods compute the distance along the path and not in linearized space :red:`- to compute velocity`
     - Binning should preferably be aligned to the nodes, at the expense of possibly uneven bin sizes :red:`- for histogram`

.. image:: ../../images/graph2.png
	   :width: 60 %
	   :align: center




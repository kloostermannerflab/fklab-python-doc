.. _electro_workflow::

Electrophysiological data - Action Potential analysis
-----------------------------------------------------

The analysis of high frequency data begin by filtering, detecting and sorting the spike in separate neuron clusters. It is based on spikes present on closed channels. To do this, a specific file called channelmap need to be created to explicit the relation between the channel (data acquisition index) with its actual position on the probe.

This file depend on the layout of the probe and the electrod chosen to record during the experiment. 


Spike sorting
.............

Spike sorting is done with Kilosort II by a job running in the nerf cluster. 

The fklab package has a wrapper code to automatically create and send a spike sorting job. 
(for the moment, available for neuropixels and OpenEphys - ask if you want for another type of data)
Your data have to be added in a mounted server on the nerf cluster. The input path should be the one in the nerfcluster-fs
and not the one on your computer. It always go like this for:

- SpikeGlx data: /mnt/server_name/path_inside_the_server/data_folder/data.ap.bin
- OpenEphys data: /mnt/server_name/path_inside_the_server/data_folder

.. codeblock: 
    usage: spike_sorting [-h] [--json_path JSON_PATH] [--input_path INPUT_PATH]
                         [--output_path OUTPUT_PATH]

    Launch a spike sorting job on the nerfcluster!

    optional arguments:
      -h, --help            show this help message and exit
      --json_path JSON_PATH
                            Path of an already existing json path accessible from
                            your local computer
      --input_path INPUT_PATH
                            Absolute path of your dataset in the server mounted on
                            the nerf cluster: Example(/mnt/server_name/path/filena
                            me_ended_by_ap.bin)
      --output_path OUTPUT_PATH
                            Absolute path for where to save the spike sorting
                            output in the server mounted on the nerf cluster:
                            (default: input_path/spike_sorting)


                        
In case of fail of the job, contact the cluster maintainer with the job command you launch and the log folder. 
This two information are displayed by the fklab script before sending the job. Don't forget to save it.

More informations:
- Kilosort II `here <https://github.com/MouseLand/Kilosort2>`_.

After that, a step of manual curation, with Phy, is done to improve the sorting and rank clusters (noise, good \... etc.) 

.. toctree::
    :maxdepth: 2
    :glob:

    ../notebooks/spike_sorting/Play with the kilosort output.ipynb

Spike collection 
................

From the output of Kilosort, a spike collection can be done either on every spikes after the Kilosort step or only on some clusters (labelled as good for example). 

A fklab.spikes.kilosort module has been developped to parse the differents files given as output.

.. toctree::
    :maxdepth: 2
    :glob:

    ../notebooks/spike_sorting/Create a spike collection from kilosort output.ipynb


Electrophysiological data - Low Frequency analysis
--------------------------------------------------

.. note:: spikeGLX has the particularity to separate in two meta and bin files the Low Frequency (LF) signals and the Action Potential signals. For the other acquisition system a filtering step is needed to separate both signals. 
 

.. toctree::
    :maxdepth: 2
    :glob:

    ../notebooks/spike_sorting/load_lfp.ipynb
    

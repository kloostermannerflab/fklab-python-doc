
Camera calibration
------------------

To launch it, just write "calibration" as command line. 

It takes in input some images with the presence of a chessboard and in output a calibration file which can be, then, used in the localize ui. 

The workflow:
.............


.. image:: ../../images/camera_calibration.png
	   :width: 80 %
	   :align: center

#.  Create new screenshots (**button 1**) or import images (**button 2**) by loading a new flux video. You can also import a previous calibration (**box 3**). 

 
#. Specify the role of each figure (for calibration or evaluation) (**box 4**) by using the checkboxes. Be careful,
for the evaluation part, images used need to be the ones with the chessboard at the level of the maze (or at a same level and orientation).


#. Compute the calibration parameters by hitting the button calibration (**button 8**). At this step, 3 parameters are asked before continuing : 
	
	- the size of the chessboard in both dimensions (black and white square numbers). If you are not satisfy of the calibration result, you can try to inverse both dimensions. The size used in the lab is 4 x 6.
	- the size of a square side in the chessboard: The size used in the lab is 6.7 cm


#. Evaluate the calibration parameters computed by hitting the button evaluation (**button 9**).
    The result will be displayed in the **box 10**. See the description of the evaluation parameters in the next section.


#. Save the new calibration parameters in the calibration.yaml file (**button 11**)


At any moment, images can be removed from the dataset (**button 5**) or display (**button 6**) in the **box 7**. 
However, for each movement in the dataset by modifying the role, adding or removing images, the calibration, evaluation and save button need to be pushed again.

calibration file : 
..................

It is always named the same way "calibration.yaml" in a directory next to all images used to compute parameters. 

The file is written in 4 parts: 

- **Calibration images**
	This part list the name of every image used to compute the calibration parameters

- **Evaluation images**
	This part list the name of every image used to evaluate the performance of the calibration

- **Options**
	Options are set at the beginning of a calibration and are immutable:
	- the **size of the chessboard** used to calibrate the image called nx and ny
	- **square size** represent the size in the real world

- **Calibration parameters**
	This part list all calibration parameter

	- The **camera matrix** should always be a 3x3 matrix with 

	.. math:: 
	 	
		\begin{bmatrix}
		        cx & 0 & a1 \\
		        0 & cx & a2 \\
		        0 & 0 & 1
		\end{bmatrix}


	cx and cy are the focal length of the camera. a1 and a2 represent the principal point is the point on the image
    which is directly below the center of the lens. It should ideally be the center of the image.

	- the **distortion matrix**: [k1 k2 p1 p2 k3]
        - the **image size** (in pixels)
        - **px2cm** : transformation from the 2d image in pixel to 3d world in cm$
        - **reprojection error** : error computed automatically during calibration by the opencv module

- **Evaluation result**
	This part list the result of the evaluation. 

	For the moment : 
		- **histogram**: path to a histogram of the square size of each chessboards present on the evaluation image before and after correction. Normally after distortion, the majority of square size should be close to 6.7cm (the real square size) but notice that some results corresponding to chessboard near to the edge of the picture could be worse than before.

		- **3d figure**: path to a 3d figure which display the variation of the square size between images distorted and undistorted in function of the position for all chessboard combined from evaluation images. 

		- **Standard deviation** of the square size of all square size of the chessboard before and after correction

		- **The average square size** of the chessboard before and after correction

		- **The camera focus** computed by the calibration. It should be near the centre of the image.


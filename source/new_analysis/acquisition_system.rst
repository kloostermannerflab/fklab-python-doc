.. _acquisition_system:

Acquisition system available in the lab
---------------------------------------


Neural data
...........


Neuralynx 
*********

Information about the software Cheetah can be found `here <https://neuralynx.com/software/cheetah>`_.

It has two functions with different outputs: 

- the electrophysiology recording
    - ncs : 1 file by channel containing the signal recorded
    - ntt : Spikes and waveforms for tetrodes
    - nev : Events

The module fklab.io.neuralynx is able to read the data and pre-process it. 

- the behavior recording with position tracking
    - video nvt (neuralynx format position) 
    - video smi (timestamp)
    - video mpg (video of the experiment)

*Data synchronisation* :  The advantage of Neuralynx is every timestamps from the behavior to the neural data are synchronized.
Now if we want to add external acquisition, processing... ect, there is also 4 TTL input ports on Cheetah.
For example, with Falcon, it is used to send via Arduino a TTL signal for ripple detection .

Open Ephys (Neuropixels probes)
*******************************

Open-Ephys software should be installed on the window computer used to plug the Neuropixels Acquisition system.
The Neuropixels plugin needs to be installed from the tab in the software to add new plugins.
Your chain needs to have at minimal a Neuropixels node and a record node.

.. warning::

    Set the recording format to the binary format as it is the only one able to record Neuropixels data

- Information about the software here : https://open-ephys.github.io/gui-docs/User-Manual/Installing-the-GUI.html
- Information about the Neuropixels node : https://open-ephys.github.io/gui-docs/User-Manual/Plugins/Neuropixels-PXI.html


SpikeGLX (Neuropixels probes)
*****************************

Information about this software can be found here: https://billkarsh.github.io/SpikeGLX

We assume that SpikeGLX will be used with the neuropixels. 
The first step before recording allows to choose the 384 electrodes to records along the 960. The most important at
this point is not forget to save the new setting.
The pre-setting can be also either the 1-long column, the checkerboard or the last block of electrodes.
The file for this type of configuration are available in the fklab repository : fklab.io.spikeGLX.config_files

The data recorded is filtered and separated in two datasets: 

- AP : (high frequencies)
- LF : (low frequencies) 

For each set, two files is given :

- bin file (with the actual data)
- meta file (with the metadata)

Both files can be parse and read via the module fklab.io.spikeGLX.readSGLX
This module has been developed by the SpikeGLX team and contain also a small gui to display some channels.

.. note::
    Often, the software catGT is used to remove fast transient in the raw data with a Global Demuxed Common Average Reference.

    The Demuxed part is important as channel are organised in subset with a slightly different references. With a Global CAR, the correction
    is less performant. However, if a Global CAR is enough for your purpose, the catGT step can be forget to use the CAR option in the spike interface pipeline
    when doing the spike sorting step.

More Info on this subject here: https://billkarsh.github.io/SpikeGLX/help/dmx_vs_gbl/dmx_vs_gbl

*Data synchronisation* : The synchronization of multiple probes should be done in the gui. However, in case of failure, it
can also be done post-processing with catGT.
For synchronization with external signal, it can be done with a synchronization pulse send both on the other acquisition system
(in our case, most often Neuralynx for the behavior acquisition) and the Neuropixels probe via the Digital Input on the PXIe module.
This digital signal will be recorded on the last channel.


Video position tracking with Cheetah (Neuralynx system)
*******************************************************


The tracking of the animal is done the detection of pixels that exceed a (color) threshold.
That is why two LEDs are positioned on the top of the animal head. Cheetah saves the transitions
and center points of the targets (color blobs) per video frame in an **.nvt** file. The tracking occurs in real-time
(this is what you can see during your recordings) but for higher accuracy we post-process the result offline using localize.

.. note::
   How to optimize Neuralynx tracking?

   The quality of the tracking depends very much on the lighting conditions in the room. The light level in the
   behavior rooms can be exactly controlled using qbus. Go to `qbuscloud <https://qbuscloud.com>`_ and login with the lab
   credentials (ask a lab member or go to the Fklab manual on google drive). Using the standard blue and red LED from
   neuralynx you end up with a good tracking if you **dim the light** to a **20% level** and set the blue and red threshold
   in cheetah to 30 and 8 respectively.


Video position tracking with Cheetah Realtime-tracking system (raspberry pi- fklab solution)
**********************************************************************************************

The tracking of the animal is done via a camera on a raspberry pi. There is different tracking method depending of the complexity of your experiment.

- Color tracking + Kalman filter: Easiest solution to setup working for any experiment using color Leds
  on the animal headset and no other (blinking) lights during the experiment.

- Deep learning model : Not yet implemented - for more difficult experiment to track.

The tracking can be setup by using the fklab tracking app.

.. codeblock:
    usage: tracking [-h] [--img IMG] [--roi ROI] [--ip IP]

    Setup your experiment for color tracking

    optional arguments:
      -h, --help  show this help message and exit
      --img IMG   Path of a screenshot from the experiment.
      --roi ROI   Path of the yaml from Amaze with all ROI defined
      --ip IP     IP address to connect to the raspberry pi

The workflow to setup the tracking with the app:

    - Request a screenshot: Your raspberry pi has to be the same condition with the led turn on on the maze.
      If you change one of this condition upper, you need to request a new screenshot to verify alignment with the mask
      and light threshold is still good.  Same conditions means:

            * position
            * maze already setup
            * Light turn in the same light condition as during experiment

    - Launch Amaze - load the screenshot and create in Amaze multiple environments

        - Environment "maze"

            * with a shape called "maze"  (Name are very important here) - create the mask of the maze / put in the shape the whole maze - be precise. Everything outside will be set to Nan

        - + As many environments needed knowing that the environment name is the event sent in `Falcon <https://falcon-core.readthedocs.io>`_. For example: (delayed / ontime)

            * the shape name is the event used in `Hive <https://fklab-controller-lab-doc.readthedocs.io>`_. For example: (Z1 / Z2 ... etc.)

    - save your environment file
    - come back in the tracking ui and click on create a mask - load the environment.yaml you created.
      if everything is correct, you should see, your different zones in different colors
    - choose the number of leds you need to track and set the Led threshold for each
    - adjust the threshold with the different barre and close when you are happy
    - select the options (save data = video, position file, read digital input)
    - You are ready for the tracking - put your animal in the maze and click to start the tracking. You can start and stop as many time you want.

The tracking configuration is saved in your local computer and upload in the raspberry pi each time the tracking is started.
The config folder is defined by where you are saving your screenshot. To reopened a previous configuration if nothing changed in the room,
instead of request a screenshot, you can directly load the one saved last time.

Deeplabcut (Offline tracking - based on any video video of the experiment)
**************************************************************************

The off-line tracking implementation of Deeplabcut is available in the NERF-cluster (make sure you have access to the main nerf servers).
The online documentation will walk you through the installation process and explain you how create you model.
Creating a new model is the most time-consuming step but you only need to do it once for each type of experiment.
Once you model is created, you can use it to analyze each video from that type of experiment.
The minimal output you need is the **.hdf5** file generated after analysis (called VT1DLC_model_used.h5).

.. note:: How to connect to the NERF-cluster?

   To access the NERF-cluster you should be in the NERF network.
   On the NERF sharepoint you can find a documentation that explains how to connect to the NERF network at an Imec site
   or from home: `starting_in_nerf <https://imecinternational.sharepoint.com/:b:/r/teams/t0007/NERF/Shared%20Documents/ICT/NERF%20COMPUTING/CONNECTIVITY%20ACCESS%20TO%20NERF%20RESOURCE/starting_in_nerf.pdf?csf=1&web=1&e=xEiQk9>`_.
   Next, documentation explaining how to connect to the NERF_cluster can be found here: `access_to_main_nerf_servers <https://imecinternational.sharepoint.com/:b:/t/t0007/NERF/EZBsa10oS7BBiISNnx11TfYBSo91_RkP5MsLyzcwnhKv6Q?e=MHkChj>`_.


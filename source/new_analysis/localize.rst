Localize
--------


Localize is used to generate the position file (HDF5 format) of the animal for each epoch in each session. You can see a programmatical version of localize in the notebook 

The input is a *session* folder containing (at least) :

- for **Neuralynx tracking** : 
   + VT1.nvt
   + info.yaml
   
- for **Deeplabcut tracking** : 
   + *.h5 or *.csv (preferred to use h5)
   + info.yaml with a datafile keyword given the absolute path of your custom position tracking file 
   + VT1.nvt (optional): only if you want the timestamp of your animal position synchronized with the timestamp from neuralynx neural data)
   + config_tracker.yaml (optional): if not using neuralynx data, some information related to the experiment will be missing, you can added them here but that's not mandatory as it is only related to the localize display and not the computation
   
- for **Raspberry tracking**: 
   + *.csv 
   + info.yaml with a datafile keyword given the absolute path of your custom position tracking file 
   + config_tracker.yaml (optional): some information related to the experiment will be missing, you can added them here but that's not mandatory as it is only related to the localize display and not the computation
   
   
**info.yaml**:


The info.yaml has several categories already constructed to fill. But one important is the time section for each epoch. By adding subtitle in the experience video ("VT1.mpg"), you can see the time of each frame. The epoch needs to be delimited by this time/1000000. if you don't using the neuralynx timestamps, the epoch have to be the frame number of the video.

If you want to use another tracking data file than VT1.nvt used by default, add in the info.yaml: 

.. code-block:: yaml

    datafile: absolute/path/datafile.(h5/csv)

**config_tracker**:


The config_tracker.yaml accept this keyword: 

.. code-block:: yaml

    fs: 25
    resolution: [720, 576]

The output is a folder named epochs containing a folder for each epoch specify in the info yaml with a H5 position file and a yaml position file.
Localize ui can be launched with the command "localize" in the console. 


The workflow :
..............

.. image:: ../../images/localize.png
	   :width: 80 %
	   :align: center

#. Load a session and then select an epoch in the list.

The first time that you load a session, the info.yaml file is read and an epochs folder with a separate folder
for each epoch is created. The first is chosen to be displayed in the ui but you can change it in the Epoch list.
Also, at this time, if no calibration file is present at the root of the session, the ui will ask if the user want to add one.
Once a calibration file has been added, it is automatically used to compute the location of the animal.

If you are paths are not following the standard configuration, you can always select path configuration and select one by one the different paths used by localize (source, epoch, info.yaml ... ect.) 

#. Data are displayed in a xy graph at the top and with some evaluation figures at the bottom:
	- position (x and y) in function of the time
	- speed in function of the time
	- both previous graphs in one 
	- head direction in function of the time
	- histogram of the angle difference between the diode orientation (adjusted in function of the orientation given in the **section 3**) and the head orientation. It should be centered around zero.  
	
The three dots on the left side allows you to hide on of the other part. 

# There are several options to enhance the display (**left pannel**):
    - show lines will transform the xy scatter plot in a lineplot, making easiest to see the time succession of positions.
    - show gaps will enlight in red the detected gap by localize
    - show outliers will enlight in bigger dot the detected outliers by localize
    - link time will link the top and bottom part as if you decide to zoom in the graph against time, it will also display only the selected part in the xy position.


#. There are several step to clean the data (**right pannel**): 
	- diode selection (**section 1**): indicate the orientation of the diodes with the head of the animal
	- ROi and time selection (**section 2**): 
	
        * include/exclude region: exclude regions not part of the maze to remove false position or include a specific region if you are interested only by a part of the maze. 
        * time window exclusion : example to remove two time sections --> [[start1,end1],[start2, end2]]
        
	- Correction options (**section 3**) : adjust the tracking of the animal 
	- Behavior estimation(**section 4**) : smooth the velocity and head direction computed by localize


#. To finish, saving will generate a position file (hdf5) useful for the next step of the analysis (by using amaze ui), and a position.yaml with all options used during the localize session. 
 

Current troubleshoot : 
......................

If the time specified in the info.yaml is wrong, the ui will crashed.
Then, modifying the info yaml won't be enough, the user needs also to modify the position yaml in the corresponding epoch. The most simple in that case is to remove the epoch folder concerned by the change of epoch time.


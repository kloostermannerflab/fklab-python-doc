.. _data_analysis:

Data analysis workflows
=======================

.. toctree::
    :maxdepth: 1

    acquisition_system
    behavior_workflow
    camera_calibration
    localize
    amaze 
    electro_workflow 



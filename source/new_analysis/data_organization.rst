.. _data_organization:

Data organization
=================

Metadata
--------

(YAML files, database, ...)

Each session has an info.yaml to be filed with the information of the experiment.

Example with tetrodes :

.. code-block::

    subject:
      species:
      strain:
      birthday: n
      id:
    procedures:
    - kind:
      date:
      comments:
      implantID:
    implants:
      24H3VB:
        kind:
        model:
        sensors:
        mapping:
          channels:
            0: CSC_TT1_1
            1: CSC_TT1_2
            2: CSC_TT1_3
            3: CSC_TT1_4
            4: CSC_TT2_1
            ...
          electrodes:
            TT1:
            - 0
            - 1
            - 2
            - 3
            TT2:
            - 4
            - 5
            - 6
            - 7
            ....
    experiment:
      date:
      experimenter:
      recording-quality:
        unit-hippocampus:
        theta:
      data_analysis:
      comments:
      pretraining:
    electrical-stimulation:
      ...
    datafile: if other than neuralynx tracking data, indicate here the behavior data path file (hdf5, csv file)
    epochs:
    - id: run
      time:
      - 960
      - 1365
      comments:
      environment:
        type:
        automated_doors:
        automated_food:
        comments:
      behavior:
        task:
        rewarded_arms:
        - 4
        - 5
        - 6
        discarded_trials:
        performance:
        comments:

Data storage
------------

If the nerf cluster has to be used (spike-sorting, Deeplabcut tracking), data should be saved on the fk-fileserver as
it is mounted on the cluster and directly accessible from it.
This data storage is also mounted in jupyterhub. Therefore, it useful to keep all your data in the fk-fileserver to ensure easy access from everywhere.


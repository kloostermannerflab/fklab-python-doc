
Behavioral data
---------------

In behavioral experiments the goal is often to correlate behavior with the recorded brain activity.
That is why the first part in the data-analysis workflow is dedicated to video position tracking and preprocessing of
the extracted position data. Video position tracking can be done by `Cheetah <https://neuralynx.com/software/cheetah>`_
(when the data was recorded with Neuralynx) or with the deep neural networks of `DeepLabCut <https://www.mousemotorlab.org/deeplabcut>`_.
Once the coordinates of the animal were successfully extracted from the video file you can perform a **camera calibration**
to account for visual distortion of the camera, **extract animal trajectory** form the diode coordinates and **clean up**
the position dataset, and finally you can also **specify the used environment** and **task rules**.

.. figure:: ../../images/behavioral_analysis.png
	   :width: 90 %
	   :figwidth: 90%
	   :align: center

	   **Graphical overview of the behavioral data analysis workflow**
	   
*Localize* will convert camera coordinates (obtained form video tracking) into diode coordinates and allows you
to clean up the data-set so that you have a clean trajectory representing the behavior of you animal. *Amaze* allows you
to define an environment (defining certain regions in space). Further fklab tools allow you to extract specific trajectories
or track visits to a certain *region of interest* (roi). There are also tools that let you search for specific task patterns.


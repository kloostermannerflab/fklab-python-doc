Starting a new analysis project
===============================

If you are new in the domain, before beginning, you may want to take a look in the FAQ to understand better
`how programming in python <FAQ/how-to-start/program_with_python.rst>`_, `how to use Git <FAQ/how-to-contribute/git_workflow.rst>`_
and `how to document your code <FAQ/how-to-contribute/documentation.rst>`_.


You are finally ready to start your first analysis project! Next step will be to learn the data organization and the data analysis workflow.
Finally, learn how to use in your analysis the fklab python module can be done from examples in notebook
or by going through the `reference <reference.rst>`_ documentation which list every available functions.

.. toctree::
    :maxdepth: 2

    new_analysis/data_organization
    new_analysis/data_analysis




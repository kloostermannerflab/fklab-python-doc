Signal Processing
=================

Core Algorithms (:mod:`fklab.signals.core`)
-------------------------------------------

.. currentmodule:: fklab.signals.core

Basic algorithms.

.. automodsumm:: fklab.signals.core
   :toctree: generated


Digital Filtering (:mod:`fklab.signals.filter`)
-----------------------------------------------

.. currentmodule:: fklab.signals.filter

Digital filtering functions.

.. automodsumm:: fklab.signals.filter.filter
   :toctree: generated
   :functions-only:


Smoothing (:mod:`fklab.signals.smooth`)
---------------------------------------

.. automodapi:: fklab.signals.smooth
   :no-heading:

Rate conversions (:mod:`fklab.signals.multirate`)
-------------------------------------------------

.. automodapi:: fklab.signals.multirate
   :no-heading:


Multitaper spectral analysis (:mod:`fklab.signals.multitaper`)
--------------------------------------------------------------

.. currentmodule:: fklab.signals.multitaper

Multitaper spectral analysis.

.. automodsumm:: fklab.signals.multitaper
   :toctree: generated


Independent Component Analysis (:mod:`fklab.signals.ica`)
---------------------------------------------------------

.. currentmodule:: fklab.signals.ica

Tools for indepedent component analysis (ICA).

.. automodsumm:: fklab.signals.ica
   :toctree: generated


Ripple (:mod:`fklab.signals.ripple`)
------------------------------------

.. currentmodule:: fklab.signals.ripple

Utilities for detection and analysis of hippocampal ripples

.. automodsumm:: fklab.signals.ripple
   :toctree: generated


Theta (:mod:`fklab.signals.theta`)
----------------------------------

.. currentmodule:: fklab.signals.theta

Utilities for the analysis of theta oscillations and theta cycle skipping

.. automodsumm:: fklab.signals.theta
   :toctree: generated
   
System time time alignment (:mod:`fklab.signals.time_alignment`)
----------------------------------------------------------------

.. automodapi:: fklab.signals.time_alignment
   :no-heading:


These installation instructions assume that you have a computer with Linux installed
(e.g. Linux Mint or Ubuntu). If Miniconda is already installed on your system, but
you would like to start over with a fresh install, then make sure to remove the old
Miniconda installation first.

#. Install Miniconda3
    
    Download Miniconda3 from http://conda.pydata.org/miniconda.html. We currently use
    Python 3.7 in the lab. Download the appropriate installer for your platform and
    follow the installation instructions on the website.
    
    For Linux (64 bit), this means that you download
    https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh.
    Next, in a terminal go to the directory that contains the downloaded file and execute:
    
    .. code-block:: console
    
        bash Miniconda3-latest-Linux-x86_64.sh
    
    During the installation, accept the default install location and answer `yes` to the
    question about initializing miniconda. Miniconda is installed in the *miniconda3* folder
    in your home directory.

#. Update conda package manager
    
    Miniconda provides the conda package manager and Python. The conda command can be used
    to install many python packages, including the FKLab specific python packages.
    
    After miniconda installation, verify that the conda package manager and Python are
    installed correctly. In a new terminal, check that the conda and python executables are
    installed in the miniconda3/bin directory by running the commands 
    
    .. code-block:: console
     
         which conda
         which python
    
    Next, we make sure that the conda package manager is updated to the latest version and 
    also install additional required packages:
    
    .. code-block:: console
    
        conda update conda
        conda install anaconda-client conda-build

#. Add channels to conda
    
    By default, the conda package manager searches for packages in the default channel that
    is managed by the creators of Miniconda.
    To install packages that are developed in the lab or by the large scientific Python
    community, we add two additional channels to conda: 
    
    .. code-block:: console
    
        conda config --append channels conda-forge
        conda config --append channels KloostermanLab
    
#. Become member of KloostermanLab organization
    
    Some packages on the KloostermanLab channel are marked private and only available to lab members. 
    To get access, sign up for a (free) Anaconda Cloud account on https://anaconda.org/.
    Next, ask the lab head to associate your account with the KloostermanLab organization on
    Anaconda Cloud. Only continue below when you are a confirmed member of the KloostermanLab
    organization and you can successfully login:
    
    .. code-block:: console
        
        anaconda login

.. note:: 
    How to uninstall Miniconda Python distribution?

    To uninstall miniconda, you can just remove the folder:

    .. code-block:: console

        rm -rf ~/miniconda3

    To also remove the configuration files (which contain the extra channels, among other things), do:

    .. code-block:: console

        rm -rf ~/.conda ~/.condarc ~/.continuum

    Finally, remove the lines from ~/.bashrc that were added by the miniconda installer



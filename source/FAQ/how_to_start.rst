How to start 
============

.. toctree::
    :maxdepth: 2
 
    how-to-start/program_with_python
    how-to-start/jupyter_notebook 


Maintain conda package
----------------------

To build conda packages from the git repositories, the maintainer manually triggers a bitbucket pipeline to create
the packages and upload them to Anaconda Cloud (**step 5**).
For fklab-* package, the process has been automatized. To trigger a release, a tag named as the release version need
to be pushed on the last commit in the repository.

Release Checklist :

- [] Merge and close branches into develop branches
- [] Ensure every commits on the develop branch have been review and approved. See list in the issue call release XX
- [] rename issue call release XX by replacing XX with the new release version
- [] merge the develop branch into the master, update the changelog with the new version, close issues with the release number
- [] verify pipelines are in green
    - release pipeline
    - trigger the release of the master documentation pipeline

.. code-block:: console

    git fetch # ensure local master and develop branches are uptodate
    git checkout master
    git merge develop
    # update changelog
    git add .
    git commit -m "update for release XX"
    git tag version_number
    git push origin master --tags


The actual version of this two packages are : 

.. csv-table:: 
   :header: fklab-python-internal, fklab-python-core, fklab-compressed-decoder

   .. include:: ../../internal_version.rst , .. include:: ../../core_version.rst, .. include:: ../../py_compressed_decoder_version.rst 

The pipeline uses a custom docker image "fklab/pybuild" that is available on dockerhub. The corresponding docker file
can be updated by the maintainer if necessary (**step 6**), after which the docker image on dockerhub is automatically rebuilt (**step 7**).

Overview of main git repositories
.................................

- **Core Python analysis tools**

:name: fklab-python-core
:description: FKLab Python packages and modules with core analysis tools.
:url: https://bitbucket.org/kloostermannerflab/fklab-python-core
:doc: `reference <../../reference.rst>`_

- **Advanced Python analysis tools**

:name: fklab-python-internal
:description: FKLab Python packages and modules with advanced analysis tools that are currently only available for lab members.
:url: https://bitbucket.org/kloostermannerflab/fklab-python-internal
:doc: `reference <../../reference.rst>`_


- **Compressed KDE decoder**

:name: fklab-compressed-decoder
:description: C++ library with Python bindings for performing Bayesian neural decoding using compressed kernel density estimators.
:url: https://bitbucket.org/kloostermannerflab/fklab-compressed-decoder
:python doc: `reference decoder <../../py_decode.rst>`_


- **Documentation of Python tools**

:name: fklab-python-doc
:description: This repository contains the documentation source for the FKLab Python tools.The documentation is written
              in RestructuredText and built using Sphinx.Building the documentation requires that fklab-python-core and
              fklab-python-internal packages tobe available on the Python path so that their doc-strings are accessible
              and can be integrated into the reference documentation.
:url: https://bitbucket.org/kloostermannerflab/fklab-python-doc

- **Writing scientific papers**

:name: paperbuilder
:description: PaperBuilder is a Python tool for building (scientific) manuscripts
              that include text, figures, tables, citations, equations, etc. The main
              idea behind PaperBuilder is that content and its structure (i.e. the text)
              is separated from presentation (i.e. layout) and data (e.g. statistics).
              PaperBuilder is particularly useful when the content of a manuscript
              (figures, tables, etc.) is dynamically generated and changes frequently.
              PaperBuilder is built on top of the document conversion tool pandoc and
              the pandoc-scholar extension for scholarly documents.
:url: https://bitbucket.org/kloostermannerflab/paperbuilder
:doc: https://paperbuilder.readthedocs.io/en/latest/

- **Conda package recipes**

:name: fklab-conda-recipes
:description: TBD
:url: https://bitbucket.org/kloostermannerflab/fklab-conda-recipes


- **Python build environment (docker)**

:name: docker-pybuild
:description: Docker image for building python packages in the Kloosterman Lab
:url: https://bitbucket.org/kloostermannerflab/docker-pybuild


- **Conda environment specifications**

:name: conda-env
:description: Conda environments used in the lab.
:url: https://bitbucket.org/kloostermannerflab/conda-env



Building conda packages from scratch
....................................

Packages that are available in the KloostermanLab conda channel are built according to a recipe.
The recipes are stored in a git repository that is hosted on Bitbucket: https://bitbucket.org/kloostermannerflab/fklab-conda-recipes.

The basic procedure is as follows:

#. Update package source
    
    A conda package is built from a source, such as a git repository (either local or on bitbucket) or an existing released package (e.g. on PyPI).
    If you are building a package for one of the FKLab repositories, then make sure that the version you want to package has been tagged.

#. Update recipe
    
    Clone the fklab-conda-recipes repository (if you haven't done so already). In the recipe, update the version and build numbers, update the source
    and the dependencies (if necessary).

#. Update version in the setup.py and _version.py

	Setup.py can also be programmed to automatically read the right version in the _version.py file.
 

#. Build package from recipe
    
    Next, build the recipe using:
    
    .. code-block:: console
        
        conda build <recipe-folder>

#. Upload package to KloostermanLab
    
    To upload a fresh package, do:
    
    .. code-block:: console
        
        anaconda upload <package> -u KloostermanLab
    
    Optionally, to automatically upload a successfully built package to the KloostermanLab organization, configure conda as follows:
    
    .. code-block:: console
        
        conda config --set anaconda_upload yes
    
    And when building a package, make sure to specify the destination channel as follows:
    
    .. code-block:: console
        
        conda build <recipe> -u KloostermanLab
    
    To do a test build without automatic uploading, do:
    
    .. code-block:: console
        
        conda build <recipe> --no-anaconda-upload

#. Test package
    
    Create a separate conda environment, install the newly built package and test if it works.




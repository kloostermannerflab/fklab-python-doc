
Maintain the conda environment
------------------------------

This repository contained also the conda environment files. The maintainer can manually updates the environment
specifications (**step 8**). The environment files are automatically uploaded to Anaconda Cloud (**step 9**).


Overview of conda environments
..............................

:fkbase: environment with the most commonly used packages for data science.


:fklab: environment with all FKLab Python tools (fklab-python-core, fklab-python-internal, py-compressed-decoder, paperbuilder), their dependencies and the most commonly used packages for data science.


:fkdev: similar to **fklab** environment, but without fklab-python-core and fklab-python-internal. For these two packages, their repository needs to be cloned locally and installed in development mode.


:decoderdev: similar to **fklab** environment, but without py-compressed-decoder and paperbuilder. The repository of py-compressed-decoder needs to be cloned locally and installed in development mode.


:paperbuilderdev: similar to **fklab** environment, but without py-compressed-decoder and paperbuilder. The repository of paperbuilder needs to be cloned locally and installed in development mode.


:environment: similar to **fklab** environment, but without fklab-python-core, fklab-python-internal and py-compressed decoder. It is used only to construct the docker image "fklab/docker-py-dependencies".



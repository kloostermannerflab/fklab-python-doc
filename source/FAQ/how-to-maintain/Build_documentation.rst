
Maintain the documentation
--------------------------

Automatically build and upload the documentation
................................................

The documentation is automatically build by the fklab-python-doc (**step 12**) repository on bitbucket.
A pipeline will automatically re-build the documentation by using sphinx and then update the kloostermannerflab.bitbucket.org website.

There are two pipelines triggered automatically by commits or merged on the master branch and the develop branch.
Both will open a docker image ("fklab/docker-py-dependencies" derived from "fklab/pybuild") containing the fkdev environment.

Pipeline for the master branch will install the latest packages for fklab-python-internal; fklab-python-core; py-compressed-kde
(code from master branch) from the KloostermanLab conda-channel.
The installation is done by mamba, with a resolver 10x more faster than conda.

if you need to add a new packages to build the doc, add the install step in the bitbucket pipeline. Do not rebuild a docker image as since
they changed the free policy it won't worked.

Documentation Release checklist for the master branch:
******************************************************

- [] Merge and close branches into develop branches
- [] git merge develop branch into master branch. (attention conflict - python release pipeline commit on the master branch
their new version files. Should also be merged into the dev branch.
- [] check the pipeline terminated in green

Pipeline for the develop branch will install fklab-python-internal; fklab-python-core; py-compressed-kde
repository from the latest commit on their develop branch.

Documentation Release checklist for the develop branch:
*******************************************************

- [] ensure in local the doc is correctly built based on the develop branches of fklab-python-X repositories
- [] merge branch / push commits into develop
- [] check the pipeline terminated in green

As the time building is limited, remember to combine your successive commits in one push.

Build and upload the documentation from scratch
...............................................

Configuration
*************

The documentation is based on sphinx and written in RestructuredText. The repository needs (at least) two files:

- index.rst : the front page of the website
- conf.py : the configuration of the documentation building with two important extensions:

	- numpydoc /sphinx.ext.napoleon for numpy style docstrings
	- nbsphinx for incorporating Jupyter notebooks


To build the documentation online, the html documentation generated need to be upload on the website bitbucket repository : account_name.bitbucket.org.git
The script below allows to do it manually but take note that in the case of the fklab.* package all of this is done by
pipeline automatically triggered by an update in the doc or a release of one of the package.

The versionning of the documentation is done with this tool : https://github.com/steinwurf/versjon

.. code-block:: console

    pip install versjon

#. **Create a temporary directory and clone the repository**

    .. code-block:: console

        mkdir _tmp_doc_build
        cd _tmp_doc_build
        git clone --depth 1 git@bitbucket.org:account_name/doc_repo.git doc
        git clone --depth 1 git@bitbucket.org:account_name/account_name.bitbucket.org.git static-doc

#. **Generate the documentation**

	.. code-block:: console

		cd doc
		make html VERSION=X.X.X # it will created a subfolder called the same version name in the build directory

#. **Remove the old static doc version and copy past the new version generated**

	.. code-block:: console

		cd ../static-doc
		rm -r  X.X.X
		cp ../doc/_build/X.X.X -r .

#. **Repeat for each doc version wanted by changing the conda env used to build for different fklab package version or the doc repository branch.**

#. **Create the index file to redirect and linked the different doc versions**

    .. code-block:: console

        versjon --user_templates=../doc/templated_versonjs


#. **Git workflow to push the new documentation online**

	.. code-block:: console

		git add .
		git commit -q -m "updated documentation"
		git push -q


#. **Clean up the workspace**

	.. code-block:: console

		cd ../../
		rm -rf _tmp_doc_build




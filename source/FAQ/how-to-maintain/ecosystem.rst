.. _guide_maintainer:

FKLab Python ecosystem
----------------------

We use a number of tools and workflows to develop and deploy analysis code
and documentation in the FKLab. Code and documentation is stored in multiple
git repositories, separated into packages with well-defined purpose.
For each package, the central repository is located in the cloud inside the
lab's `bitbucket account <https://bitbucket.org/kloostermannerflab>`_.

To simplify installation of FKLab Python packages for beginners, we build 
packages that can be installed with the conda package manager. These conda
packages are available through the KloostermanLab channel on the Anaconda
Cloud. Also hosted on the Anaconda Cloud are environment files that provide
a standard and tested set of packages (including FKLab packages) for data
analysis.

Documentation of the FKLab Python tools is compiled from the sources and
published to https://kloostermannerflab.bitbucket.io/ and https://paperbuilder.readthedocs.io.

The Figure below shows schematically the various steps in the use and
maintenance of the FKLab Python ecosystem. We define three different roles:
beginning users, power users and maintainers. Each step in the workflow is
performed by one of the roles, or is automated.

.. image:: ../../../images/fklab_python_ecosystem.png
	   :width: 50 %
	   :align: center


Different levels of users
.........................

#. **Beginning users**
	For beginning users to get started, they need to download and install
	miniconda and create a conda environment that includes FKLab packages
	(**step 1**).
	The installation guide can be found in the `getting started <../../set-up/getting_started.rst>`_  page.

#. **Power users**

	For power users to get started, they need to download and install
	miniconda, create a conda environment with all required dependencies (**step 2**)
	and clone and "install" the FKLab git repositories (**step 3** and **step 4**).
	After this, they can add/improve code, push code to the central git repository
	on bitbucket, review and test code contributed by others and document the code.

	The installation guide can be found in the `guide for power user <../../set-up/guide_power_user.rst>`_.

#. **Maintainers**

	The maintainers users need to clone all fklab repositories and install fklab environment.
	The role of the maintainer is:

	- to release new package version when important features has been added in the repository
	- to build the documentation when it is updated

	This two parts are described below.  



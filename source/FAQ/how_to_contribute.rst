How to contribute
=================

.. toctree::
    :maxdepth: 2

    how-to-contribute/git_workflow
    how-to-contribute/documentation
 

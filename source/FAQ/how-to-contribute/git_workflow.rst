by updating a repository with git?
----------------------------------

For tutorials of how to properly work with local copies of the repositories see
https://confluence.atlassian.com/get-started-with-bitbucket/work-on-a-repository-861185298.html


.. image:: ../../../images/git_workflow.svg


In case of trouble to follow the workflow :


.. image:: ../../../images/troubleshoot_git_workflow.svg

Some additional commands: 

	- always check in which branch your local repository is before modifying anything. 
	
		.. code-block:: console
		
	       		git branch

		To change the branch you are using it 

		.. code-block:: console
		
	       		git checkout [branch_name]

	- check the state of the branch with 

		.. code-block:: console
		
	       		git status 

	- don't push on the master branch



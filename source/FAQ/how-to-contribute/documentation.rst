By updating the documentation?
------------------------------

Every lab member is strongly encouraged to contribute to and improve the documentation. 

This documentation is automatically build from 3 pieces: 

- Update the documentation website - instructions/guides/faq
- Update the notebooks - code example
- Update the code API reference - the documentation that is part of and describes the python code


People can contribute to any of 3 pieces. Identify which part of the documentation you would like to edit and what you would like to add/change. 


Update the documentation website
................................

The docstring in the fklab tool is following the `numpydoc <https://numpydoc.readthedocs.io/en/latest/format.html>`_.
Then, it is integrated with the help of automodapi extension which automatically generate the documentation for functions and classes specify in __all__ module.


For a new module, where no documentation is already existing, it needs to be add in the reference toctree. 
For this, create a file, named "py_module-name.rst" in source. 

For example, you can open every file "py*".

Their are two methods : automodsumm and automodapi with some style difference. First, automodsumm will generate only
the class and function documentation without the module level description. It allows you to keep in the doc this description.
Automodapi give you a cleaner documentation with the class and function separated and if you want a inherited dependency graph.
It will also import the module level documentation. This will create a problem in the title level. It is why the sub-title
should be always added here and removed from the module level documentation.

Finally, this file needs to be add in the toc-tree in the file "source/reference.rst".

.. note:: The previous method to generate docstrings was to used automodule and autosummary in the module-level documentation.
If you are using automodsumm and automodapi, you should remove every autosummary list in your module as it is doing the same thing in a different way.

Update the notebooks
....................

Notebooks are in the folder Notebooks separated in subfolder following their themes. Notebooks are integrated in the doc
by adding a file (if new subfolder) in source/learn_by_examples, named "nb_subfolder_name".
For example, you can open every file "nb*".

Then, this file is added in the toctree of the file source/learn_by_examples.rst. 
The notebook can be already run or strip out (without output).In the last case, it will run at building time. 

Update the code API reference 
.............................

TBD


Build the doc in local
......................

*If you never used git or conda before, go back to the* `developer guide <../../set_up/guide_power_user.rst>`_ *to prepare your environment.*

#. Go in your fklab-python-doc local repository and build the doc in local.

    .. code-block:: console

	cd fklab-python-doc
        conda activate doc_builder_env
        make html 

#. In the console, a dropbox token is asked. Go in `dropbox app <https://www.dropbox.com/developers/apps>`_. You need to be connected to the kloosterman Lab account.


#. Create a new app named "fklab_your_name" and clicked on generate a token. Copy past the token in the console. You can close the dropbox app. Now you will always have access to the dropbox file when using the fklab.io.cloud.dropbox_API module.


#. The documentation build take around 5mn. It will generate the reference documentation based on the docstring in the fklab module and run notebooks. Everything is then generated in html format. When the notebook is containing a dataset, it is automatically download from dropbox and store in a dataset folder in your desktop. You can remove it once you finished to work with the documentation.

#. Once the build is finished, you can go in _build/html folder and open the index.html file.


Push your modification online
..............................

.. note:: For small modifications, you can also change it directly online

	 * Go to the corresponding repository on Bitbucket. 
	 * Depending on the kind of changes you made, file an issue to notify others and start a discussion. 
	 * Edit the appropriate file by clicking on the edit button in the top right corner.
	 * When you're finished commit the changes (by clicking commit in the bottom right corner) and indicate you want to create a pull request.  In this way your changes will be pushed in a separate branch and the pull requests let you tell others about changes you've pushed. Once a pull request is opened, you can discuss and review the potential changes with collaborators and add follow-up commits before your changes are merged into the master-branch. 
	 * Rename the branch properly and optionally assign reviewers. 
	 * Once changes are final merge the branch with the master branch by going to the tab 'branches' on the left in bitbucket. Go to your branch and click on merge (top right corner). Now others can pull your changes

#. Ensure that the pre-commit hook has been installed in the repository. It is very important as it will strip out the notebooks and avoid to upload heavy files in the online repository. 

.. code-block:: console

    pre-commit install 

#. Create your own branch. In your branch, don't be worry to break something or wrote bad things, it won't be uploaded on the website without a maintainer approval. 

.. code-block:: console

    git checkout master
    git checkout -b meaningfull_name_branch   # This create a new repository version just for you

#. Do your modification in the documentation - you can re-build the doc to see how your modification looks like in the website.

#. Once you finished your modification, commit and push it online. Redo the commit will the automatic modifications if pre-commit tests failed.

.. code-block:: console

    git commit -a -m "commit message explaining your modification'
    git push master branch_name




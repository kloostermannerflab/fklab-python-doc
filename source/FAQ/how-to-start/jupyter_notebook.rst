
How to work with Jupyter Notebooks
----------------------------------

A Jupyter Notebook is a document that contains live (Python) code, equations, visualizations and explanatory text.
In the FKLab we mainly use notebooks as an educational tool, to demonstrate the use of python modules and functions,
and to provide examples of data analysis approaches.

You can see existing analysis notebooks in `learn by example <../new_analysis_learn_by_examples>`_).
Almost all notebooks are runnable with the current version of the fklab package. You can download them in this bitbucket
repository:  (https://bitbucket.org/kloostermannerflab/fklab-python-doc/src/master/source/notebooks)

To open a notebook, you need to create the jupyter notebook server :

.. code-block::

    conda activate fklab #(or fkdev depending of your environment)
    jupyter notebook

A page will be open in your browser. You can select an already existing notebook or create a new one by selecting the
python3 kernel.

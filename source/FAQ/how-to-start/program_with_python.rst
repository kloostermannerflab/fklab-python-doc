.. _program_with_python:

How to program with Python?
===========================

scientific libraries
-------------------- 

- numpy
- scipy
- pandas
- matplotlib
- scikit-learn
- scikit-image

To learn more about the scientific libraries used in the fklab package : see https://scipy-lectures.org/index.html


Editor
------

- jupyter
-  spyder


PEP8 standard 
-------------

To keep a consistency in the code standard, the PEP8 standard is highly recommanded. 

run regurlarly pylint
.....................

Pylint is similar as blake because it is a linker but it won't actively modified your file.
It gives the same style information as black plus standard warning more related to the code as for example :

- bad variable name
- unused import
- missing docstring

pylint is already install in the conda environment used in the lab and can be run in the console.
		
.. code-block:: console

    pylint filename.py

 
Best practices
--------------

To learn more about scripts vs functions, see:

- https://scipy-lectures.org/intro/language/reusing_code.html
- https://docs.python-guide.org/

Some good practices:

- **DRY**: don't repeat yourself
- **KISS**: keep it simple, stupid
- Avoid premature optimization
- Avoid premature generalization



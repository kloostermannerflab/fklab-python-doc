Content table
=============

.. toctree::
    :maxdepth: 3

    set_up
    starting_new_project
    learn_by_examples
    reference
    FAQ/how_to_start
    FAQ/how_to_contribute
    FAQ/how_to_maintain
    FAQ/report_bugs
    FAQ/todo

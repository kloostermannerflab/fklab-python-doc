Reference
=========

Know what you’re looking for & just need API details? View our auto-generated API documentation:

.. toctree::
    :maxdepth: 2
    
    py_codetools
    py_utilities
    py_segments
    py_events
    py_signals
    py_statistics
    py_geometry
    py_io
    py_radon
    py_plot
    py_behavior
    py_camera_calibration
    py_spikes
    py_ratemap
    py_replay
    py_computation_dependency_tree
    py_decode

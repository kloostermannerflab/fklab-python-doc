How-to guides
=============

Some of the notebook tutorial need a dataset to run.
The python snippet at the beginning of a notebook will allow you to automatically download the dataset from the FKlab dropbox. 
However, the first time that you want to use your dropbox account in your code, you need to create an access token.

#. go in your `dropbox account app <https://www.dropbox.com/developers/apps>`_

#. create a new app : set the app type (dropbox API), the right access (all files or only some folder), and the app name : "fklab_your_name"
 
#. go in generate an access token, copy this token and validate your app

The first time, you will use the dropbox api module, this token will be ask in the console. Once it has been loaded once,
it does not need to be keep for future use. The token is very confidential and should not be wrote or shared.
In case of doubt, you can always remove your app and create a new one. It will deactivate the access to everyone using illegally this token.


.. toctree::
    :maxdepth: 2

    learn_by_examples/nb_theory
    learn_by_examples/nb_basic
    learn_by_examples/nb_kilosort
    learn_by_examples/nb_statistics
    learn_by_examples/nb_geometry
    learn_by_examples/nb_file_io
    learn_by_examples/nb_behavior
    learn_by_examples/nb_ratemap
    learn_by_examples/nb_signal_analysis
    learn_by_examples/nb_replay
    learn_by_examples/nb_decoding



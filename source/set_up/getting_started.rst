Getting started
===============

Install Miniconda Python distribution
-------------------------------------

.. include:: ../FAQ/install_miniconda.rst


Create environment with FKLab Python tools
------------------------------------------

Conda has the concept of *environments*, which are isolated installations of
Python plus a set of packages. After installation of Miniconda, only a single
root environment is created.
Importantly, the root environment is special (as it contains the conda package
manager) and should be kept as clean as possible. For our actual data analysis,
we create a dedicated environment with the desired python packages.

The simplest way is to download the pre-configured fklab environment from the
KloostermanLab channel:

.. code-block:: console

    conda env create KloostermanLab/fklab

This will create the new fklab environment with the FKLab python packages and
all their dependencies. To switch from the root to the fklab environment, do:

.. code-block:: console

    conda activate fklab
    
Notice that the name of the current environment is prepended to the command
line prompt.

If you would like to or need to switch back to the root environment, you do:

.. code-block:: console

    conda deactivate
    
Normally, the root environment is activated every time you open a new terminal
and you would have to manually switch to the fklab environment. To make the
fklab environment the default environment, add the following line to the end
of the .bashrc file in your home directory:

.. code-block:: console
    
    conda activate fklab

Update your environment regularly

.. code-block:: console

    conda env update -n fklab -f KloostermanLab/fklab

Getting started without any installation
========================================

Creating your programming environment on your computer is a good thing for flexibility but if you want to go really fast 
and start analysis as soon as possible, you can use the jupyterhub installed on a fklab server. 


**Step 1** : Connection to Jupyterhub

- Connect on the nerf network via imec wifi network or via ethernet or via vpn
- connect in your browser to this website : https://fksuperpc1.imec.be
- Create your account with a username and a password

**Step 2** : Exploration

This is opening a new session persistent in the time and personnalized for you. You can save your files in your home (/home/jovyan) but
even more practical you also have access to the fk-fileserver in the data folder (two paths to reach it : /home/jovyan/data or /mnt/fk-fileserver). 
To access your data from anywhere, the most easy way then is to save them in the fk-fileserver. 

In the documentation folder, you can also find the same notebooks as on this doc website. You can run it, modify it but careful the folder is read-only. If you want it persistent in time, you need to copy the file in your home. 


**Step 3**: Develop your analysis

Select one kernel (with or without debugger) and open a jupyterlab notebook. You can start importing modules and running analysis, congrats!

If you want, you can also install your own conda environment as you would do it in local with the console. See next steps to see how. 


**Step 4**: Create your own fkdev environment

You can create any conda environment but here a snippet to quickly create a clone of the existing environment in jupyterhub
but with the adding benefite of being able to modify it.

Open a console in your jupyterhub home.

.. code-block:: console

    conda init bash
    conda create -n fkdev --clone base
    conda activate fkdev
    git clone https://<USER>@bitbucket.org/kloostermannerflab/fklab-python-core.git
    cd fklab-python-core
    git checkout develop
    python setup.py build_ext --inplace
    pip install -e . --no-deps --user
    cd ..
    git clone https://<USER>@bitbucket.org/kloostermannerflab/fklab-python-internal.git
    cd fklab-python-internal
    git checkout develop
    python setup.py build_ext --inplace
    pip install -e . --no-deps --user
    cd ..

Guide for developer users
=========================


Install Miniconda Python distribution
-------------------------------------

.. include:: ./../FAQ/install_miniconda.rst


Install FKLab dependencies
--------------------------

- **Set up compiler**

For compiling C++ extensions, the gcc compiler needs to be installed:

.. code-block:: console
    
    sudo apt install g++

- **Set up git version control**

Code, documentation and notebooks are stored in git repositories that are hosted on the lab's bitbucket account (https://bitbucket.org/kloostermannerflab).
To install git, do:

.. code-block:: console
    
    sudo apt install git

And configure git with your full name and email address:

.. code-block:: console

    git config --global user.name "Your Name"
    git config --global user.email your.email@nerf.be

Finally, ask the lab head to add you to the lab's bitbucket team account.


Clone and install FKLab repositories
------------------------------------

#. **Clone fklab code repositories**
    
    The first step is to clone the repositories. The fklab-python-core repository is public and
    accessible for outside researchers. The fklab-python-internal repository is private and only
    for use inside the FKLab. As a general rule, bug fixes and enhancements of *existing* code
    can be applied to either repository, but new features are generally first added to the
    fklab-python-internal repository. More mature and/or published tools will eventually be moved
    from the internal to the core repository.

    To clone the core and internal code repositories, execute the following commands in which <PATH>
    should be replaced by the path where the repositories will be saved, and <USER> should be set to
    your bitbucket.org user name.
    
    .. code-block:: console
        
        cd <PATH>
        git clone https://<USER>@bitbucket.org/kloostermannerflab/fklab-python-core.git
        git clone https://<USER>@bitbucket.org/kloostermannerflab/fklab-python-internal.git

    You will also need to clone the doc repository to update it when your modifications are done. 

    .. code-block:: console

        git clone https://<USER>@bitbucket.org/kloostermannerflab/fklab-python-doc.git

#. **Set up conda environment**
    
    The next step is to set up a conda environment that has all necessary python tools and dependencies installed for the fklab-python code to work properly.
    The predefined KloostermanLab/fkdev environment can be used for this purpose:
    
    .. code-block:: console
        
        conda env create KloostermanLab/fkdev
    
    Optionally, make the fkdev the default environment by adding `conda activate fkdev` to your .bashrc file.

#. We will used a specific conda environment for building the doc.

    .. code-block:: console

        conda env create KloostermanLab/doc_builder_env 

#. **"Install" repositories**
    
    The git repositories still need to be put on the Python path. First, make sure you have
    activated the development environment, e.g. `conda activate fkdev`. Next, for each
    fklab repository, do (where <REPO> is the path to the cloned repository):
    
    .. code-block:: console
        
        cd <REPO>
        python setup.py build_ext --inplace
        pip install -e . --no-deps
        
    
    This will first build all C/C++ extensions in place and add a link to the repository in
    Python's module search path.

#. **Install pre-commit hooks**


To keep a consistency in the code standard, the PEP8 standard is highly recommended.

Both fklab repository are developed by using the pre-commit library.
The library is already configured with this framework :

- Black (active linker)
- remove trailing whitespace
- End-line document
- reorder python import 

For each repository, set the pre-commit config for the fklab-python-core, fklab-python-internal and fklab-python-doc repository

    .. code-block:: console
			
	cd <REPO>
	pre-commit install 

.. note:: If your commit did not pass all tests, the hook will actively modified your files to respect the code standard and failed your commit. It means that you need to add the modification done by pre-commit and commit again before pushing online. Everything is wrotten in the output of the git commit, takes time to read it ! 

	Therefore, the git workflow will look like this : 
	
	#. Open a new branch 
	
	.. code-block:: console

		git checkout -b meaningfull_name_branch

	#. Do and commit the modification

	.. code-block:: console

		git add modif_files
		git commit -m "description of the modification 

	Then, all tests in pre-commit are run.

	If some modification are actively done, the commit will failed. At this moment, files have been modified. This modification can be seen by running git diff. Add this modification and re-create your commit.

	.. code-block:: console

		git add modif_files
		git commit -m "description of the modification 


After the above steps, you should be able to switch to the fkdev, open an ipython interpreter and import the fklab modules.

Update your environment regularly

.. code-block:: console

    conda env update -n fkdev -f KloostermanLab/fkdev

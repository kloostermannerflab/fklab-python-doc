====================================================
Camera calibration (:mod:`fklab.camera_calibration`)
====================================================

.. currentmodule:: fklab.camera_calibration

Collection of utilities to process behavioral data


Calibration (:mod:`fklab.camera_calibration.calibration`)
---------------------------------------------------------

.. currentmodule:: fklab.camera_calibration.calibration

Calibration class to calibrate from a set of images, evaluate from another set of images and then save in a yaml file

.. automodsumm:: fklab.camera_calibration.calibration
   :toctree: generated



Transformation (:mod:`fklab.camera_calibration.transformation`)
---------------------------------------------------------------

.. currentmodule:: fklab.camera_calibration.transformation

Module API for undistort/distort points/images based on calibration coefficient

.. automodsumm:: fklab.camera_calibration.transformation
   :toctree: generated



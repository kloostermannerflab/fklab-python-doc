Installing Fklab analysis tool
==============================


**Don't want to lose time and install anything ?** Jump directly in the analysis world and connect you the fklab jupyterhub. 

.. toctree::
    :maxdepth: 3

    set_up/jupyterhub

**All new users** should first follow the complete `guide to get started <set-up/getting_started.rst>`_.
This guide will show you how to set up Python and install the FKLab Python tools so that you can use them in your analysis.

.. toctree::
    :maxdepth: 3

    set_up/getting_started

**As you gain experience** with the FKLab Python tools and related utilities, read the `guide for developer users <set_up/guide_power_user.rst>`_
to learn about how you can contribute to improving the tools.

.. toctree::
    :maxdepth: 3

    set_up/guide_power_user 




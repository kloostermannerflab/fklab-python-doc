==================
Data visualization
==================

Plotting functions (:mod:`fklab.plot.plotfunctions`)
----------------------------------------------------

.. currentmodule:: fklab.plot.plotfunctions

Low-level plotting functions.

.. automodsumm:: fklab.plot.plotfunctions
   :toctree: generated

Artists (:mod:`fklab.plot.artists`)
-----------------------------------

.. currentmodule:: fklab.plot.artists

Custom matplotlib artists.

.. automodsumm:: fklab.plot.artists
   :toctree: generated

Utilities (:mod:`fklab.plot.utilities`)
---------------------------------------

.. currentmodule:: fklab.plot.utilities

Plotting utilities

.. automodsumm:: fklab.plot.utilities
   :toctree: generated

Interaction (:mod:`fklab.plot.interaction`)
-------------------------------------------

.. currentmodule:: fklab.plot.interaction

Utilities for interaction with matplotlib figures

.. automodsumm:: fklab.plot.interaction
   :toctree: generated

Neuralynx plot tools (:mod:`fklab.plot.neuralynx`)
--------------------------------------------------

.. currentmodule:: fklab.plot.neuralynx

Neuralynx plotting tools.

.. automodsumm:: fklab.plot.neuralynx
   :toctree: generated


Openephys plot tools (:mod:`fklab.plot.openephys`)
--------------------------------------------------

.. currentmodule:: fklab.plot.openephys

openephys plotting tools.

.. automodsumm:: fklab.plot.openephys
   :toctree: generated

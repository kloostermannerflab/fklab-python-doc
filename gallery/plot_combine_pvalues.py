"""
Combine p-values
----------------

This example shows how you can plot the rejection regions
for the combined p-value of two independent tests.

The function `fklab.statistics.core.combine_pvalues`
calculates a combined p-value from multiple p-values that are
obtained by multiple independent tests of the same null
hypothesis.

The function is a thin wrapper around `scipy.stats.combine_pvalues`
and adds two methods ('friston' and 'nichols') and implements a 
work-around for a bug in scipy
(see https://github.com/scipy/scipy/issues/15373).

"""

import itertools

import numpy as np
import matplotlib.pyplot as plt

from fklab.statistics.core import combine_pvalues


# we select a few methods
methods = ["fisher", "stouffer", "nichols", "friston", "pearson", "tippett"]

# select p-values for the original two tests
# between 0.001 and 1 on a logarithmic scale
pvalues = np.logspace(-3, 0, 50)

# compute combined p-values for each pair of
# p-values derived from two independent tests.
combined_pvalues = {
    method: np.array(
        [
            combine_pvalues((a, b), method=method)
            for a, b in itertools.product(pvalues, repeat=2)
        ]
    ).reshape((len(pvalues), -1))
    for method in methods
}

# create a figure with an axes for each method
fig, axes = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(12, 6))

for k, (method, p) in enumerate(combined_pvalues.items()):

    # destination axes
    ax = axes.ravel()[k]

    # plot log-transformed combined p-values
    img = ax.pcolormesh(
        pvalues,
        pvalues,
        np.log10(np.clip(p, 0.0001, 1)),
        shading="auto",
        cmap="magma_r",
        vmin=-4,
        vmax=0,
    )

    # show original p-values also on log scale
    ax.loglog()

    # draw contours for the typical alpha levels
    contour = ax.contour(pvalues, pvalues, p, [0.01, 0.05], colors="white")
    ax.clabel(
        contour,
        contour.levels,
        fmt=lambda l: "{:.2f}".format(l),
        inline=True,
        fontsize=12,
    )

    # set some axes properties
    ax.set(xticks=[0.001, 0.01, 0.1, 1], xticklabels=[0.001, 0.01, 0.1, 1])
    ax.set(yticks=[0.001, 0.01, 0.1, 1], yticklabels=[0.001, 0.01, 0.1, 1])

    ax.set(
        xlabel="p-value of test 1",
        ylabel="p-value of test 2",
        title="method = {}".format(method),
    )

# hide x labels and tick labels for top plots
# and y ticks for right plots.
for ax in axes.flat:
    ax.label_outer()

# draw colorbar
cb = plt.colorbar(img, ax=axes, label="combined p-value", fraction=0.1, shrink=0.5)
cb.set_ticks([-4, -3, -2, -1, 0])
cb.set_ticklabels([0.0001, 0.001, 0.01, 0.1, 1])

plt.show()

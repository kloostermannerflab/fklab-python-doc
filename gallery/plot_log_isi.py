"""
Inter-spike interval distribution
---------------------------------

This example shows how you can plot the distribution of
log inter-spike intervals.

The `fklab.spikes.plot.plot_log_isi` function computes a
kernel dernsity estimate of the log10-transformed inter-spike
intervals. The bandwidth of the smoothing gaussian kernel
should thus be expressed in log10 units.

If no destination axes is provided to the function, the 
isi distribution will be plotted in the current axes (or a new
figure and axes will be created if none exist yet).

"""

import numpy as np
import matplotlib.pyplot as plt

import fklab.spikes.plot

# options
nspikes = 1000
spike_rate = 2.0  # Hz
bandwidth = 0.1  # log10 units

# create random spike intervals
spike_intervals = np.random.exponential(scale=1 / spike_rate, size=(nspikes,))

ax = fklab.spikes.plot.plot_log_isi(spike_intervals, bandwidth=bandwidth)

plt.show()

"""
Spike train correlations
------------------------

This example shows how you can compute and plot spike train
correlations (or more generally: event series correlations).
This analysis may also be referred to as event-triggered or
spike-triggered histograms, or peri-event histograms.

We will show one function (`peri_event_histogram`) that computes
a histogram of spike counts around reference events, and another
function that is based on kernel-density estimation.

"""

import numpy as np

import matplotlib.pyplot as plt

import fklab.events
import fklab.spikes.simulation


# %%
# Simulate spike trains
# ---------------------
#
# We first simulate the activity of two cells. The activity of cell 1
# is modulated by an oscillation. The activity of cell 2 follows spikes
# of cell 1, but with some temporal jitter and with reduced rate.

T = 60  # seconds, duration recording
fs = 1000  # Hz, sampling frequency of oscillatory modulation signal

base_rate = 5  # Hz, basal firing rate of cell 1
modulation_freq = 7  # Hz, frequency of modulatory oscillation
modulation = 20  # Hz, firing rate swing due to oscillation

cell2_shift = 0.3  # seconds, time lag of cell 2 relative to cell 1
cell2_spread = 0.05  # seconds, bandwidth of gaussian jitter

# time vector
time = np.linspace(0, T, T * fs + 1)

# oscillatory modulation signal
y = (1 + np.sin(2 * np.pi * modulation_freq * time)) * modulation + base_rate

# spike times of cell 1
cell1 = fklab.spikes.simulation.inhomogeneous_poisson_thinning(y, time)

# spike time of cell 2
cell2 = np.sort(cell1 + np.random.normal(cell2_shift, cell2_spread, size=cell1.shape))
cell2 = cell2[::2]  # drop spikes to reduce rate

# %%
# Peri-event histogram
# --------------------

# Here we compute the auto-correlations of cell1 and cell2
# and their cross-correlations.

hist, bins = fklab.events.peri_event_histogram(
    [cell1, cell2],
    [cell1, cell2],
    lags=np.linspace(-0.5, 0.5, 52),
    normalization="rate",
    unbiased=True,
)

mean_rate = [len(cell1) / T, len(cell2) / T]
max_y = np.percentile(hist, 99) * 1.2

fig, ax = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(8, 5))

for k in [0, 1]:
    for j in [0, 1]:
        ax[k, j].axhline(mean_rate[k], color="gray", lw=1)
        ax[k, j].axvline(0, color="gray", lw=1)

        if k == 1:
            ax[k, j].set(xlabel="lag [s] to cell {} spikes".format(j + 1))

        if j == 0:
            ax[k, j].set(ylabel="cell {}\nrate [Hz]".format(k + 1))

        if k != j:
            ax[k, j].axvline(cell2_shift * (2 * k - 1), color="gray", lw=1, ls=":")

        ax[k, j].bar(
            bins[:, 0],
            hist[:, k, j],
            width=bins[:, 1] - bins[:, 0],
            align="edge",
            color="darkgreen",
            alpha=0.3,
        )
        ax[k, j].set(ylim=(0, max_y))

# %%
# Peri-event density
# ------------------

# Here we compute the auto-correlations of cell1 and cell2
# and their cross-correlations.

density, lags = fklab.events.peri_event_density(
    [cell1, cell2],
    [cell1, cell2],
    lags=[-0.5, 0.5],
    npoints=201,
    bandwidth=0.005,
    remove_zero_lag=False,
)

mean_rate = [len(cell1) / T, len(cell2) / T]
max_y = np.percentile(density, 99) * 1.2

fig, ax = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(8, 5))

for k in [0, 1]:
    for j in [0, 1]:
        ax[k, j].axhline(mean_rate[k], color="gray", lw=1)
        ax[k, j].axvline(0, color="gray", lw=1)

        if k == 1:
            ax[k, j].set(xlabel="lag [s] to cell {} spikes".format(j + 1))

        if j == 0:
            ax[k, j].set(ylabel="cell {}\nrate [Hz]".format(k + 1))

        if k != j:
            ax[k, j].axvline(cell2_shift * (2 * k - 1), color="gray", lw=1, ls=":")

        ax[k, j].plot(lags, density[:, k, j], color="darkgreen", lw=2)
        ax[k, j].set(ylim=(0, max_y))


plt.show()

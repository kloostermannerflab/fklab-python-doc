"""
Spike rasters
-------------

This example shows how you can create spike raster plots.

"""

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm

import fklab.plot

# generate a few spike trains
# note that the times are sorted

np.random.seed(7)

ncells = 20
duration = 30  # seconds
nspikes = np.random.randint(30, 300, size=ncells)

spike_times = [np.sort(np.random.uniform(0, duration, size=n)) for n in nspikes]

# colors from a colormap
colors = [matplotlib.cm.turbo(n / (ncells - 1)) for n in range(ncells)]

# %%
# matplotlib's eventplot
# ----------------------
#
# The eventplot function is convenient for plotting multiple spike trains
# with given offsets.

# create a figure
fig, ax = plt.subplots(1, 1)

# eventplot / EventCollection
ax.eventplot(
    spike_times,
    # first event series is plotted at the top!
    lineoffsets=np.arange(ncells, 0, -1),
    linelengths=0.8,
    # a separate color for each spike train
    colors=colors,
)

# we will have to deal with labels ourselves
ax.set(
    yticks=np.arange(1, ncells + 1),
    yticklabels=["cell {}".format(c) for c in np.arange(ncells, 0, -1)],
)

_ = ax.set(title="spike raster with eventplot")

# %%
# fklab's plot_event_raster
# -------------------------
#
# The plot_event_raster function uses plot_event_image to convert event
# series to an image. The advantage is that plotting of many events or
# many event series is faster than plotting many line segments. This
# is especially critical when exploring many spike trains recorded
# over hours.
#
# Another difference with eventplot is that plot_event_raster also
# (optionally) can color the background of a spike train. And,
# plot_event_raster assists in labeling the spike trains and allow
# for varying label colors.
#
# Keyword arguments are used to specify properties, such as color and
# transparency, which can be either scalar values or sequences. In the
# latter case, properties can be set separately for each of the input
# spike trains.
#
# Note that goal of the code below is to show some features of the
# plot_event_raster function, and not to produce a very esthetic image.

# create a figure
fig, ax = plt.subplots(1, 1)

bg_colors = ["white", "lightgray"]
label_colors = ["royalblue", "tomato"]

fklab.plot.plot_event_raster(
    spike_times,
    axes=ax,
    heights=0.8,
    labels=["cell {}".format(c) for c in range(1, ncells + 1)],
    colors=colors,
    linewidths=2,
    # create banding in background color
    background_colors=[bg_colors[k % 2] for k in range(ncells)],
    background_alphas=0.5,
    # create additional grouping by label colors
    label_colors=[label_colors[k < ncells / 3] for k in range(ncells)],
)

_ = ax.set(title="spike raster with plot_event_raster")

plt.show()

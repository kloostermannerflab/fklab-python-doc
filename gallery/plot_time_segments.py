"""
Plot segments
-------------

This example shows how you can mark time segments in your plots.

"""

import numpy as np

import matplotlib.pyplot as plt

import fklab.segments

# create a few segment lists
run = np.array(
    [[0.5, 1.5], [3.5, 4], [5, 6], [9, 9.5], [11, 12], [13, 14.5], [17, 17.75]]
)
stop = fklab.segments.segment_intersection(
    fklab.segments.segment_invert(run), [[0, 20]]
)

# different colors for run and stop segments
colors = ["darkgreen", "orange"]

# generate some random data
x = np.linspace(0, 20, 100)
y = np.random.normal(1.5, 1, size=x.shape)

# create a figure with three axes
# to show three different methods
fig, axes = plt.subplots(3, 1, sharex=True, gridspec_kw={"hspace": 0.4})

# sphinx_gallery_defer_figures

# %%
# full-height patches
# -------------------
#
# Use the plot_segments_as_patches function in fklab.segments
# to mark segments with rectangles that span the height of the full
# axes (using the keyword argument ycoords='axes').
#
# Note that you can set the keyword argument orientation='vertical'
# if the time dimension should align with the y-axis.

_ = fklab.segments.plot_segments_as_patches(
    run, ycoords="axes", color="black", alpha=0.2, axes=axes[0]
)

_ = axes[0].plot(x, y, color="black")
_ = axes[0].set(title="segments as full-height patches")

# sphinx_gallery_defer_figures

# %%
# patches
# -------
#
# Use the plot_segments_as_patches function in fklab.segments
# to mark segments with rectangles with a specified y-location
# and height.
#
# Note that you can set the keyword argument orientation='vertical'
# if the time dimension should align with the y-axis.

for k, (seg, col) in enumerate(zip([run, stop], colors)):
    _ = fklab.segments.plot_segments_as_patches(
        seg, y=k + 1, height=0.8, color=col, axes=axes[1]
    )

_ = axes[1].set(
    title="segments as patches", ylim=(0, 3), yticks=[1, 2], yticklabels=["run", "stop"]
)

# sphinx_gallery_defer_figures

# %%
# line segments
# -------------
#
# Use the plot_segments_as_lines function in fklab.segments
# to mark segments with lines and optional start and end markers.
#
# Note that you can set the keyword argument orientation='vertical'
# if the time dimension should align with the y-axis.

for k, (seg, col) in enumerate(zip([run, stop], colors)):
    _ = fklab.segments.plot_segments_as_lines(
        seg, y=k + 1, color=col, axes=axes[2], startmarker="|", endmarker=">"
    )

_ = axes[2].set(
    title="segments as lines", ylim=(0, 3), yticks=[1, 2], yticklabels=["run", "stop"]
)

plt.show()

"""
Mark event times
----------------

This example shows how you can mark certain times in your plots.

"""

import numpy as np

import matplotlib.pyplot as plt

import fklab.plot

# generate a few event times
# note that the times are sorted

np.random.seed(7)
event_times = np.sort(np.random.uniform(size=5))

# create a figure with five axes
# to show five different methods
fig, ax = plt.subplots(5, 1, sharex=True, gridspec_kw={"hspace": 0.6})

# sphinx_gallery_defer_figures

# %%
# matplotlib's axvline
# --------------------
#
# To mark one (or a few) event times in your plot, you can make use
# of the matplotlib function axvline. This function will draw a vertical line
# that spans the full height of the axes at a given x coordinate.
#
# Note that you can use the equivalent axhline function to draw horizontal lines
# that span the width of the axes.

# axvline
for t in event_times:
    ax[0].axvline(t)

_ = ax[0].set(title="axvline")

# sphinx_gallery_defer_figures

# %%
# matplotlib's vlines
# --------------------
#
# The vlines function can be used to mark multiple event times in your plot.
# This function will draw vertical lines between two y-coordinates for the given
# x-coordinates.
#
# Note that you can use the equivalent hlines function to draw horizontal lines.

# vlines
ax[1].vlines(event_times, ymin=0.25, ymax=0.75)

_ = ax[1].set(title="vlines", ylim=(0, 1))

# sphinx_gallery_defer_figures


# %%
# matplotlib's vlines (span full axes)
# ------------------------------------
#
# You can make vlines draw lines that span the full axes (similar to
# the axvline function) by setting the appropriate transform.
#
# Note that when you want to the same with the hlines function,
# set the transform to ax.get_yaxis_transform().

# vlines
ax[2].vlines(event_times, ymin=0, ymax=1, transform=ax[2].get_xaxis_transform())
_ = ax[2].set(title="vlines with transform", ylim=(0, 2))

# sphinx_gallery_defer_figures


# %%
# fklab's labeled_vmarker
# -----------------------
#
# For a convenient way to draw vertical lines with a label, you can use
# the labeled_vmarker function.
#
# Note that you can usew the equivalent labeled_hmarker function to draw horizontal
# lines with a label.

# labeled_vmarker
for t in event_times:
    fklab.plot.labeled_vmarker(
        t, "t={:.2f}".format(t), ax=ax[3], text_y=0.05, text_offset=0.01
    )

_ = ax[3].set(title="labeled_vmarker")

# sphinx_gallery_defer_figures


# %%
# matplotlib's eventplot
# ----------------------
#
# To efficiently draw vertical lines for a multiple sets of event times
# (e.g., a spike trains from multiple neurons), you can use the eventplot
# function. This function will draw short vertical lines in data coordinates
# with specified y-offsets and heights.
#
# Note that you can set the keyword argument orientation='vertical' to draw
# horizontal lines.

# create multiple event time vectors with some jitter
multi_event = [
    event_times + np.random.normal(0, 0.02, size=event_times.shape) for k in range(4)
]

# eventplot / EventCollection
ax[4].eventplot(multi_event, lineoffsets=[1, 2, 3, 4], linelengths=0.8)
ax[4].set(title="eventplot")

plt.show()

"""
Plotting in polar coordinates
-----------------------------

This example shows how you can plot with polar coordinates

"""

import numpy as np
import matplotlib.pyplot as plt

import fklab.plot as fkplot

# %%
# Polar axes styles
# -----------------


fig, axes = plt.subplots(
    1,
    6,
    figsize=(16, 6),
    subplot_kw={"projection": "polar"},
    gridspec_kw={"wspace": 0.5},
)
fkplot.set_polar_axes(
    axes,
    show_radial_axis=False,
    angle_grid=False,
)

for ax, style in zip(
    axes,
    [
        "math",
        "math-degrees",
        "navigation",
        "navigation-degrees",
        "compass",
        "egocentric",
    ],
):

    fkplot.style_polar_axes(ax, style)
    ax.text(0, 0, style, ha="center", va="center")


# %%
# Angular reference plot and floating radial axis
# -----------------------------------------------

# sphinx_gallery_thumbnail_number = 2
fig, axes = plt.subplots(1, 1, figsize=(2, 2), subplot_kw={"projection": "polar"})

flax = fkplot.float_radial_axis(
    axes,
    label="test",
    offset=5,
    tickprops={"labelsize": "large"},
    labelprops={"fontsize": "x-large"},
)

fkplot.set_polar_axes(
    axes, annulus=0.3, radial_min=5, radial_max=10, radial_ticks=[5, 10]
)

fkplot.annotate_angles(
    axes,
    style="compass",
    facecolor=[0.9, 0.9, 0.9],
    fontsize=12,
    arrowprops={"color": "red", "lw": 1},
)


# %%
# Create grid of polar axes
# -------------------------

nplots = 10

polar_props = {
    "style": "egocentric",
    "style_kw": {"labels": {90: "L", 270: "R"}},
    "radial_min": 0,
    "radial_max": 10,
    "radial_ticks": [0, 5, 10],
    "annulus": 0.2,
    "radial_label": "radial",
}

fig, axes = fkplot.setup_polar_axes_grid(
    nplots,
    show_reference=True,
    reference_kw={"facecolor": [0.9, 0.9, 0.9]},
    axsize=(2, 2),
    axes_props=polar_props,
)

# %%
# Plot color maps
# ---------------

nplots = 14

polar_props = {
    "style": "egocentric",
    "style_kw": {"labels": {90: "L", 270: "R"}},
    "radial_min": 0,
    "radial_max": 10,
    "radial_ticks": [0, 5, 10],
    "annulus": 0.2,
    "radial_label": "radial",
}

fig, axes = fkplot.setup_polar_axes_grid(
    nplots + 1,  # add extra axes for colorbar
    show_reference=True,
    reference_kw={"facecolor": [0.9, 0.9, 0.9]},
    axsize=(2, 2),
    axes_props=polar_props,
)

roundto = 5

for k, ax in enumerate(axes.ravel()[:nplots]):
    data = np.random.uniform(0, np.random.uniform(5, 20, 1), size=(100, 100))
    vmax = np.ceil(np.nanmax(data) / roundto) * roundto

    fkplot.plot_polar_map(data, ax=ax, vmax=vmax)

    fkplot.axes_annotation(ax, "{}".format(k + 1), hloc="center", vloc="center")
    fkplot.axes_annotation(
        ax, "{:.0f} Hz".format(vmax), hloc="right in", vloc="top out"
    )


# use spot of extra axes for colorbar
ax = axes.ravel()[nplots]
ax.axis(False)

fkplot.fixed_colorbar(
    vmax="max",
    cmap="inferno",
    ndivisions=5,
    ax=ax,
    label="firing rate [Hz]",
    fraction=0.7,
    shrink=0.9,
    aspect=10,
)

# %%
# Plot color bar
# --------------

# plot cyclic colorbar in polar axes
fig, axes = plt.subplots(1, 1, figsize=(2, 2), subplot_kw=dict(projection="polar"))
fkplot.polar_colorbar(axes, rotation=-np.pi, cmap="hsv", style="math", annulus=0.5)

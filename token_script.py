from fklab.io.cloud import dropboxAPI
import os

pwd = os.environ.get("DATAPASSWORD")

dbx = dropboxAPI()
if pwd:
    dbx.add_token_file(pwd)

path = os.path.join(os.path.expanduser("~/Desktop"), "dataset")

if not os.path.exists(path):
    os.makedirs(path)

dbx.download("/Data/localize/info.yaml", os.path.join(path, "info.yaml"))  # Test
print("Dropbox Connexion established")
